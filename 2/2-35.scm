(define (displayn x)
  (display x)
  (newline))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
;; E.g.
;; (accumulate + 0 '(1 2 3))
;; (+ 1 (+ 2 (+ 3 0)))

;; (define (count-leaves t)
;;   (accumulate <??> <??> (map <??> <??>)))

;; E.g.
;; (c '(1 (2 3) 4)) -> 4
;; Using 'map' (and 'count-leaves') we can do this:
;; '(1 (2 3) 4) -> '(1 2 1)
;; And accumulate will just be called like: (ac + 0 result)

;; (map p '(1 (2 3) 4)) -> '(1 2 1)
;; '((p 1) (p '(2 3)) (p 4)) ->
;; If it's a leaf, 'p' returns '1', if not, call 'count-leaves'
;; (1 (c '(2 3)) 1) -> '(1 2 1)

(define (count-leaves t)
  (accumulate + 0 (map (lambda (item)
			 (if (list? item)
			     (count-leaves item)
			     1))
		       t)))

(displayn (count-leaves '(1 (2 (3 4)) (5 6 (7))))) ;; 7

;; But this actually can be done without using 'map':

;; (define (count-leaves t)
;;   (accumulate <??> <??> t))

;; E.g. (c '(1 (2 3) 4)) -> 4
;; (a p init '(1 (3 4) 4)) -> (p 1 (p '(2 3) (p 4))) ->
;; (+ 1 (+ (c '(2 3)) (+ 1 0)))


(define (count-leaves t)
  (accumulate (lambda (this-coeff higher-terms)
		(if (list? this-coeff)
		    (+ (count-leaves this-coeff) higher-terms)
		    (+ 1 higher-terms)))
	      0
	      t))

(displayn (count-leaves '(1 (2 (3 4)) (5 6 (7))))) ;; 7
