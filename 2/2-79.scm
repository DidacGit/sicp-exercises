;; Generic equality predicate.
(define (equ? x y) (apply-generic 'equ? x y))

;; Installation for all three data types. Note that the return value
;; is not tagged, since it's just the boolean symbols.
(put 'equ? '(scheme-number scheme-number) =)
(put 'equ? '(rational rational)
     (lambda (x y) (and (= (numer x) (numer y))
			(= (denom x) (denom y)))))
(put 'equ? '(complex complex)
     (lambda (x y) (and (= (real-part x) (real-part y))
			(= (imag-part x) (imag-part y)))))
