#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

;;(define (right-split painter n)
;;  (if (= n 0)
;;      painter
;;      (let ((smaller (right-split painter (- n 1))))
;;        (beside painter (below smaller smaller)))))

;;(define (up-split painter n)
;;  (if (= n 0)
;;      painter
;;      (let ((smaller (up-split painter (- n 1))))
;;        (below painter (beside smaller smaller)))))

(define (split procedure-1 procedure-2)
  (define (iter painter n)
    (if (= n 0)
        painter
        (let ((smaller (iter painter (- n 1))))
          (procedure-1 painter (procedure-2 smaller smaller)))))
  iter)

(define right-split (split beside below))
(define up-split (split below beside))

(paint (right-split einstein 3))
(paint (up-split einstein 3))