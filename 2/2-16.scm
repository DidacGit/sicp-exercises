(define (make-interval a b)
  (cons (min a b) (max a b)))
(define (lower-bound a)
  (car a))
(define (upper-bound a)
  (cdr a))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))
(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))
(define (sub-interval minu subtra)
  (make-interval (- (lower-bound minu) (upper-bound subtra))
		 (- (upper-bound minu) (lower-bound subtra))))
(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))
(define (div-interval x y)
  (if (= (width y) 0)
      (error "The width of the divisor is '0'.")
      (mul-interval x
		    (make-interval (/ 1.0 (upper-bound y))
				   (/ 1.0 (lower-bound y))))))

(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))
(define (make-center-percent c p)
  (let ((t (/ p 100)))
    (make-interval (- c (* c t))
		   (+ c (* c t)))))
(define (percent i)
  (let ((c (/ (+ (lower-bound i) (upper-bound i)) 2))
	(w (/ (- (upper-bound i) (lower-bound i)) 2)))
    (* (/ w c) 100)))
(define (display-interval i)
  (display "[")
  (display (center i))
  (display ", ")
  (display (percent i))
  (display "%]")
  (newline))

;; We can design functions so that variables that represent uncertain
;; numbers get repeated less (idealy, they don't get repeated)

;; For example, we know that: Ix/Iy = [cx/cy, px+py]
;; Now we try to find 'mul' in terms of 'c' and 'p'

(define r1 (make-center-percent 3 10))
(define r2 (make-center-percent 9 10))
(display-interval (mul-interval r1 r2)) ;; [27.27, 19.8019802%]
;; We can see how Ix*Iy = [cx*cy, px+py]

(define (mul-interval-new x y)
  (make-center-percent (* (center x) (center y))
		       (+ (percent x) (percent y))))
(define (div-interval-new x y)
  (make-center-percent (/ (center x) (center y))
		       (+ (percent x) (percent y))))

;; Now we can see how these new functions give the exact result, since
;; the variables that represent uncertain numbers are not repeated. 
(display-interval (mul-interval r1 r2)) ;; [27.27, 19.8019802%]
(display-interval (div-interval r2 r1)) ;; [3.0606060606060606, 19.801980198019802%]
(display-interval (mul-interval-new r1 r2)) ;; [27, 20%]
(display-interval (div-interval-new r2 r1)) ;; [3, 20%]

