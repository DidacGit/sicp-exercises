;; Original APPLY-GENERIC procedure:
(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
	  (apply proc (map contents args))
	  (if (= (length args) 2)
	      (let ((type1 (car type-tags))
		    (type2 (cadr type-tags))
		    (a1 (car args))
		    (a2 (cadr args)))
		(let ((t1->t2 (get-coercion type1 type2))
		      (t2->t1 (get-coercion type2 type1)))
		  (cond (t1->t2
			 (apply-generic op (t1->t2 a1) a2))
			(t2->t1
			 (apply-generic op a1 (t2->t1 a2)))
			(else (error "No method for these types"
				     (list op type-tags))))))
	      (error "No method for these types"
		     (list op type-tags)))))))

;; a)
(exp complex-a complex-b)
(apply-generic 'exp complex-a complex-b)
;; Since the EXP procedure is not implemented for complex numbers,
;; APPLY-GENERIC starts an infinite loop: It keeps calling itself in
;; the line "(apply-generic op (t1->t2 a1) a2)", coercing the fist
;; argument to its own type.

;; If this is done with scheme numbers, it works because the EXP
;; procedure is implemented for scheme numbers.

;; b) He is correct about the fact that something has to change, but
;; his solution is not correct. The APPLY-GENERIC procedure has to be
;; changed so it doesn't coerce arguments to each other's type if they
;; have the same one.

;; c) Modification of APPLY-GENERIC:
(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
	  (apply proc (map contents args))
	  (if (= (length args) 2)
	      (let ((type1 (car type-tags))
		    (type2 (cadr type-tags))
		    (a1 (car args))
		    (a2 (cadr args)))
		(if (eq? type1 type2)
		    ;; They have the same type but there's no procedure.
		    (error "No method for these types" (list op type-tags))
		    (let ((t1->t2 (get-coercion type1 type2))
			  (t2->t1 (get-coercion type2 type1)))
		      (cond (t1->t2
			     (apply-generic op (t1->t2 a1) a2))
			    (t2->t1
			     (apply-generic op a1 (t2->t1 a2)))
			    (else (error "No method for these types"
					 (list op type-tags)))))))
	      (error "No method for these types"
		     (list op type-tags)))))))
