(define (reverse l)
  (define (reverse-iter in out)
    (if (null? in)
	out
	(reverse-iter (cdr in) (cons (car in) out))))
  (reverse-iter l (list)))

(display (reverse (list 1 4 9 16 25))) ;; (25 16 9 4 1)
(newline)

