#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

(define (transform-painter painter origin corner1 corner2)
  (lambda (frame)
    (let ((m (frame-coord-map frame)))
      (let ((new-origin (m origin)))
        (painter (make-frame
                  new-origin
                  (sub-vect (m corner1) new-origin)
                  (sub-vect (m corner2) new-origin)))))))

;; Answer

;; Version analogous to beside
(define (below-a painter1 painter2)
  (let* ((mid-point (make-vect 0 0.5))
         (paint-bottom (transform-painter painter1
                                          (make-vect 0 0)
                                          (make-vect 1 0)
                                          mid-point))
         (paint-top (transform-painter painter2
                                       mid-point
                                       (make-vect 1 0.5)
                                       (make-vect 0 1))))
    (lambda (frame)
      (paint-bottom frame)
      (paint-top frame))))

;; Below is the same as rotating both painters 90 grads, putting them
;; beside and rotating the whole by 270 grads.
(define (below-b painter1 painter2)
  (lambda (frame)
    (rotate-270 (beside (rotate-90 painter2) (rotate-90 painter1)))))
