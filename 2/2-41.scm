;; Necessary procedures
(define (displayn x)
  (display x)
  (newline))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))
(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

;; Exercise procedures:

;; i!=j!=k <= n
;; E.g. n=4
;; i: (1 2 3 4)
;; for i=1, j: (2 3 4)
;; for i=1, j=2, k: (3 4)
;; triples: (1 2 3) (1 2 4)

;; We will need this two simple procedures
;; Return #t if 'x' is inside the list 'l'
(define (is-in? x l)
  (cond ((null? l) #f)
	((eq? x (car l)) #t)
	(else (is-in? x (cdr l)))))
;; Enumerate interval except the elements of the list 'without'
(define (enumerate-interval-without low high without)
  (filter (lambda (x)
	    (not (is-in? x without)))
	  (enumerate-interval low high)))

;; This builds only the (i j) pairs, it's very similar to the last exercise
(define (make-pairs n)
  (flatmap (lambda (i)
	     (map (lambda (j)
		    (list i j))
		  (enumerate-interval-without 1 n (list i))))
	   (enumerate-interval 1 n)))

(displayn (make-pairs 4)) ;; ((1 2) (1 3) (1 4) (2 1) (2 3) (2 4) (3 1) (3 2) (3 4) (4 1) (4 2) (4 3))

;; This builds the (i j k) triples, it uses a third nested 'map'
(define (make-triples n)
  (flatmap (lambda (i)
	     (flatmap (lambda (j)
			(map (lambda (k)
			       (list i j k))
			     (enumerate-interval-without 1 n (list i j))))
		      (enumerate-interval-without 1 n (list i))))
	   (enumerate-interval 1 n)))

(displayn (make-triples 4)) ;; ((1 2 3) (1 2 4) (1 3 2) (1 3 4) (1 4 2) (1 4 3) (2 1 3) (2 1 4) (2 3 1) (2 3 4) (2 4 1) (2 4 3) (3 1 2) (3 1 4) (3 2 1) (3 2 4) (3 4 1) (3 4 2) (4 1 2) (4 1 3) (4 2 1) (4 2 3) (4 3 1) (4 3 2))

(define (unique-triples n s)
  (filter (lambda (x)
	    (eq? s (accumulate + 0 x)))
	  (make-triples n)))

(displayn (unique-triples 4 6)) ;; ((1 2 3) (1 3 2) (2 1 3) (2 3 1) (3 1 2) (3 2 1))
