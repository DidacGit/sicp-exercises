(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (element-of-set? x set)
  (cond ((null? set) #f)
	((= x (entry set)) #t)
	((< x (entry set))
	 (element-of-set? x (left-branch set)))
	((> x (entry set))
	 (element-of-set? x (right-branch set)))))

(define (adjoin-set x set)
  (cond ((null? set) (make-tree x '() '()))
	((= x (entry set)) set)
	((< x (entry set))
	 (make-tree (entry set)
		    (adjoin-set x (left-branch set))
		    (right-branch set)))
	((> x (entry set))
	 (make-tree (entry set)
		    (left-branch set)
		    (adjoin-set x (right-branch set))))))

(define (tree->list-1 tree)
  (if (null? tree)
      '()
      (append (tree->list-1 (left-branch tree))
	      (cons (entry tree)
		    (tree->list-1 (right-branch tree))))))

(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
	result-list
	(copy-to-list (left-branch tree)
		      (cons (entry tree)
			    (copy-to-list (right-branch tree)
					  result-list)))))
  (copy-to-list tree '()))

;; Answer:

(define tree-a '(7 (3 (1 () ())
		      (5 () ()))
		   (9 ()
		      (11 () ()))))

(define tree-b '(3 (1 () ())
		   (7 (5 () ())
		      (9 ()
			 (11 () ())))))

(define tree-c '(5 (3 (1 () ())
		      ())
		   (9 (7 () ())
		      (11 () ()))))


(tree->list-1 tree-a) ; (1 3 5 7 9 11)
(tree->list-2 tree-a) ; (1 3 5 7 9 11)
(tree->list-1 tree-b) ; (1 3 5 7 9 11)
(tree->list-2 tree-b) ; (1 3 5 7 9 11)
(tree->list-1 tree-c) ; (1 3 5 7 9 11)
(tree->list-2 tree-c) ; (1 3 5 7 9 11)

;; a) Both procedures work for every tree.

;; b) Both procedures call themselves two times for each node. But the
;; first one makes use of append, which has to iterate the nodes
;; (O(n)). So instead of O(log(n)) it grows with O(1).

;; For a list 'n' append grows with O(n) while cons grows with
;; O(1). Append has to traverse the list while cons always takes the
;; same constant amount of time.

;; For a tree with 'n' nodes:
;; For tree->list-1 (that uses append)
;; It visits each tree item once and applies append:
;; O(n*append) = O(n*(n/2 + n/4 + n/8 ...)) = O(nlogn)
;; For tree->list-2 (that uses only cons)
;; It visits each tree item once and applies cons to it: O(n*1) = O(n)
