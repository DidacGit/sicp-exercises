;; Generic operation that pushes an object down in the tower of types.
(define (project n)
  (apply-generic 'project n))

;; Just take the real part of the complex number.
(put 'project '(complex)
     (lambda (n)
       (make-real (real-part n))))
;; I can't devise a simple way to simplify a real number to a rational
;; one. So try to project it directly to an integer.
(put 'project '(real)
     (lambda (n)
       (make-integer (round x))))
;; Round the rational number to the closest integer.
(put 'project '(rational)
     (lambda (n)
       (make-integer (round (/ (numer n) (denom n))))))

;; Procedure that simplifies an object type to the lowest level of the
;; tower possible. To do that it iterates comparing the result of
;; projecting it and then raising it, if it's still the same, it can
;; be lowered.
(define (drop n)
  ;; First we need to check that N is not at the bottom of the tower
  ;; of types. If it is, just return it.
  (if (get 'project (list (type-tag n)))
      (let ((n-projected (project n)))
	(if (equ? n (raise n-projected))
	    (drop n-projected)
	    n))
      n))

;; Make APPLY-GENERIC simplify its answers.
(define (apply-generic op . args)
  (let* ((type-tags (map type-tag args))
	 (proc (get op type-tags)))
    (if proc
	;; The only change is here:
	(drop (apply proc (map contents args)))
	(if (< 1 (length args))
	    (coerce op args type-tags)
	    (error "No method for this type" (list op (car args)))))))
