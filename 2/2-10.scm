(define (make-interval a b) (cons a b))

(define (upper-bound a)
  (max (car a) (cdr a)))

(define (lower-bound a)
  (min (car a) (cdr a)))

(define (width-interval a) ;; measure of uncertainity
  (/ (- (upper-bound a) (lower-bound a))
     2))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (sub-interval minu subtra)
  (make-interval (- (lower-bound minu) (upper-bound subtra))
		 (- (upper-bound minu) (lower-bound subtra))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (if (= (width-interval y) 0)
      (error "The width of the divisor is '0'.")
      (mul-interval x
		    (make-interval (/ 1.0 (upper-bound y))
				   (/ 1.0 (lower-bound y))))))
(define (display-interval i)
  (display "[")
  (display (lower-bound i))
  (display ", ")
  (display (upper-bound i))
  (display "]")
  (newline))

(define ia (make-interval 2 3))
(define ib (make-interval 3 3))
(div-interval ia ib) ;; "Error: the width of the divisor is '0'."
(newline)
