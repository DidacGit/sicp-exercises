;; Leaf implementation.
(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x)
  (cadr x))
(define (weight-leaf x)
  (caddr x))

;; Tree implementation.
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

;; Decoding algorithm.
(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (decode-1 bits tree))

(define (choose-branch bit branch)
  (cond ((= bit 0) (left-branch branch))
        ((= bit 1) (right-branch branch))
        (else (error "bad bit: CHOOSE-BRANCH" bit))))

;; Encoding algorithm.
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol sym tree)

  (define (symbol-is-here? branch)
    (if (memq sym (symbols branch))
        #t #f))

  (define (encode-symbol-iter tree)
    (if (leaf? tree)
        '()
        (let ((left-b (left-branch tree))
              (right-b (right-branch tree)))
          (if (symbol-is-here? left-b)
              (cons 0 (encode-symbol-iter left-b))
              (cons 1 (encode-symbol-iter right-b))))))

  (if (symbol-is-here? tree)
      (encode-symbol-iter tree)
      (error "bad symbol: ENCODE-SYMBOL" sym)))


(define sample-tree
  (make-code-tree (make-leaf 'A 4)      ; 0
                  (make-code-tree
                   (make-leaf 'B 2)     ; 10
                   (make-code-tree
                    (make-leaf 'D 1)    ; 110
                    (make-leaf 'C 1))))) ; 111

;; Answer:

;; For n = 5:
(define pairs-a '((A 1) (B 2) (C 4) (D 8) (E 16)))
(define tree-a (generate-huffman-tree pairs-a))
;; (((((leaf A 1)
;;     (leaf B 2)
;;     (A B) 3)
;;    (leaf C 4)
;;    (A B C) 7)
;;   (leaf D 8)
;;   (A B C D) 15)
;;  (leaf E 16)
;;  (A B C D E) 31)

;; For the most frequent symbol:
(encode '(E) tree-a) ; (1)
;; It does these calls:
;; One call to encode, therefore one call to append, but on a single-item list so it's constant.
;; One call to encode-symbol, therefore one call to memq: O(n)
;; Which calls its iter procedure 1 time. Calling memq: O(n)

;; In total, for N: 2*O(n) = O(n)

;; For the least frequent symbol:
(encode '(A) tree-a) ; (0 0 0 0)
;; It does these calls:
;; One call to encode, therefore one call to append, but on a single-item list so it's constant.
;; One call to encode-symbol, therefore one
;; call to memq: O(n) Which calls its iter procedure n times.
;; Every time calling memq: O(n)

;; In total, for N: O(n) + n*O(n) = O(n^2)
