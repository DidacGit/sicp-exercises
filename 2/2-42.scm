;; More information on the problem (useful): https://en.wikipedia.org/wiki/Eight_queens_puzzle

;; Necessary procedures
(define (displayn x)
  (display x)
  (newline))
(define (displaym lst)
  (map (lambda (element) (display element)) lst))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))
(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))
(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
	    (accumulate-n op init (map cdr seqs)))))
;; E.g.
;; (accumulate-n - 0 '((1 2) (3 4))) -> '((- 1 (- 3 0)) (- 2 (- 4 0))) -> '(-2 -2)
;; return the element of an index of a list
(define (list-ref k lst) 
  (define (iter count lst)
    (if (= count k)
	(car lst)
	(iter (+ count 1) (cdr lst))))
  (iter 0 lst))

;; Exercise Procedures

;; For us, a board is a list of columns, which are represented by a
;; number, the position of the queen. An empty board is an empty list.

(define (display-board board number-of-rows)
  (let ((number-of-columns (length board)))
    ;; In an incorrect board, there can be more than one queen per row
    (map (lambda (row-number)
	   (map (lambda (row-position)
	   	  (if (= row-number (list-ref row-position board))
	   	      (display 'X)
	   	      (display 'O))
	   	  (display " "))
	   	(enumerate-interval 0 (- number-of-columns 1)))
	   (newline))
	 (enumerate-interval 1 number-of-rows))
    (newline)))
(define (display-boards boards number-of-rows)
  (map (lambda (board) (display-board board number-of-rows)) boards))

(define (queens board-size)
  (let ((empty-board '()))
    
    (define (adjoin-position new-column board)
      ;; It doesn't need the 'k'
      (append board (list new-column)))

    (define (safe? board)
      ;; It doesn't need the 'k'
      (let* ((board-length (length board))
	     (last-queen-position (list-ref (- board-length 1) board)))
	(define (iter-board column-number)
	  (if (= column-number (- board-length 1))
	      #t
	      (let ((current-queen-position (list-ref column-number board))
		    (column-difference (- board-length (+ column-number 1))))
		;; Same row, up diagonal and down diagonal
		(if (or (= last-queen-position current-queen-position)
			(= last-queen-position (- current-queen-position column-difference))
			(= last-queen-position (+ current-queen-position column-difference)))
		    #f
		    (iter-board (+ column-number 1))))))
	(iter-board 0)))
    
    (define (queen-cols k)
      (if (= k 0)
	  (list empty-board)
	  (filter
	   (lambda (board) (safe? board))
	   (flatmap
	    (lambda (board)
	      (map (lambda (new-column)
		     (adjoin-position new-column board))
		   (enumerate-interval 1 board-size)))
	    (queen-cols (- k 1))))))
    
    (queen-cols board-size)))

(displaym (list "Queens " 1 ": length " (length (queens 1)))) (newline)
(displaym (list "Queens " 2 ": length " (length (queens 2)))) (newline)
(displaym (list "Queens " 3 ": length " (length (queens 3)))) (newline)
(displaym (list "Queens " 4 ": length " (length (queens 4)))) (newline)
(displaym (list "Queens " 5 ": length " (length (queens 5)))) (newline)
(displaym (list "Queens " 8 ": length " (length (queens 8)))) (newline)
(displayn "Some examples: ")
(display-board (car (queens 8)) 8)
(display-board (list-ref 2 (queens 8)) 8)
