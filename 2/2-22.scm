(define (square x)
  (expt x 2))

(define (square-list-recur items)
  (if (null? items)
      '()
      (cons (expt (car items) 2) (square-list-recur (cdr items)))))

;; It produces the answer in reverse order since we iterate the list from the
;; first item to the last one. The first item is the first added and the last item
;; is the last added. The last item will be the first and the first one will be the last.
(define (square-list-a items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (cons (square (car things))
		    answer))))
  (iter items '()))

;; Interchanging the arguments makes it add a list as the car of the new list, each time.
;; Resulting in a bunch of nested lists.
(define (square-list-b items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (cons answer
		    (square (car things))))))
  (iter items '()))

;; A solution would be to first apply 'square-list-a' and then 'reverse-list'
;; Which is what's done in the exercise '2-20'

(display (square-list-recur (list 1 2 3 4))) ;; (1 4 9 16)
(newline)
(display (square-list-a (list 1 2 3 4))) ;; (16 9 4 1)
(newline)
(display (square-list-b (list 1 2 3 4))) ;; ((((() . 1) . 4) . 9) . 16)
(newline)
