(define (displayn x)
  (display x)
  (newline))

;; Basic Procedures (1st level of abstraction)
;; To mantain the abstraction barrier, we have to implement the 'mobile?' procedure

;; First implementation: lists
(define (make-mobile left right)
  (list left right))
(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (car (cdr mobile)))
(define (make-branch length structure)
  (list length structure))
(define (branch-length branch)
  (car branch))
(define (branch-structure branch) ;; Either a number (weight) or another mobile
  (car (cdr branch)))
(define (mobile? structure) ;; For the lists implementation
  (list? structure))

;; Second implementation: pairs
(define (make-mobile left right)
  (cons left right))
(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (cdr mobile))
(define (make-branch length structure)
  (cons length structure))
(define (branch-length branch)
  (car branch))
(define (branch-structure branch)
  (cdr branch))
(define (mobile? structure) ;; For the pairs implementation
  (pair? structure))

;; Other procedures (2nd level of abstraction)
(define (total-weight mobile)
  (define (branch-weight-full branch)
    (let ((structure (branch-structure branch)))
      (if (mobile? structure)
	  (mobile-weight-full structure)
	  structure)))
  (define (mobile-weight-full mobile)
    (+ (branch-weight-full (left-branch mobile))
       (branch-weight-full (right-branch mobile))))
  (mobile-weight-full mobile))

(define (total-length mobile)
  (define (branch-length-full branch)
    (let ((b-length (branch-length branch))
	  (b-struct (branch-structure branch)))
      (if (mobile? b-struct)
	  (+ b-length (mobile-length-full b-struct))
	  b-length)))
  (define (mobile-length-full mobile)
    (+ (branch-length-full (left-branch mobile))
       (branch-length-full (right-branch mobile))))
  (mobile-length-full mobile))

;; We have to know that a balanced mobile can be thought as a weight,
;; which is the sum of it's child weights
(define (balanced? mobile)
  (define (torque branch)
    (let ((b-struct (branch-structure branch)))
      (* (branch-length branch)
	 (if (mobile? b-struct)
	     ;; If the struct is a mobile, calculate the total weight
	     (total-weight b-struct)
	     ;; If the struct is a weight, it's straight 
	     b-struct))))
  (define (balanced?-aux mobile)
    (let* ((l-branch (left-branch mobile))
	   (r-branch (right-branch mobile))
	   (l-branch-struct (branch-structure l-branch))
	   (r-branch-struct (branch-structure r-branch)))
      ;; The torques of both branches always need to be equal
      (and (= (torque l-branch) (torque r-branch))
	   ;; If a branch is a mobile, it needs to be balanced
	   (or (not (mobile? l-branch-struct)) (balanced?-aux l-branch-struct))
	   (or (not (mobile? r-branch-struct)) (balanced?-aux r-branch-struct)))))
  (balanced?-aux mobile))



(define random-mobile (make-mobile (make-branch 4 5)
				   (make-branch 2 (make-mobile (make-branch 4 3)
							       (make-branch 6 6)))))
(define balanced-mobile (make-mobile (make-branch 2 15.75)
				     (make-branch 3 (make-mobile (make-branch 4 4.5)
								 (make-branch 3 6)))))

(displayn (total-weight random-mobile)) ;; 14
(displayn (total-length random-mobile)) ;; 16
(displayn (balanced? random-mobile)) ;; #f
(displayn (balanced? balanced-mobile)) ;; #t
