;; First Version. Ditched for the next version since append runs in
;; O(n) and therefore this will run in O(n^2).
(define (adjoin-set x set)
  (define (iter set-in set-out)
    (if (null? set-in)
	;; If it was not in the set and it was not inserted, append it
	;; at the end of it.
	(append set-out (list x))
	(let ((car-in (car set-in))
	      (cdr-in (cdr set-in)))
	  (cond
	   ;; If it's already in the set, stop iterating and return
	   ;; the original set.
	   ((= x car-in) set)
	   ;; If it's still not on the set, just continue iterating.
	   ((> x car-in)
	    (iter cdr-in (append set-out (list car-in))))
	   ;; If the position is found, we add the 'x' in-between both
	   ;; lists.
	   (else (append set-out (list x) set-in))))))
  (iter set '()))

(adjoin-set 2 '(3 4 5 6 7)) ; (2 3 4 5 6 7)
(adjoin-set 8 '(3 4 5 6 7)) ; (3 4 5 6 7 8)
(adjoin-set 5 '(3 4 6 7)) ; (3 4 5 6 7)


;; On average it will require half as many steps as the unordered
;; representation. It will still grow with O(n).
(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< x (car set)) (cons x set))
        ((= x (car set)) set)
        (else (cons (car set) (adjoin-set x (cdr set))))))

(adjoin-set 2 '(3 4 5 6 7)) ; (2 3 4 5 6 7)
(adjoin-set 8 '(3 4 5 6 7)) ; (3 4 5 6 7 8)
(adjoin-set 5 '(3 4 6 7)) ; (3 4 5 6 7)
