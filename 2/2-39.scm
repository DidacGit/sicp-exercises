(define (displayn x)
  (display x)
  (newline))
(define (fold-right op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (fold-right op initial (cdr sequence)))))
;; E.g.
;; (fold-right + 0 '(1 2 3)) -> (+ 1 (+ 2 (+ 3 0))) -> 6
(define (fold-left op initial sequence)
  (define (iter result rest)
    (if (null? rest)
	result
	(iter (op result (car rest))
	      (cdr rest))))
  (iter initial sequence))
;; E.g.
;; (fold-left + 0 '(1 2 3)) -> (+ (+ (+ 0 1) 2) 3) -> 6

;; (define (reverse sequence)
;;   (fold-right (lambda (x y) <??>) nil sequence))

;; E.g.
;; (r '(1 2 3)) -> '(3 2 1)
;; (fr p '() '(1 2 3)) -> (p 1 (p 2 (p 3 '()))) ->
;; (p 1 (p 2 '(3))) -> (p 1 '(3 2)) -> '(3 2 1)
;; 'p' is like 'cons' but adds it at the end
;; (append '(3 2) '(1)) -> '(3 2 1)

(define (reverse sequence)
  (fold-right (lambda (x y)
		(append y (list x)))
	      '()
	      sequence))
(displayn (reverse '(1 2 3))) ;; '(3 2 1)

;; (define (reverse sequence)
;;   (fold-left (lambda (x y) <??>) nil sequence))

;; E.g.
;; (r '(1 2 3)) -> '(3 2 1)
;; (fl p '() '(1 2 3)) -> (p (p (p '() 1) 2) 3) ->
;; (p (p '(1) 2) 3) -> (p '(2 1) 3) -> '(3 2 1)
;; 'p' is just a reversed-parameters 'cons'

(define (reverse sequence)
  (fold-left (lambda (x y)
	       (cons y x))
	     '()
	     sequence))
(displayn (reverse '(1 2 3))) ;; '(3 2 1)
