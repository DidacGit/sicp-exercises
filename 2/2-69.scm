;; Leaf implementation.
(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x)
  (cadr x))
(define (weight-leaf x)
  (caddr x))

;; Tree implementation.
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

;; Construct an ordered set of leaves out of symbol-frequency pairs.
(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)   ; symbol
                               (cadr pair)) ; frequency
                    (make-leaf-set (cdr pairs))))))

;; Construct a huffman tree out of symbol-frequency pairs.
(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

;; Answer:
(define sample-pairs '((A 5) (B 3) (C 2) (D 2)))
(make-leaf-set sample-pairs) ; ((leaf D 2) (leaf C 2) (leaf B 3) (leaf A 5))

(define (successive-merge set)
  (if (null? (cdr set))
      (car set)
      (successive-merge (adjoin-set (make-code-tree (car set) (cadr set))
                                    (cddr set)))))

(generate-huffman-tree sample-pairs)
;; ((leaf A 5)
;;  ((leaf B 3)
;;   ((leaf D 2)
;;    (leaf C 2)
;;    (D C) 4)
;;   (B D C) 7)
;;  (A B D C) 12)
