(define (displayn x)
  (display x)
  (newline))

(define x '(1 3 (5 7) 9))
(displayn (car (cdr (car (cdr (cdr x)))))) ;; 7

(define y '((7)))
(displayn (car (car y))) ;; 7

(define z '(1 (2 (3 (4 (5 (6 7)))))))
(displayn (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr z))))))))))))) ;; 7
