(define (displayn x)
  (display x)
  (newline))
(define (square num)
  (* num num))

;; Already done, in the last exercise (2-30)
(define (tree-map proc tree)
  (cond ((null? tree) '())
	((not (pair? tree)) (proc tree))
	(else (cons (tree-map proc (car tree))
		    (tree-map proc (cdr tree))))))
(define (square-tree tree)
  (tree-map square tree))

(define tree-a (list 1 (list 2 (list 3 4) 5) (list 6 7))) ;; (1 (2 (3 4) 5) (6 7))
(displayn (square-tree tree-a)) ;; (1 (4 (9 16) 25) (36 49))

