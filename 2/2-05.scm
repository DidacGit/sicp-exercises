(define (cons a b)
  (* (expt 2 a) (expt 3 b)))
;; The integer has two parts (multipliers), an even part 2^a and an odd one 3^b

;; It iterates, dividing the product until it yields a non-null remainder,
;; then, it returns the amount of divisions made
(define (divide-until-remainder base exponent product)
  (if (= 0 (remainder product base))
      (divide-until-remainder base (+ exponent 1) (/ product base))
      exponent))

(define (car n)
  (divide-until-remainder 2 0 n))

(define (cdr n)
  (divide-until-remainder 3 0 n))

;; Test
;; 3888 = 2^4 3^5
(define z (cons 4 5))
(display z) ;; 3888
(newline)
(display (car z)) ;; 4
(newline)
(display (cdr z)) ;; 5
(newline)
