(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (list->tree elements)
  (car (partial-tree elements (length elements))))

;; a)

;; The procedure is clearer using "let*".
;; Takes as arguments an integer N and list of at least N elements and
;; constructs a balanced tree containing the ﬁrst N elements of the
;; list.

;; The result returned by it is a pair (formed with cons) whose car is
;; the constructed tree and whose cdr is the list of elements not
;; included in the tree.
(define (partial-tree elts n)
  ;; If the size of this branch (left or right) is 0, return just the
  ;; empty list.
  (if (= n 0)
      (cons '() elts)
      (let* (
	     ;; First calculate the size of both branches. For odd
	     ;; lists it will be the same and for even lists the right
	     ;; branch will be one element bigger.
	     (left-size (quotient (- n 1) 2))
	     (right-size (- n (+ left-size 1)))
	     ;; The left tree will be the balanced tree of the
	     ;; elements of the first half of the list, the smaller
	     ;; ones.
	     (left-result (partial-tree elts left-size))
	     (left-tree (car left-result))
	     (non-left-elts (cdr left-result))
	     ;; The first element of the list of elements not included
	     ;; in the left tree is the entry element. The rest of the
	     ;; list are the elements of the right tree.
	     (this-entry (car non-left-elts))
	     (right-result (partial-tree (cdr non-left-elts)
					 right-size))
	     (right-tree (car right-result))
	     ;; The remaining elements will be an empty list when the
	     ;; length of ELTS equals N.
	     (remaining-elts (cdr right-result)))
	;; Return the tree and the remaining elements.
	(cons (make-tree this-entry
			 left-tree
			 right-tree)
	      remaining-elts))))

(list->tree '(1 3 5 7 9 11))
;; (5 (1 ()
;;       (3 () ()))
;;    (9 (7 () ())
;;       (11 () ())))

;;     5
;;  1     9
;;   3   7 11

;; This is how the left branch is computed:
(car (partial-tree '(1 3 5 7 9 11) 2)) ;; (1 () (3 () ()))
;; ls = 0
;; rs = 1
;; lr = (pt elts 0) = (cons '() elts)
;; lt = '()
;; nle = elts
;; te = 1
;; rr = (pt '(3 5 7 9 11) 1) = '((3 () ()) ..)
;; rt = '(3 () ())
;; re = '(5 7 9 11)

;; b)

;; The procedure PARTIAL-TREE is going to be run one time for each
;; node of the tree, i.e. each element of the list. We assume that all
;; the procedures called within it take constant time. Therefore the
;; procedure has O(n).  
