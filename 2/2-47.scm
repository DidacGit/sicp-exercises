#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

;; Frame Implementation
(define implementation 1)

(if (= implementation 1)
    (begin
      (define (make-frame origin edge1 edge2)
        (list origin edge1 edge2))

      (define (origin-frame frame) (car frame))
      (define (edge1-frame frame) (nth 1 frame))
      (define (edge2-frame frame) (nth 2 frame)))
    (begin
      (define (make-frame-b origin edge1 edge2)
        (cons origin (cons edge1 edge2)))

      (define (origin-frame frame) (car frame))
      (define (edge1-frame frame) (cadr frame))
      (define (edge2-frame frame) (cddr frame))))