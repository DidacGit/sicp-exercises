;; Message Passing.

(define (apply-generic op arg) (arg op))

;; The data object is represented as a procedure that takes as input the
;; operation name.
(define (make-from-real-imag x y)
  (define (dispatch op)
    (cond ((eq? op 'real-part) x)
          ((eq? op 'imag-part) y)
          ((eq? op 'magnitude) (sqrt (+ (expt x 2) (expt y 2))))
          ((eq? op 'angle) (atan y x))
          (else (error "Unknown op: MAKE-FROM-REAL-IMAG" op))))
  dispatch)

(apply-generic 'magnitude (make-from-real-imag 4 16)) ;; 16.492422502470642
(apply-generic 'angle (make-from-real-imag 4 16)) ;; 1.3258176636680326

;; Answer:

(define (make-from-mag-ang r a)
  ;; The internal procedure.
  (define (dispatch op)
    (cond ((eq? op 'real-part) (* r (cos a)))
          ((eq? op 'imag-part) (* r (sin a)))
          ((eq? op 'magnitude) r)
          ((eq? op 'angle) a)
          (else (error "Unknown op: MAKE-FROM-MAG-ANG" op))))
  dispatch)

(apply-generic 'real-part (make-from-mag-ang 16.5 1.33)) ;; 3.934854881656432
(apply-generic 'imag-part (make-from-mag-ang 16.5 1.33)) ;; 16.023948235697237
