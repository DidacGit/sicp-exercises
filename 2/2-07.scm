(define (make-interval a b) (cons a b))

;; If 'make-interval's arguments are 'value' and 'tolerance'
(define (upper-bound a)
  (let ((value (car a))
	(tolerance (cdr a)))
    (+ value (* tolerance value))))

(define (lower-bound a)
  (let ((value (car a))
	(tolerance (cdr a)))
    (- value (* tolerance value))))

;; If 'make-interval's arguments are 'lower-bound' and 'upper-bound'
(define (upper-bound a)
  (max (car a) (cdr a)))

(define (lower-bound a)
  (min (car a) (cdr a)))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval x
		(make-interval (/ 1.0 (upper-bound y))
			       (/ 1.0 (lower-bound y)))))

(define example (make-interval 6.12 7.48))
(display (upper-bound example)) ;; 7.48
(newline)
(display (lower-bound example)) ;; 6.12
(newline)
