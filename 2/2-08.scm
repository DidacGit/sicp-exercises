(define (make-interval a b) (cons a b))

(define (upper-bound a)
  (max (car a) (cdr a)))

(define (lower-bound a)
  (min (car a) (cdr a)))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

;; The two extreme cases are:
;; 1) The minuend is at its lower bound and the subtrahend is at its upper (lowest result)
;; 2) The minuend is at its upper bound and the subtrahend is at its lower (highest result)
(define (sub-interval minu subtra)
  (make-interval (- (lower-bound minu) (upper-bound subtra))
		 (- (upper-bound minu) (lower-bound subtra))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval x
		(make-interval (/ 1.0 (upper-bound y))
			       (/ 1.0 (lower-bound y)))))
(define (display-interval i)
  (display "[")
  (display (lower-bound i))
  (display ",")
  (display (upper-bound i))
  (display "]")
  (newline))

(define res-1 (make-interval 8 15))
(define res-2 (make-interval 2 6))
(display-interval (sub-interval res-1 res-2)) ;; 2 (8-6) 13 (15-2)
