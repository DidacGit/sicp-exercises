(define (displayn x)
  (display x)
  (newline))
(define (square x)
  (* x x))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
;; E.g.
;; (accumulate + 0 '(1 2 3))
;; (+ 1 (+ 2 (+ 3 0)))


;; (define (map p sequence)
;;   (accumulate (lambda (x y) <??>)
;; 	      nil
;; 	      sequence))
;; E.g.
;; (map square '(1 2 3)) -> '(1 4 9)
;; (ac f '() '(1 2 3)) ->
;; (f 1 (f 2 (f 3 '()))) -> (cons (sq 1) (cons (sq 2) (cons (sq 3) '())))

(define (map p sequence)
  (accumulate (lambda (x y)
		(cons (p x) y))
	      '()
	      sequence))

(displayn (map square '(1 2 3))) ;; (1 4 9)

;; (define (append seq1 seq2)
;;   (accumulate cons <??> <??>))
;; E.g.
;; (ap '(1 2 3) '(4 5 6)) -> '(1 2 3 4 5 6)
;; (ac cons '(4 5 6) '(1 2 3)) ->
;; (cons 1 (cons 2 (cons 3 '(4 5 6))))

(define (append seq1 seq2)
  (accumulate cons seq2 seq1))

(displayn (append '(1 2 3) '(4 5 6))) ;; '(1 2 3 4 5 6)

;; (define (length sequence)
;;   (accumulate <??> 0 sequence))
;; E.g.
;; (l '(1 2 3 4)) -> 4
;; (ac f 0 '(1 2 3 4))
;; (f 1 (f 2 (f 3 (f 4 0)))) -> '(+ 1 (+ 1 (+ 1 (+ 1 0))))

(define (length sequence)
  (accumulate (lambda (x y)
		(+ 1 y))
	      0
	      sequence))

(displayn (length '(1 2 3 4))) ;; 4
