;; Assume that everything that's not a list is a symbol
;; Also assume that (equal? '() '()) => #t
(define (equal? a b)
  (cond ((and (null? a) (null? b))
	 #t)
	((and (symbol? a) (symbol? b))
	 (eq? a b))
	((and (list? a) (list? b))
	 (and (equal? (car a) (car b))
	      (equal? (cdr a) (cdr b))))
	(else #f)))

(equal? '(this is a list) '(this is a list)) ; #t

(equal? '(this is a list) '(this (is a) list)) ; #f
