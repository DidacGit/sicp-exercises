;; Generalization of APPLY-GENERIC to handle coercion for multiple
;; arguments. It attempts to coerce all the arguments to the type of
;; the ﬁrst argument, then to the type of the second argument, and so
;; on.
(define (apply-generic op . args)
  (let* ((type-tags (map type-tag args))
	 (proc (get op type-tags)))
    (if proc
	(apply proc (map contents args))
	(if (< 1 (length args))
	    (coerce op args type-tags)
	    (error "No method for this type" (list op (car args)))))))

;; For each of the argument types, check if every coercion procedure
;; needed and the procedure itself exist, then apply the coercions
;; and, after that, the procedure.
(define (coerce op args type-tags)
  (define (iter unique-tags)
    (if (null? unique-tags)
	(let ((type (car unique-tags))
	      (tags (symbol-list (length args) type))
	      (proc (get op tags)))
	  (if proc
	      (let ((coercion-procedures type-tags type))
		(if coercion-procedures
		    (apply proc (map-iter coercion-procedures (map contents args)))
		    (error "The needed coercion procedures don't exist for these types"
			   (list op args))))
	      (iter (cdr unique-tags))))
	(error "The needed methods or coercion procedures don't exist for these types"
	       (list op args))))
  ;; Iterate each of the unique tags.
  (iter (uniquify type-tags)))

;; Return the list of unique elements.
(define (uniquify lst)
  (if (null? lst)
      '()
      (let ((car-lst (car lst))
	    (cdr-lst (cdr lst)))
	(if (member car-lst cdr-lst)
	    (uniquify cdr-lst)
	    (cons car-lst (uniquify cdr-lst))))))

;; Return a list of length LEN with the same symbol SYM.
(define (symbol-list len sym)
  (if (zero? len)
      '()
      (cons sym (symbol-list (- len 1) sym))))

;; Return a list of the procedures to coerce each argument type for
;; the TYPE. If they have the same type, simply return the identity
;; procedure. If one of the procedures doesn't exist, return #f.
(define (coercion-procedures types type)
  (define (iter types procedures)
    (if (null? types)
	procedures
	(let ((current-type (car types)))
	  (if (eq? current-type type)
	      (iter (cdr types)
		    (append procedures (list (lambda (x) x))))
	      (let ((proc (get-coercion current-type type)))
		(if proc
		    (iter (cdr types)
			  (append procedures (list proc)))
		    #f))))))
  (iter types '()))

;; Iterate both PROCEDURES and ELEMENTS applying each procedure to
;; each element. Return the resulting list.
(define  (map-iter procedures elements)
  (if (null? procedures)
      '()
      (cons ((car procedures) (car elements))
	    (map-iter (cdr procedures) (cdr elements)))))

;; This strategy is not sufficiently general since it coerces the
;; arguments so only the operations where all arguments of the same
;; type are searched. For N arguments that would be N operations. But,
;; taking into account mixed-type operations, the number of possible
;; operations can be up to N^N, in the extreme case where all
;; arguments had different type.

;; For example:
(exp rational complex)
;; If the corresponding procedure was not in the table, our strategy
;; would try again with these two coercions:
(exp rational rational)
(exp complex complex)
;; Leaving out this possible permutation:
(exp complex rational)
