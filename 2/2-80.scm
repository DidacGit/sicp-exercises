;; Generic predicate to test if its argument is zero.
(define (=zero? x) (apply-generic '=zero? x))

;; Implementation for all data types.
(put '=zero? '(scheme-number)
     (lambda (x) (= x 0)))
(put '=zero? '(rational)
     (lambda (x) (= (numer x) 0)))
(put '=zero? '(complex)
     (lambda (x) (= (real-part x) (imag-part x) 0)))
