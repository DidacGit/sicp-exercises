(define (displayn x)
  (display x)
  (newline))

(define x
  (list 1 (list
	   2 (list 3 4))))

(displayn x)
;; (1 (2 (3 4)))

;; Box and pointer structure and tree structure can be found in
;; '2-24.jpeg'
