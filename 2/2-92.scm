;; We first need to change the procedures so they work for polynomials
;; in different variables.

;; Our strategy is the one outlined in the text: impose a towerlike
;; structure. Convert one of the two polynomials so it has the other
;; one's variable as dominant and its former variable buried in the
;; coefficients.
(define (add-poly p1 p2)
  (let ((var-1 (variable p1))
	(term-list-1 (term-list p1)))
    (make-poly
     var-1
     (add-terms term-list-1
		(term-list (if (same-variable? var-1 (variable p2))
			       p2
			       (convert-poly p2 var-1)))))))
(define (mul-poly p1 p2)
  (let ((var-1 (variable p1))
	(term-list-1 (term-list p1)))
    (make-poly
     var-1
     (mul-terms term-list-1
		(term-list (if (same-variable? var-1 (variable p2))
			       p2
			       (convert-poly p2 var-1)))))))

;; For our conversion procedure to work, and not get into an infinite
;; loop, we need the generic operations ADD and MUL to work on the
;; mixed types of polynomial and a regular scheme number.
(define (install-mixedtype-mul-and-add)
  ;; Multiply the coefficient of each term of the POLY by NUM.
  (define (mul-poly-scheme poly num)
    (make-poly (variable poly)
	       (map (lambda (term)
		      (make-term (order term)
				 ;; The recursion will end when the
				 ;; coefficient is also a number.
				 (mul num (coeff term))))
		    (term-list poly))))
  ;; If the POLY has an order zero term, add NUM to it, if not, add a
  ;; new order zero term.
  (define (add-poly-scheme poly num)
    (define (find-last-term terms)
      (let ((term (first-term terms))
	    (rest-of-terms (rest-terms terms)))
	(cond ((not (empty-termlist? rest-of-terms))
	       (adjoin-term term (find-last-term rest-of-terms)))
	      ((= (order term) 0)
	       (make-term 0 (add (coeff term) num)))
	      (else (adjoin-term term (make-term 0 num))))))
    (make-poly (variable poly)
	       (find-last-term (term-list poly))))
  (define (tag p) (attatch-tag 'polynomial p))
  (put 'mul '(polynomial scheme)
       (lambda (pol num) (tag (mul-poly-scheme pol num))))
  (put 'mul '(scheme polynomial)
       (lambda (num pol) (tag (mul-poly-scheme pol num))))
  (put 'add '(polynomial scheme)
       (lambda (pol num) (tag (add-poly-scheme pol num))))
  (put 'add '(scheme polynomial)
       (lambda (num pol) (tag (add-poly-scheme pol num))))
  'done)
(install-mixedtype-mul-and-add)

;; We will also need a procedure to add multiple polynomials. They are
;; expected to have the same variable as dominant.
(define (add-polys lst)
  (accumulate add
	      (make-poly (variable (car lst))
			 (the-empty-termlist))
	      lst))

;; Convert POLY so VAR is the dominant variable.  Recursively convert
;; the coefficient of every term, multiplying it by the uni-term,
;; coefficient-one polygon that the variable by itself forms. Then add
;; all the resulting polygons. 
(define (convert-poly poly var)
  (cond ((not (poly? poly))
	 ;; Now ADD works with mixed-type POLY and SCHEME.
	 poly)
	((eq? (variable poly) var)
	 poly)
	(else (add-polys (map convert-and-mulitply
			      (split-poly poly))))))

;; Convert the coefficient and then multiply it by the variable.
(define (convert-and-multiply poly)
  (let ((term (first-term (term-list poly))))
    (mul
     (convert-poly (coeff term))
     ;; Make the uni-term poly with coefficient of one (the variable).
     (make-poly (variable poly)
		(adjoin-term (make-term (order term) 1)
			     (the-empty-termlist))))))

;; Check if the expression is a polynomial.
(define (poly? exp)
  (and (pair? exp) (eq? (car exp) 'poly)))

;; Return all the terms of a polynomial as a list of polynomials.     
(define (split-poly poly)
  (map (lambda (term)
	 (make-poly (variable poly)
		    (adjoin-term term (the-empty-termlist))))
       (term-list poly)))
