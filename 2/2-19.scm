(define (cc amount coin-values)
  (cond ((= amount 0) 1)
	((or (< amount 0) (no-more? coin-values)) 0)
	(else (+ (cc amount
		     (except-first-denomination coin-values))
		 (cc (- amount
			(first-denomination coin-values))
		     coin-values)))))

(define (first-denomination values)
  (car values))
(define (except-first-denomination values)
  (cdr values))
(define (no-more? values)
  (null? values))

(define uk-coins (list 100 50 20 10 5 2 1 0.5))
(define us-coins (list 50 25 10 5 1))
(display (cc 100 us-coins)) ;; 292
(newline)
(define us-coins (list 1 5 10 25 50))
(display (cc 100 us-coins)) ;; 292
;; The order of the coin list doesn't matter
;; It will always iterate the whole list, trying all possible combinations
;; Although it will be faster if the list starts with the lower valued coins
(newline)

