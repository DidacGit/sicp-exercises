#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

;; Vector, Frame and Segment Representation
(define (make-vect x y) (cons x y))
(define (xcor-vect vect) (car vect))
(define (ycor-vect vect) (cdr vect))

(define (add-vect v1 v2)
  (make-vect (+ (xcor-vect v1) (xcor-vect v2))
             (+ (ycor-vect v1) (ycor-vect v2))))

(define (sub-vect v1 v2)
  (make-vect (- (xcor-vect v1) (xcor-vect v2))
             (- (ycor-vect v1) (ycor-vect v2))))

(define (scale-vect s v)
  (make-vect (* s xcor-vect v) (* s ycor-vect v)))

(define (make-frame origin edge1 edge2)
        (list origin edge1 edge2))
(define (origin-frame frame) (car frame))
(define (edge1-frame frame) (nth 1 frame))
(define (edge2-frame frame) (nth 2 frame))

(define (frame-coord-map frame)
  (lambda (v)
    (add-vect
     (origin-frame frame)
     (add-vect (scale-vect (xcor-vect v) (edge1-frame frame))
               (scale-vect (ycor-vect v) (edge2-frame frame))))))

(define make-segment cons)
(define start-segment car)
(define end-segment cdr)

(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line
        ((frame-coord-map frame)
         (start-segment segment))
        ((frame-coord-map frame)
         (end-segment segment))))
     segment-list)))

;; Answer. I guess the d), to define the 'wave' painter, is a joke, lol.

(let ((top-left (make-vect 0 1))
      (top-right (make-vect 1 1))
      (bottom-left (make-vect 0 0))
      (bottom-right (make-vect 1 0))
      (mid-left (make-vect 0 0.5))
      (mid-top (make-vect 0.5 1))
      (mid-right (make-vect 1 0.5))
      (mid-bottom (make-vect 0.5 0)))
  
 (define painter-outline
   (segments->painter
    (list (make-segment bottom-left top-left)
          (make-segment top-left top-right)
          (make-segment top-right bottom-right)
          (make-segment bottom-right bottom-left))))

(define painter-x
   (segments->painter
    (list (make-segment bottom-left top-right)
          (make-segment bottom-right top-left))))

(define painter-diamond
   (segments->painter
    (list (make-segment mid-left mid-top)
          (make-segment mid-top mid-right)
          (make-segment mid-right mid-bottom)
          (make-segment mid-bottom mid-left)))))