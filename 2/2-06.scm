;; This procedure works like this:
(define zero
  (lambda (f)
    (lambda (x)
      x)))
;; For example, this expression
((zero f) x)
;; Evaluates to
((lambda (x) x)
 x)
;; And finally to
x
;; Which means: 'zero applications of a function'
;; Church Number 'n' => 'n' applications of a function

(define (add-1 n)
  (lambda (f)
    (lambda (x)
      (f ((n f) x)))))
;; This expression
(add-1 zero)
;; Evaluates to
((lambda (f)
   (lambda (x)
     (f ((zero f) x)))))
;; Substituting the expression: '((zero f) x)'
((lambda (f)
   (lambda (x)
     (f x))))
;; This should equal to 'one', since it is: '(+ zero one)'

;; Church Number 0 => 0 applications of a function
((zero f) x)
;; Evaluates to
x

;; Church Number 1 => 1 application of a function
((one f) x)
;; Should evaluate to
(f x)
;; Therefore (it equals to the result of '(+ zero 1)')
(define one
  (lambda (f)
    (lambda (x)
      (f x))))

;; Church Number 2 => 2 applications of a function
((two f) x)
;; Should evaluate to
(f (f x))
;; Therefore
(define two
  (lambda (f)
    (lambda (x)
      (f (f x)))))
;; Testing it
((two f) x)
;; Evaluates to
((lambda (x)
   (f (f x)))
 x)
;; And then to
(f (f x))

;; Since '(add a b)' will be used like this '(((add a b) f) x)', it
;; will consist of a second-order function (2-level nested lambdas)
;; and it will look like this
(define (add a b)
  (lambda (f)
    (lambda (x)
      ...)))
;; Since two Church Numbers 'a' and 'b' are used like this:
;; ((a f) x) => (fa (fa-1 (... (f1 x))
;; ((b f) x) => (fb (fb-1 (... (f1 x))
;; And since '(add a b)' is basically applying 'a+b' times 'f', we can 
;; substitute the 'x' in the first expression with the second expression
;; ((a f) ((b f) x) => (fa (fa-1 (... (f1 (fb (fb-1 (... (f1 x))))
(define (add a b)
  (lambda (f)
    (lambda (x)
      ((a f) ((b f) x)))))

;; Some examples of the application of these procedures are these
((zero square) 2) ;; 2
((one square) 2) ;; 4
((two square) 2) ;; 16
(((add-1 one) square) 2) ;; 16
(((add one one) square) 2) ;; 16
