;; Represent ordinary numbers as Scheme numbers instead of pairs with
;; a symbol.

(define (install-scheme-number-package)
  (put 'add '(scheme-number scheme-number) +)
  (put 'sub '(scheme-number scheme-number) -)
  (put 'mul '(scheme-number scheme-number) *)
  (put 'div '(scheme-number scheme-number) /)
  'done)

;; And now we don't need the MAKE-SCHEME-NUMBER procedure.

(define (attach-tag type-tag contents)
  (if (eq? type-tag 'scheme-number)
      ;; This modification can be unnecessary. 
      contents
      (cons type-tag contents)))
(define (type-tag datum)
  (cond ((pair? datum) (car datum))
	((number? datum) 'scheme-number)
	(else (error "Bad tagged datum: TYPE-TAG" datum))))
(define (contents datum)
  (cond ((pair? datum) (cdr datum))
	((number? datum) datum)
	(else (error "Bad tagged datum: CONTENTS" datum))))
