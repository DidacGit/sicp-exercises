;; It's very similar to MUL-POLY. Returns a list of the quotient and
;; remainder polys.
(define (div-poly p1 p2)
  (let ((var (variable p1)))
    (if (same-variable? var (variable p2))
	(map (lambda (termlist)
	       (make-poly var termlist))
	     (div-terms (term-list p1) (term-list p2))))
    (error "Polys not in same var: DIV-POLY" (list p1 p2))))

;; Returns a list with the quotient and the remainder termlists.
(define (div-terms L1 L2)
  (if (empty-termlist? L1)
      ;; This is the end of the recursion when there's no remainder.
      (list (the-empty-termlist) (the-empty-termlist))
      (let ((t1 (first-term L1))
	    (t2 (first-term L2)))
	(if (> (order t2) (order t1))
	    ;; This is the end of the recursion when there's a
	    ;; remainder.
	    (list (the-empty-termlist) L1)
	    (let* ((new-c (div (coeff t1) (coeff t2)))
		   (new-o (- (order t1) (order t2)))
		   (new-term (make-term new-o new-c)))
	      (let ((rest-of-result
		     ;; Here's the complete set of operations listed in the exercise.
		     (div-terms (sub-terms L1 (mul-terms new-term L2))
				L2)))
		;; The quotients need to be added and the remainder is
		;; simply the CADR of the last iteration.
		(list (add-terms new-term (car rest-of-result))
		      (cadr rest-of-result))))))))
