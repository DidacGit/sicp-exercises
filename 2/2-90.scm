;; This program has a problem. The ADJOIN-TERM are generic procedures
;; with one non-typified argument: TERM. Therefore, there will be an
;; error when APPLY-GENERIC is called since it will try to retrieve
;; the type-tag of that argument. This could be solved by modifying
;; APPLY-GENERIC to ignore non-typified arguments.

;; First we make the operations on term lists generic.
(define (adjoin-term term term-list) (apply-generic 'adjoin-term term term-list))
(define (first-term term-list) (apply-generic 'first-term term-list))
(define (rest-terms term-list) (apply-generic 'rest-terms term-list))
(define (empty-termlist? term-list) (apply-generic 'empty-termlist? term-list))
;; The procedures MAKE-TERM, ORDER and COEFF are not generic since the
;; implementation of terms will be the always the same.

;; Since the procedure THE-EMPTY-TERMLIST has no arguments. We only
;; have to create non-generic procedures for each type
;; (e.g. THE-EMPTY-TERMLIST-DENSE/SPARSE).

(define (install-polynomial-package)
  ;; Implementation of polynomials, terms and arithmetic.
  ...)
(install-polynomial-package)

(define (install-termlist-sparse-package)
  ;; First, the non-generic procedure.
  (define (adjoin-term-sparse term term-list)
    (if (=zero? (coeff term))
	term-list
	(cons term term-list)))
  (define (first-term-sparse term-list) (car term-list))
  (define (rest-terms-sparse term-list) (cdr term-list))
  (define (empty-termlist?-sparse term-list) (null? term-list))
  (define (tag term-list) (attach-tag 'sparse term-list))
  ;; This is the only non-generic procedure.
  (define (the-empty-termlist-sparse) (tag '()))
  (put 'adjoin-term '(sparse)
       (lambda (term term-list) (tag (adjoin-term-sparse term term-list))))
  (put 'first-term '(sparse) first-term-sparse)
  (put 'rest-terms '(sparse)
       (lambda (term-list) (tag (rest-terms-sparse term-list))))
  (put 'empty-termlist? '(sparse) empty-termlist?-sparse))
(install-termlist-sparse-package)

(define (install-termlist-dense-package)
  (define (adjoin-term-dense term term-list)
    (define (add-zeros zeros term-list)
      (if (zero? zeros)
	  (cons (coeff term) term-list)
	  (add-zeros (1- zeros) (cons 0 term-list))))
    (add-zeros (- (order term) (length term-list)) term-list))
  (define (first-term-dense term-list)
    (make-term (1- (length term-list))
	       (car term-list)))
  (define (tag term-list) (attach-tag 'dense term-list))
  ;; This is the only non-generic procedure.
  (define (the-empty-termlist-dense) (tag '()))
  (put 'adjoin-term '(dense)
       (lambda (term term-list) (tag (adjoin-term-dense term term-list))))
  (put 'first-term '(dense) first-term-dense)
  ;; The two remaining procedures are the same as the procedures for
  ;; the SPARSE type.
  (put 'rest-terms '(dense)
       (lambda (term-list) (tag (contents ((get 'rest-terms '(sparse))
					   term-list)))))
  (put 'empty-termlist? '(dense) (get 'empty-termlist? '(sparse))))
(install-termlist-dense-package)
