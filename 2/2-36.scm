(define (displayn x)
  (display x)
  (newline))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
;; E.g.
;; (accumulate + 0 '(1 2 3))
;; (+ 1 (+ 2 (+ 3 0)))

;; (define (accumulate-n op init seqs)
;;   (if (null? (car seqs))
;;       '()
;;       (cons (accumulate op init <??>)
;; 	    (accumulate-n op init <??>))))

;; E.g.
;; (ac-n + 0 '((1 2 3) (4 5 6) (7 8 9))) -> '(12 15 18) ->
;; (cons (ac + 0 '(1 4 7)) (cons (ac + 0 '(2 5 8)) (cons (ac + 0 '(3 6 9)))))

;; exp1 -> '(1 4 7)
;; We have to turn '((1 2 3) (4 5 6) (7 8 9)) into '(1 4 7)
;; (map car seq)

;; (ac-n + 0 '((1 2 3) (4 5 6) (7 8 9))) -> (cons 12 (ac-n + 0 exp2))
;; (ac-n + 0 exp2) -> '(15 18)
;; exp2 -> '((2 3) (5 6) (8 9))
;; We have to turn '((1 2 3) (4 5 6) (7 8 9)) into '((2 3) (5 6) (8 9))
;; (map cdr seq)

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
	    (accumulate-n op init (map cdr seqs)))))

(define s '((1 2 3) (4 5 6) (7 8 9) (10 11 12)))
(displayn (accumulate-n + 0 s)) ;; '(22 26 30)
