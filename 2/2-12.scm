;; Old implementation
(define (make-interval a b)
  (cons (min a b) (max a b)))
(define (lower-bound a)
  (car a))
(define (upper-bound a)
  (cdr a))
(define (display-interval i)
  (display "[")
  (display (lower-bound i))
  (display ", ")
  (display (upper-bound i))
  (display "]")
  (newline))
;; New implementation
(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

;; Answer implementation
(define (make-center-percent c p)
  (let ((t (/ p 100)))
    (make-interval (- c (* c t))
		   (+ c (* c t)))))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (percent i)
  (let ((c (/ (+ (lower-bound i) (upper-bound i)) 2))
	(w (/ (- (upper-bound i) (lower-bound i)) 2)))
    (* (/ w c) 100)))

;; Example: lb=10, ub=12
;; We can se how:
;; c=(lb+ub)/2, w=(ub-lb)/2, w=ct, p=100t
;; c=11, w=1, tolerance=0.091, p=9.1%
(define intv-a (make-interval 10 12)) ;; [10, 12]
(display-interval intv-a)
(define intv-b (make-center-percent 11 9.1)) ;; [9.999, 12.001]
(display-interval intv-b)
(newline)
