;; This procedure remains the same. It's O(n).
(define (element-of-set? x set)
  (cond ((null? set) #f)
	((equal? x (car set)) #t)
	(else (element-of-set? x (cdr set)))))

;; This one goes from O(n) to O(1).
(define (adjoin-set x set)
  (cons x set))

;; This one goes from O(n^2) to O(n), assuming the built-in append is O(n).
(define (union-set set1 set2)
  (append set1 set2))

;; This procedure remains the same. It's O(n^2). 
(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
	((element-of-set? (car set1) set2)
	 (cons (car set1) (intersection-set (cdr set1) set2)))
	(else (intersection-set (cdr set1) set2))))

;; This representation would be used with sets of in which repeating
;; elements would make sense. An example of this could be a production
;; line of a factory.
