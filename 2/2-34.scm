(define (displayn x)
  (display x)
  (newline))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
;; E.g.
;; (accumulate + 0 '(1 2 3))
;; (+ 1 (+ 2 (+ 3 0)))

;; (define (horner-eval x coefficient-sequence)
;;   (accumulate (lambda (this-coeff higher-terms) <??>)
;; 	      0
;; 	      coefficient-sequence))

;; E.g. 1 + 2x + 3x^2 -> 1 + x(2 + 3x)
;; (ac p 0 '(1 2 3)) -> (p 1 (p 2 (p 3 0))) ->
;; (+ 1 (* x (+ 2 (* x (+ 3 0)))))
;;
;; E.g. with the first element
;; this-coeff=1 and higher-terms=(+ 2 (* x (+ 3 0)))
;; (p this-coeff higher-terms) -> (+ this-coeff (* x higher-terms))

(define (horner-eval x coefficient-sequence)
  (accumulate (lambda (this-coeff higher-terms)
		(+ this-coeff (* x higher-terms)))
	      0
	      coefficient-sequence))

;; 1 + 3x + 5x^3 + x^5 at x = 2
(displayn (horner-eval 2 (list 1 3 0 5 0 1))) ;; 79
