(define (displayn x)
  (display x)
  (newline))

(define (reverse l)
  (define (iter in out)
    (if (null? in)
	out
	(iter (cdr in) (cons (car in) out))))
  (iter l '()))

(define (deep-reverse l)
  (define (iter in out)
    (cond ((null? in)
	   out)
	  ((list? (car in))
	   (iter (cdr in) (cons (deep-reverse (car in)) out)))
	  (else
	   (iter (cdr in) (cons (car in) out)))))
  (iter l '()))

(define x (list (list 1 2) (list 3 4))) ;; ((1 2) (3 4))
(displayn (reverse x)) ;; ((3 4) (1 2))
(displayn (deep-reverse x)) ;; ((4 3) (2 1))
