(car ''abracadabra)

(car (quote (quote abracadabra)))

;; 1) The outer list is evaluated
;; 2) The "car" symbol is evaluated, it is a function
;; 3) The inner list "(quote (quote abracadabra))" is evaluated
;; 4) The "quote" symbol is evaluated, it is a macro
;; its argument, the expression "(quote abracadabra)" is returned as data,
;; not evaluated

;; (car (quote (quote abracadabra))) => (car '(quote abracadabra)) => quote

