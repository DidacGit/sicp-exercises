(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp)
         (if (same-variable? exp var) 1 0))
        ((sum? exp)
         (make-sum (deriv (addend exp) var)
                   (deriv (augend exp) var)))
        ((product? exp)
         (make-sum (make-product
                    (multiplier exp)
                    (deriv (multiplicand exp) var))
                   (make-product
                    (deriv (multiplier exp) var)
                    (multiplicand exp))))
        ;; More rules can be added here.
        (else (error "unknown expression type:DERIV" exp))))

;; Answer:
(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        (else ((get 'deriv (operator exp))
               (operands exp) var))))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))

;; a) The previous procedure was written in a data-directed style. It is a
;; specific form of dispatching on type programming, which aids additivity. We
;; can't assimilate NUMBER? and VARIABLE? because these two procedures won't be
;; applied to tagged data, since there's only one implementation of them.

;; b) The procedures for derivatives of sums and products:

(define (install-first-package)
  (define (deriv-sum exp)
    (make-sum (deriv (addend exp) var)
              (deriv (augend exp) var)))
  (define (deriv-product exp)
    (make-sum (make-product
               (multiplier exp)
               (deriv (multiplicand exp) var))
              (make-product
               (deriv (multiplier exp) var)
               (multiplicand exp))))
  (put 'deriv '+ deriv-sum)
  (put 'deriv '* deriv-product))

;; c) Install another differentiation rule:

(define (install-exponentiation-rule)
  (define (deriv-exponentiation exp)
    (let ((base-exp (base exp))
	      (exponent-exp (exponent exp)))
	  (make-product (make-product
                     exponent-exp
				     (make-exponentiation
                      base-exp
					  (make-sum exponent-exp -1)))
			        (deriv base-exp var))))
  (put 'deriv '** deriv-exponentiation))

;; Note that we don't have to change the DERIV procedure. This implementation
;; has the additive property.

;; d) Changes in the

(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        (else ((get (operator exp) 'deriv)
               ;; (get 'deriv (operator exp))
               (operands exp) var))))

;; The only changes that need to be made are in the procedures that access the
;; dispatch table, where the operators need to be also switched:

(put '+ 'deriv deriv-sum)
(put '* 'deriv deriv-product)
(put '** 'deriv deriv-exponentiation)
