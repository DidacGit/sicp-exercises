;; We define subtraction as the addition of the first argument and the
;; second argument negated.

;; The new needed generic operation.
(define (negate x)
  (apply-generic 'negate x))

;; In the polynomial package:
(define (install-polynomial-package)
  ...
  (define (negate-poly p)
    ;; Iterate the term list, negating each coefficient.
    (define (iter in out)
      (if (empty-termlist? in)
	  out
	  (iter (rest-terms in)
		(let ((current-term (first-term in)))
		  (adjoin-term (make-term (order current-term)
					  (negate (coeff current-term)))
			       out)))))
    (make-poly (variable p) (iter (term-list p)
				  (the-empty-termlist))))
  
  (define (sub-poly min sub)
    (add-poly min (negate-poly sub)))
  
  (put 'negate '(polynomial)
       (lambda (p) (tag (negate-poly p))))
  (put 'sub '(polynomial polynomial)
       (lambda (m s) (tag (sub-poly m s)))))
