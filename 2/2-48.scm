#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

;; Segment Representation
(define make-segment cons)
(define start-segment car)
(define end-segment cdr)