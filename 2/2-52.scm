#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

;; a)

(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line
        ((frame-coord-map frame)
         (start-segment segment))
        ((frame-coord-map frame)
         (end-segment segment))))
     segment-list)))

;; We don't even need to have the segments that make up the wave painter

(define wave-with-additions
  (define additions
    ;; Add a triangle, for example
    (let ((top-left (make-vect 0 1))
	  (top-right (make-vect 1 1))
	  (bottom (make-vect 0.5 0)))
      (segments->painter (list (make-segment bottom top-left)
			       (make-segment top-left top-right)
			       (make-segment top-right bottom)))))
  (lambda (frame)
    (wave frame)
    (additions frame)))

;; b)

(define (flipped-pairs painter)
  (let ((painter2 (beside painter (flip-vert painter))))
    (below painter2 painter2)))

(define (right-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
	(beside painter (below smaller smaller)))))

(define (up-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (up-split painter (- n 1))))
        (below painter (beside smaller smaller)))))

(define (corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
	    (right (right-split painter (- n 1))))
	(let ((top-left (beside up up))
	      (bottom-right (below right right))
	      (corner (corner-split painter (- n 1))))
	  (beside (below painter top-left)
		  (below bottom-right corner))))))

;; Answer: use only one copy of the "up-split" and "right-split" images instead of two
;; Also, use "n" instead of "n - 1" for them now
(define (corner-split-alt painter n)
  (if (= n 0)
      painter
      (let ((top-left (up-split painter n))
	    (bottom-rigtht (right-split painter n))
	    (corner (corner-split-alt painter (- n 1))))
	(beside (below painter top-left)
		(below bottom-right corner)))))

;; c)

(define (square-of-four tl tr bl br)
  (lambda (painter)
    (let ((top (beside (tl painter) (tr painter)))
	  (bottom (beside (bl painter) (br painter))))
      (below bottom top))))

(define (square-limit painter n)
  (let ((combine4 (square-of-four flip-horiz identity
				  rotate180 flip-vert)))
    (combine4 (corner-split painter n))))

((square-of-four p1 p2 p3 p4) painter)

;; Answer: make the painter (e.g. rogers) look outward from each
;; corner of the square
(define (square-limit-alt painter n)
  (let ((combine4 (square-of-four identity flip-horiz
				  flip-vert rotate180)))
    (combine4 (corner-split painter n))))
