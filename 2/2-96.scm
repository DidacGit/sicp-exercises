;; a) Like REMAINDER-TERMS except that it multiplies the dividend by
;; the integerizing factor before calling DIV-TERMS. Returns the
;; pseudeoremainder. This only works for polynomials whose
;; coefficients are integers.
(define (pseudoremainder-terms a b)
  (cadr (div-terms (let ((first-term-b (first-term b)))
		     (mul-terms a
				;; Term list with just one order zero term (the constant).
				(adjoin-term
				 (make-term 0
					    (** (coeff first-term-b)
						(+ 1 (- (order first-term-b)
							(order (first-term a))))))
				 (the-empty-term-list))))
		   b)))

;; Modify it to use PSEUDOREMAINDER-TERMS.
(define (gcd-terms a b)
  (if (empty-termlist? b)
      a
      (gcd-terms b (pseudoremainder-terms a b))))

;; b) Modify it so that it removes common factors from the
;; coefficients of the answer by dividing all the coefficients by
;; their (integer) greatest common divisor.
(define (gcd-terms a b)
  (define (iter a b)
    (if (empty-termlist? b)
	a
	(iter b (pseudoremainder-terms a b))))
  ;; Return a list of all the coefficients of a term list.
  (define (get-coefficients term-list)
    (let ((rest-of-terms (rest-terms term-list))
	  (first-term-coeff (coeff (first-term term-list))))
      (if (empty-termlist? rest-of-terms)
	  first-term-coeff
	  (cons first-term-coeff (get-coefficients rest-of-terms)))))
  (define (remove-common-factors term-list)
    (map (lambda (term)
	   (make-term (order term)
		      (/ (coeff term)
			 ;; GCD of all the coefficients.
			 (accumulate (lambda (coeff-1 coeff-2)
				       (gcd coeff-1 coeff-2))
				     1
				     (get-coefficients term-list)))))
	 term-list))
  (remove-common-factors (iter a b)))
