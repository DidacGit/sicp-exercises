;; Leaf implementation.
(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x)
  (cadr x))
(define (weight-leaf x)
  (caddr x))

;; Tree implementation.
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

;; Construct an ordered set of leaves out of symbol-frequency pairs.
(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)   ; symbol
                               (cadr pair)) ; frequency
                    (make-leaf-set (cdr pairs))))))

;; Construct a huffman tree out of symbol-frequency pairs.
(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

(define (successive-merge set)
  (if (null? (cdr set))
      (car set)
      (successive-merge (adjoin-set (make-code-tree (car set) (cadr set))
                                    (cddr set)))))


(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol sym tree)
  (define (symbol-is-here? branch)
    (if (memq sym (symbols branch))
        #t #f))
  (define (encode-symbol-iter tree)
    (if (leaf? tree)
        '()
        (let ((left-b (left-branch tree))
              (right-b (right-branch tree)))
          (if (symbol-is-here? left-b)
              (cons 0 (encode-symbol-iter left-b))
              (cons 1 (encode-symbol-iter right-b))))))
  (if (symbol-is-here? tree)
      (encode-symbol-iter tree)
      (error "bad symbol: ENCODE-SYMBOL" sym)))

;; ;; Answer:
(define pairs '((A 2) (GET 2) (SHA 3) (WAH 1) (BOOM 1) (JOB 2) (NA 16) (YIP 9)))
(define lyrics-tree (generate-huffman-tree pairs))
;; ((leaf NA 16)
;;  ((leaf YIP 9)
;;   (((leaf A 2)
;;     ((leaf BOOM 1)
;;      (leaf WAH 1)
;;      (BOOM WAH) 2)
;;     (A BOOM WAH) 4)
;;    ((leaf SHA 3)
;;     ((leaf JOB 2)
;;      (leaf GET 2)
;;      (JOB GET) 4)
;;     (SHA JOB GET) 7)
;;    (A BOOM WAH SHA JOB GET) 11)
;;   (YIP A BOOM WAH SHA JOB GET) 20)
;;  (NA YIP A BOOM WAH SHA JOB GET) 36)

(define lyrics-symbols '(GET A JOB SHA NA NA NA NA NA NA NA NA
                             GET A JOB SHA NA NA NA NA NA NA NA NA
                             WAH YIP YIP YIP YIP YIP YIP YIP YIP YIP
                             SHA BOOM))
(encode lyrics-symbols lyrics-tree)
;; 84 bits:
;; (1 1 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 0 1 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 1 0 1 1 0 1 0)

;; There are 8 different symbols, therefore:
;; 2^x = 8; x = log2(8) = 3 bits for each symbol are needed.
;; The song has 36 words, therefore:
;; 36*3 = 108 bits are needed to encode this song.
;; 84 < 108, we can see how non-fixed lenght code is more efficient.
