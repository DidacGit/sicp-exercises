;; In the polynomial package:
(define (install-polynomial-package)
  ...
  (define (=zero-poly? p)
    (define (iter terms)
      (or (empty-termlist? terms)
	  (and (=zero? (coef (first-term term)))
	       (iter (rest-terms terms)))))
    (iter (term-list p)))
  (put '=zero? '(polynomial) =zero-poly?))
