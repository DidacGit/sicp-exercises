(define (map proc items)
  (if (null? items)
      '()
      (cons (proc (car items))
	    (map proc (cdr items)))))

(define (square-list-a items)
  (if (null? items)
      '()
      (cons (expt (car items) 2) (square-list-a (cdr items)))))

(define (square-list-b items)
  (map (lambda (x) (expt x 2)) items))

(display (square-list-a (list 1 2 3 4))) ;; (1 4 9 16)
(newline)
(display (square-list-b (list 1 2 3 4))) ;; (1 4 9 16)
(newline)
