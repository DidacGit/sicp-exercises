(define (make-interval a b)
  (cons (min a b) (max a b)))
(define (lower-bound a)
  (car a))
(define (upper-bound a)
  (cdr a))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))
(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))
(define (sub-interval minu subtra)
  (make-interval (- (lower-bound minu) (upper-bound subtra))
		 (- (upper-bound minu) (lower-bound subtra))))
(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))
(define (div-interval x y)
  (if (= (width y) 0)
      (error "The width of the divisor is '0'.")
      (mul-interval x
		    (make-interval (/ 1.0 (upper-bound y))
				   (/ 1.0 (lower-bound y))))))

(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))
(define (make-center-percent c p)
  (let ((t (/ p 100)))
    (make-interval (- c (* c t))
		   (+ c (* c t)))))
(define (percent i)
  (let ((c (/ (+ (lower-bound i) (upper-bound i)) 2))
	(w (/ (- (upper-bound i) (lower-bound i)) 2)))
    (* (/ w c) 100)))

;; Formulae for paralel resistances
(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
		(add-interval r1 r2)))
(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval one
		  (add-interval (div-interval one r1)
				(div-interval one r2)))))

;; For this excersise this way of printing the intervals will be more useful
(define (display-interval i)
  (display "[")
  (display (center i))
  (display ", ")
  (display (percent i))
  (display "%]")
  (newline))

;; Example: intervals with low and high percentages
(define r1l (make-center-percent 3 0.001))
(define r2l (make-center-percent 9 0.001))
(define r1h (make-center-percent 3 10))
(define r2h (make-center-percent 9 10))

(display-interval (div-interval r1l r1l)) ;; [1.0000000002, 0.0019999999998184933%]
(display-interval (div-interval r1h r1h)) ;; [1.02020202020202, 19.801980198019795%]

(display-interval (div-interval r2l r1l)) ;; [3.0000000006, 0.001999999999812942%]
(display-interval (div-interval r2h r1h)) ;; [3.0606060606060606, 19.801980198019802%]

;; As we can see from comparing 'r1/r1' and 'r2/r1' for low and high percentages:
;; Ix/Iy = [cx/cy, px+py]

;; So in our example: R = 1/((1/Ix)+(1/Iy)) = IxIy/(Ix+Iy) = 9/4 = 2.25
(display-interval (par1 r1l r2l)) ;; [2.2500000009, 0.0029999999992094653%]
(display-interval (par2 r1l r2l)) ;; [2.25, 0.001000000000001617%]
(display-interval (par1 r1h r2h)) ;; [2.340909090909091, 29.223300970873783%]
(display-interval (par2 r1h r2h)) ;; [2.25, 10.000000000000004%]

;; Since there are more operations on 'par1' the error becomes greater with each one.
;; There are less variables repeated in 'par2', that's why it's a better program
