(define (displayn x)
  (display x)
  (newline))
;; prime procedures
(define (square x)
  (* x x))
(define (smallest-divisor n)
  (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b)
  (= (remainder b a) 0))
(define (prime? n)
  (= n (smallest-divisor n)))
;; other procedures
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))
(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))
(define (prime-sum? pair)
  (prime? (+ (car pair) (cadr pair))))
(define (make-pair-sum pair)
  (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))

;; Original procedure
(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter prime-sum?
	       (flatmap
		(lambda (i)
		  (map (lambda (j) (list i j))
		       (enumerate-interval 1 (- i 1))))
		(enumerate-interval 1 n)))))

(displayn (prime-sum-pairs 4)) ;; ((2 1 3) (3 2 5) (4 1 5) (4 3 7))

;; E.g. n=4
;; i: (1 2 3 4)
;; for i=4, j=(1 2 3) -> (4 1) (4 2) (4 3)
;; for i=3 j=(1 2) -> (3 1) (3 2)
;; for i=2 j=(1) -> (2 1)
;; 'i' can't be '1' since i>j>=1

(define (unique-pairs n)
  (flatmap (lambda (i)
	     (map (lambda (j)
		    (list i j))
		  (enumerate-interval 1 (- i 1))))
	   (enumerate-interval 2 n)))

(displayn (unique-pairs 4)) ;; ((2 1) (3 1) (3 2) (4 1) (4 2) (4 3))

(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter prime-sum?
	       (unique-pairs n))))

(displayn (prime-sum-pairs 4)) ;; ((2 1 3) (3 2 5) (4 1 5) (4 3 7))
