;; Old implementation
(define (make-interval a b)
  (cons (min a b) (max a b)))
(define (lower-bound a)
  (car a))
(define (upper-bound a)
  (cdr a))
(define (display-interval i)
  (display "[")
  (display (lower-bound i))
  (display ", ")
  (display (upper-bound i))
  (display "]")
  (newline))
;; New implementation
(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

;; Answer implementation
(define (make-center-percent c p)
  (let ((t (/ p 100)))
    (make-interval (- c (* c t))
		   (+ c (* c t)))))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (percent i)
  (let ((c (/ (+ (lower-bound i) (upper-bound i)) 2))
	(w (/ (- (upper-bound i) (lower-bound i)) 2)))
    (* (/ w c) 100)))

;; Since 'all numbers are positive'
(define (mul-interval x y)
  (make-interval (* (lower-bound x) (lower-bound y))
		 (* (upper-bound x) (upper-bound y))))

;; We have two intervals Ix=[lx, ux], Iy=[ly, uy]
;; Their product is: Ixy=[(lxly, uxuy)]

;; We find 't' in terms of 'l' and 'u'
;; We know c=(u+l)/2, w=(u-l)/2, w=ct
;; Therefore
;; t=(u-l)/(u+l)

;; Txy = (uxuy - lxly)/(lxly + uxuy)
;; We apply 'l' and 'u' in terms of 'c' and 't': l=c(1-t) and u=c(1+t)
;; Txy = (cx(1+tx)cy(1+ty) - cx(1-tx)cy(1-ty)) / (cx(1-tx)cy(1-ty) + cx(1+tx)cy(1+ty))
;; Txy = (ty + tx)/(1 + txty)
;; Since 'tx' and 'ty' are said to be small, it can be aproximated to:
;; Txy ~ ty + tx

;; Example
(define Ix (make-center-percent 11 3))
(define Iy (make-center-percent 15 2))
(define Ixy (mul-interval Ix Iy))
(display (/ (percent Ixy) 100)) ;; 0.049 ~ 0.03 + 0.02
(newline)
