;; The amount of code that needs to be written when adding a new data type or a
;; new operation is about the same under all three approaches. The difference is
;; that in some cases, old code has to be changed.

;; Table: Must old code be changed?
;;
;; | Dispatch Type            | New Data Type | New Operation |
;; |--------------------------+---------------+---------------|
;; | Explicit Dispatch        | Yes           | No            |
;; | Data-directed Dispatch   | No            | No            |
;; | Message-passing Dispatch | No            | Yes           |

;; The only strategy that is appropriate in both cases is Data-directed
;; Dispatch.
