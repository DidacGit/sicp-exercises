(define (displayn x)
  (display x)
  (newline))
;; E.g. of the built-in 'map' procedure:
;; (map + '(1 2 3) '(4 5 6)) -> '(5 7 9)
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
;; E.g.
;; (ac + 0 '(1 2 3)) -> (+ 1 (+ 2 (+ 3 0)))
(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
	    (accumulate-n op init (map cdr seqs)))))
;; E.g.
;; (accumulate-n - 0 '((1 2) (3 4))) -> '((- 1 (- 3 0)) (- 2 (- 4 0))) -> '(-2 -2)
(define mat-a '((1 2 3) (4 5 6)))
(define mat-b '((1 2) (3 4) (5 6)))
(define vec-a '(1 2 3))
(define vec-b '(4 5 6))

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(displayn (dot-product vec-a vec-b)) ;; 32
;; E.g.
;; (dp '(1 2 3) '(4 5 6)) -> (+ 4 (+ 10 (+ 18 0))) -> 32

;; (define (matrix-*-vector m v)
;;   (map <??> m))

;; E.g.
;; (mv '((1 2 3) (4 5 6)) '(1 2 3)) -> '((1 4 9) (4 10 18))
;; (mv '((1 2 3) (4 5 6)) '(1 2 3)) -> (map exp '((1 2 3) (4 5 6))) ->
;; '((exp '(1 2 3)) (exp '(4 5 6))) -> '((1 4 9) (4 10 18))
;; (exp '(4 5 6)) -> '(4 10 18) -> '((* 1 4) (* 2 5) (* 3 6)) ->
;; ((lambda (row) (map * (list row '(1 2 3)))) '(4 5 6))

(define (matrix-*-vector m v)
  (map (lambda (row)
	 (map * row v))
       m))

(displayn (matrix-*-vector mat-a vec-a)) ;; '((1 4 9) (4 10 18))

;; (define (transpose mat)
;;   (accumulate-n <??> <??> mat))

;; E.g.
;; (t '((1 2 3) (4 5 6))) -> '((1 4) (2 5) (3 6))
;; (ac-n p init '((1 2 3) (4 5 6))) ->
;; '((p 1 (p 4 init)) (p 2 (p 5 init)) (p 3 (p 6 init)))
;; (cons 1 (cons 4 '())) -> '(1 4)

(define (transpose mat)
  (accumulate-n cons '() mat))

(displayn (transpose mat-a)) ;; '((1 4) (2 5) (3 6))

;; (define (matrix-*-matrix m n)
;;   (let ((cols (transpose n)))
;;     (map <??> m)))

;; E.g.
;; (mm mat-a mat-b) -> (mm '((1 2 3) (4 5 6)) '((1 2) (3 4) (5 6))) -> '((22 28) (49 64))
;; We can see how the variable 'cols' corresponds to the columns of 'mat-b'
;; cols -> '((1 3 5) (2 4 6))
;; (mm mat-a mat-b) -> (map p '((1 2 3) (4 5 6))) ->
;; '((p '(1 2 3)) (p '(4 5 6)))
;; '(p '(1 2 3)) -> '(22 28) ->
;; '((+ (* 1 1) (* 2 3) (* 3 5)) (+ (* 1 2) (* 2 4) (* 3 6))) ->
;; '((dot-product '(1 2 3) cols-1) (dot-product '(1 2 3) cols-2))
;; So we have:
;; cols -> '(c1 c2)
;; (p row) -> ((dp row c1) (dp row c2))
;; The procedure maps 'cols', applying 'dp' (with 'row') to it

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (m-row)
	   (map (lambda (n-col)
		  (dot-product m-row n-col))
		cols))
	 m)))

(displayn (matrix-*-matrix mat-a mat-b)) ;; '((22 28) (49 64))
