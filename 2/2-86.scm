;; First, we need to change the rectangular and polar packages so
;; their procedures can operate on multiple types, not just scheme
;; numbers. For this to work, we'll need to use these generic
;; procedures: SQUARE-ROOT (instead of SQRT), ADD (instead of +),
;; SQUARE (instead of the non-generic version), ARCTAN (instead of
;; ATAN), SINE (instead of SIN), COSINE (instead of COS) and MUL
;; (instead of *).

(define (install-rectangular-package)
  ...
  (define (magnitude z)
    (square-root (add (square (real-part z))
		      (square (imag-part z)))))
  (define (angle z)
    (arctan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a)
    (cons (mul r (cosine a)) (mul r (sine a))))
  (put 'magnitude '(rectangular) magnitude)
  (put 'angle '(rectangular) angle)
  (put 'make-from-map-ang 'rectangular
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(define (install-polar-package)
  ...
  (define (real-part z)
    (mul (magnitude z) (cosine (angle z))))
  (define (imag-part z)
    (mul (magnitude z) (sine (angle z))))
  (define (make-from-real-imag x y)
    (cons (square-root (add (square x) (square y)))
	  (arctan y x)))
  (put 'real-part '(polar) real-part)
  (put 'imag-part '(polar) imag-part)
  (put 'make-from-real-imag 'polar
       (lambda (x y)
	 (tag (make-from-real-imag)))))

;; We also have to change the complex package so it uses generic
;; procedures. The new procedures needed are these: SUB (instead of
;; -) and DIV (instead of /).

(define (install-complex-package)
  ...
  (define (add-complex z1 z2)
    (make-from-real-imag (add (real-part z1) (real-part z2))
			 (add (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2)
    (make-from-real-imag (sub (real-part z1) (real-part z2))
			 (sub (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2)
    (make-from-mag-ang (mul (magnitude z1) (magnitude z2))
		       (add (angle z1) (angle z2))))
  (define (div-complex z1 z2)
    (make-from-mag-ang (div (magnitude z1) (magnitude z2))
		       (sub (angle z1) (angle z2))))
  (put 'add '(complex complex)
       (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex)
       (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex)
       (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex)
       (lambda (z1 z2) (tag (div-complex z1 z2)))))

;; Now we have to install the 5 new generic functions, since ADD, SUB,
;; MUL, DIV are already implemented.

(define (square n)
  (apply-generic 'square n))
(define (square-root n)
  (apply-generic 'square-root n))
(define (arctan x y)
  (apply-generic 'arctan x y))
(define (sine a)
  (apply-generic 'sine a))
(define (cosine a)
  (apply-generic 'cosine a))

;; We have to install these procedures in the scheme-number package.
(define (install-scheme-number-package)
  ...
  (put 'square '(scheme-number)
       (lambda (n) (tag (** n 2))))
  (put 'square-root '(scheme-number)
       (lambda (n) (tag (sqrt n))))
  (put 'arctan '(scheme-number scheme-number)
       (lambda (n) (tag (atan n))))
  (put 'sine '(scheme-number)
       (lambda (n) (tag (sin n))))
  (put 'cosine '(scheme-number)
       (lambda (n) (tag (cos n)))))

;; And we have to implement them for the rational package.
(define (install-rational-package)
  ...
  (define (rational->scheme n)
    (/ (numer n) (denom n)))
  ;; In this case, we can easily write this procedure so it returns
  ;; another rational number instead of a scheme-number.
  (put 'square '(rational)
       (lambda (n) (tag (mul-rat n n))))
  (put 'square-root '(rational)
       (lambda (n) (make-scheme-number (sqrt (rational->scheme n)))))
  (put 'arctan '(rational rational)
       (lambda (n) (make-scheme-number (atan (rational->scheme n)))))
  (put 'sine '(rational)
       (lambda (n) (make-scheme-number (sin (rational->scheme n)))))
  (put 'cosine '(rational)
       (lambda (n) (make-scheme-number (cos (rational->scheme n))))))
