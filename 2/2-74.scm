;; Tagging implementation.
(define (attach-tag type-tag contents)
  (cons type-tag contents))
(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum: TYPE-TAG" datum)))
(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum: CONTENTS" datum)))

;; Answer (all a, b, c and d at the same time):

;; Dispatch table and GET and PUT implementation. The table is a two-dimensional
;; dictionary. It is implemented as an alist, in wich each key is itself a cons
;; cel of the two keys (the operation and the division).

;; We define it as a hash-table, since it's mutable.
(define dispatch-table (make-hash-table))

;; Put the procedure in the dispatch table. We make the first element
;; of the key be the operation and the second the type.
(define (put op type proc)
  (hash-set! dispatch-table (list op type) proc))

;; Get the procedure from the dispatch table.
(define (get op type)
  (hash-ref dispatch-table (list op type)))

;; Generic procedures. They access the dispatch table to get the procedure that
;; match the type.

;; Get the record of an employee of a specific division.

;; The division files will contain a set of employees' records. The
;; division file of each division can be structured differently, since
;; each will be using a different procedure.
(define (get-record division-file employee-name)
  (apply (get 'get-record (type-tag division-file))
         (list (contents division-file) employee-name)))

;; Get the salary of an employee.

;; The records of the employees will contain their address and salary. Each
;; division can have a different implementation of the record structure, since
;; each division will use a different procedure to access this information.
(define (get-salary employee-record)
  (apply (get 'get-salary (type-tag employee-record))
         (list (contents employee-record))))

;; Return the record of an employee from all the divisions files. This
;; is not a generic procedure, although it uses the GET-RECORD generic
;; procedure.
(define (find-employee-record employee-name division-files)
  (define (iter division-files)
    (if (null? division-files)
        ;; If the employee is not present in any file.
        #f
        (let ((record (get-record (car division-files) employee-name)))
          ;; If the record is not present in this file, continue iterating.
          (or record (iter (cdr division-files))))))
  (iter division-files))

;; Here are two implementations of division files and their respective
;; records. Both use the data structure of a dictionary. But the first
;; uses hash tables and the second uses alists. Both the division
;; files and the employee records need to be tagged data.

(define chicago-division-file (attach-tag 'chicago (make-hash-table)))
(define chicago-liam-record (attach-tag 'chicago (make-hash-table)))
(define chicago-emma-record (attach-tag 'chicago (make-hash-table)))
(hash-set! (contents chicago-liam-record) 'address "Oak Street")
(hash-set! (contents chicago-liam-record) 'salary 3500)
(hash-set! (contents chicago-emma-record) 'address "Huron Street")
(hash-set! (contents chicago-emma-record) 'salary 2900)
(hash-set! (contents chicago-division-file) 'liam chicago-liam-record)
(hash-set! (contents chicago-division-file) 'emma chicago-emma-record)

;; Example of accessing:
(hash-ref (contents (hash-ref (contents chicago-division-file) 'liam)) 'salary) ;; 3500

(define houston-division-file
  (attach-tag 'houston (list
			(cons 'noah (attach-tag 'houston
						'((address . "Fannin Street")
						  (salary . 2500))))
			(cons 'olivia (attach-tag 'houston
						  '((address . "Stuart Street")
						    (salary . 3200)))))))

;; Example of accessing:
(assoc-ref (contents (assoc-ref (contents houston-division-file) 'noah)) 'salary)

;; We also need to put all the division files in a list so it can be
;; used with the FIND-EMPLOYEE-RECORD procedure.

(define division-files (list chicago-division-file houston-division-file))

;; Install both packages that implement the two procedures for the
;; Chicago and Houston division (hash table and alist implementations,
;; respectively).

(define (install-chicago-package)
  ;; Internal procedures.
  (define (get-record division-file-contents employee-name)
    (hash-ref division-file-contents employee-name))
  (define (get-salary employee-record-contents)
    (hash-ref employee-record-contents 'salary))
  ;; Add the procedures to the dispatch table.
  (put 'get-record 'chicago get-record)
  (put 'get-salary 'chicago get-salary)
  'done)
(install-chicago-package)

(define (install-houston-package)
  ;; Internal procedures.
  (define (get-record division-file-contents employee-name)
    (assoc-ref division-file-contents employee-name))
  (define (get-salary employee-record-contents)
    (assoc-ref employee-record-contents 'salary))
  ;; Add the procedures to the dispatch table.
  (put 'get-record 'houston get-record)
  (put 'get-salary 'houston get-salary)
  'done)
(install-houston-package)

;; Example usage of the two generic procedures with the two
;; implementations:
(get-salary (get-record chicago-division-file 'liam)) ;; 3500
(get-salary (get-record houston-division-file 'noah)) ;; 2500

;; Example usage of the third procedure:
(get-salary (find-employee-record 'liam division-files)) ;; 3500
(get-salary (find-employee-record 'noah division-files)) ;; 2500

;; When the company opens another division, none of the previous procedures have
;; to be changed. Only the implementations of the two generic procedures have to
;; be written for this new division, according to its implementation of its
;; division file and record structure. This new file has to be also added to the
;; files list.

;; This "additivity" property arises from the use of the data-directed
;; programming strategy of "dispatching on type".
