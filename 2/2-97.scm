;; a) Returns NN and DD, which are N and D reduced to lowest terms via
;; the explained algorithm.
(define (reduce-terms n d)
  (let ((g (gcd-terms n d)))
    (list (car (div-terms n g))
	  (car (div-terms d g)))))

;; This procedure is to REDUCE-TERMS the same as the procedure
;; ADD-POLY is to ADD-TERMS.
(define (reduce-poly p1 p2)
  (let ((v1 (variable p1)))
    (if (same-variable? v1 (variable p2))
	(let ((reduced-terms (reduce-terms (term-list p1) (term-list p2))))
	  (list (make-poly v1 (car reduced-terms))
		(make-poly v1 (cadr reduced-terms))))
	(error "Polys not in same var: REDUCE-POLY" (list p1 p2)))))
