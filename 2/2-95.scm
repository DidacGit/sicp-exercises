;; GCD of Q1 and Q2 is not P1 due to the fact that GCD invokes
;; GCD-TERMS, then REMAINDER-TERMS, then DIV-TERMS. In that procedure,
;; the coefficients of the highest terms of the two polynomials are
;; divided. This results in noninteger operations, and programming
;; languages implementations have limited-precision decimal numbers.
