(define (reverse-list x)
  (define (reverse-list-iter in out)
    (if (null? in)
	out
	(reverse-list-iter (cdr in) (cons (car in) out))))
  (reverse-list-iter x (list)))

(define (same-parity . x)
  (let ((first (car x)))
    (define (same-parity-it in out)
      (cond ((null? in)
	     ;; It's faster to do it like this and then reverse the resulting list
	     (reverse-list out))
	    ;; If 'first' and 'car' are both even-even or odd-odd
	    ((equal? (even? first) (even? (car in)))
	     (same-parity-it (cdr in) (cons (car in) out)))
	    (else
	     (same-parity-it (cdr in) out))))
    (same-parity-it x (list))))

(display (same-parity 1 2 3 4 5 6 7)) ;; (1 3 5 7)
(newline)
(display (same-parity 2 3 4 5 6 7)) ;; (2 4 6)
(newline)
