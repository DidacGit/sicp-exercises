(define (make-interval a b)
  (cons (min a b) (max a b)))

(define (lower-bound a)
  (car a))

(define (upper-bound a)
  (cdr a))

(define (width-interval a) ;; measure of uncertainity
  (/ (- (upper-bound a) (lower-bound a))
     2))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (sub-interval minu subtra)
  (make-interval (- (lower-bound minu) (upper-bound subtra))
		 (- (upper-bound minu) (lower-bound subtra))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

;; There are basically three options (3x3=9), for an interval: 1) Both ends
;; are positive 2) Both ends are negative 3) The interval spans '0'. 
(define (positive-intv? a)
  (and (> (upper-bound a) 0)
       (> (lower-bound a) 0)))
(define (negative-intv? a)
  (and (< (upper-bound a) 0)
       (< (lower-bound a) 0)))
(define (spans-zero? a)
  (and (> (upper-bound a) 0)
       (< (lower-bound a) 0)))

(define (mul-interval x y)
  (let ((lx (lower-bound x))
	(ux (upper-bound x))
	(ly (lower-bound y))
	(uy (upper-bound y)))
    (cond ((and (positive-intv? x) (positive-intv? y))
	   (make-interval (* lx ly) (* ux uy)))
	  ((and (positive-intv? x) (negative-intv? y))
	   (make-interval (* ux ly) (* lx uy)))
	  ((and (positive-intv? x) (spans-zero? y))
	   (make-interval (* ux ly) (* ux uy)))
	  ((and (negative-intv? x) (positive-intv? y))
	   (make-interval (* lx uy) (* ux ly)))
	  ((and (negative-intv? x) (nevative-intv? y))
	   (make-interval (* ux uy) (* lx ly)))
	  ((and (negative-intv? x) (spans-zero? y))
	   (make-interval (* lx uy) (* lx ly)))
	  ((and (spans-zero? x) (positive-intv? y))
	   (make-interval (* lx uy) (* ux uy)))
	  ((and (spans-zero? x) (nevative-intv? y))
	   (make-interval (* ux ly) (* lx ly)))
	  ((and (spans-zero? x) (spans-zero? y))
	   ;; There are 2 possibilities for each bound
	   (make-interval (min (* ux ly) (* lx uy))
			  (max (* ux uy) (* lx ly)))))))

(newline)
