#lang racket/gui
(require sicp-pict)
;; The exercises from this section are made to be interpreted by DrRacket (using the 'sicp-pict' module)

(define (transform-painter painter origin corner1 corner2)
  (lambda (frame)
    (let ((m (frame-coord-map frame)))
      (let ((new-origin (m origin)))
        (painter (make-frame
                  new-origin
                  (sub-vect (m corner1) new-origin)
                  (sub-vect (m corner2) new-origin)))))))

;; Answer
(let ((top-left (make-vect 0 1))
      (top-right (make-vect 1 1))
      (bottom-left (make-vect 0 0))
      (bottom-right (make-vect 1 0)))
  
  (define (flip-horiz painter)
    (transform-painter painter bottom-right bottom-left top-right))
  
  (define (rotate-180 painter)
    (transform-painter painter top-right top-left bottom-right))
  
  (define (rotate-270 painter)
    (transform-painter painter top-left bottom-left top-right)))