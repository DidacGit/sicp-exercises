(define (displayn x)
  (display x)
  (newline))
(define (square num)
  (* num num))

;; Direct definition
(define (square-tree-direct tree)
  (cond ((null? tree) '())
	((not (pair? tree)) (square tree))
	(else (cons (square-tree-direct (car tree)) (square-tree-direct (cdr tree))))))

;; Higher-order definitions
;; Using the map(-list) procedure
(define (square-tree-map tree)
  (map (lambda (sub-tree)
	 (if (pair? sub-tree)
	     (square-tree-map sub-tree)
	     (square sub-tree)))
       tree))

;; Using the map-tree procedure (it makes more sense, in my opinion)
(define (map-tree proc tree)
  (cond ((null? tree) '())
	((not (pair? tree)) (proc tree))
	(else (cons (map-tree proc (car tree)) (map-tree proc (cdr tree))))))
(define (square-tree-map-tree tree)
  (map-tree square tree))

(define tree-a (list 1 (list 2 (list 3 4) 5) (list 6 7))) ;; (1 (2 (3 4) 5) (6 7))

(displayn (square-tree-direct tree-a)) ;; (1 (4 (9 16) 25) (36 49))
(displayn (square-tree-map tree-a))  ;; (1 (4 (9 16) 25) (36 49))
(displayn (square-tree-map-tree tree-a))  ;; (1 (4 (9 16) 25) (36 49))
