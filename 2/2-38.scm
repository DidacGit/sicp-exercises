(define (displayn x)
  (display x)
  (newline))
(define (fold-right op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (fold-right op initial (cdr sequence)))))
;; E.g.
;; (fold-right + 0 '(1 2 3)) -> (+ 1 (+ 2 (+ 3 0))) -> 6
(define (fold-left op initial sequence)
  (define (iter result rest)
    (if (null? rest)
	result
	(iter (op result (car rest))
	      (cdr rest))))
  (iter initial sequence))
;; E.g.
;; (fold-left + 0 '(1 2 3)) ->
;; (it 0 '(1 2 3)) -> (it (+ 0 1) '(2 3)) ->
;; (it 1 '(2 3)) -> (it (+ 1 2) '(3)) ->
;; (it 3 '(3)) -> (it (+ 3 3) '()) -> 6
;; Result: '(+ (+ (+ 0 1) 2) 3), therefore
;; (fl + 0 '(1 2 3)) -> (+ (+ (+ 0 1) 2) 3)

(displayn (fold-right / 1 '(1 2 3))) ;; (/ 1 (/ 2 (/ 3 1))) -> 3/2
(displayn (fold-left / 1 '(1 2 3))) ;; (/ (/ (/ 1 1) 2) 3) -> 1/6
(displayn (fold-right list '() '(1 2 3))) ;; (list 1 (list 2 (list 3 '()))) -> '(1 (2 (3 ())))
(displayn (fold-left list '() '(1 2 3))) ;; (list (list (list '() 1) 2) 3) -> '(((() 1) 2) 3)

;; (fd + 0 '(1 2 3)) -> (+ 1 (+ 2 (+ 3 0))) -> 6
;; (fl + 0 '(1 2 3)) -> (+ (+ (+ 0 1) 2) 3) -> 6
;; 3 + 0 + 2 + 1 = 0 + 1 + 2 + 3

;; Commutative Property (of '+'): changing the order of the operands
;; does not affect the result
