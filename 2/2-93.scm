;; Change all the addition, subtraction, multiplication and division
;; procedures to their generic version.
(define (install-rational-package)
  (define (make-rat n d)
    (let ((g (greatest-common-divisor n d)))
      (cons (div n g) (div d g))))
  (define (add-rat x y)
    (make-rat (add (mul (numer x) (denom y))
		   (mul (numer y) (denom x)))
	      (mul (denom x) (denom y))))
  (define (sub-rat x y)
    (make-rat (sub (mul (numer x) (denom y))
		   (mul (numer y) (denom x)))
	      (mul (denom x) (denom y))))
  (define (mul-rat x y)
    (make-rat (mul (numer x) (numer y))
	      (mul (denom x) (denom y))))
  (define (div-rat x y)
    (make-rat (mul (numer x) (denom y))
	      (mul (denom x) (numer y))))
  ...)
;; We have to make GREATEST-COMMON-DIVISOR a generic procedure so it can work for
;; polynomials (and potentially also for other types besides scheme
;; numbers).
(define (greatest-common-divisor x y)
  (apply-generic 'greatest-common-divisor x y))

(define (install-gcd-procedures)
  (define (gcd a b)
    (if (= b 0)
	a
	(gcd b (remainder a b))))
  (define (gcd-terms a b)
    (if (empty-termlist? b)
	a
	(gcd-terms b (remainder-terms a b))))
  (define (remainder-terms a b)
    ;; Exercise 2-91 procedure, which returns a list with the quotient
    ;; and the reminder.
    (cadr (div-terms a b)))
  (put 'greatest-common-divisor '(scheme scheme)
       (lambda (x y)
	 (make-scheme-number (gcd x y))))
  (put 'greatest-common-divisor '(polynomial polynomial)
       (lambda (x y)
	 (let ((var-x (variable x)))
	   (if (same-variable? var-x (variable y))
	       (make-poly var-x
			  (gcd-terms (term-list x) (term-list y)))
	       (error "Polys don't have the same variable" (list x y))))))
  'done)
(install-gcd-procedures)
