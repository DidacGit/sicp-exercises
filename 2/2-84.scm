;; If no method is found in the dispatch table for the arguments,
;; coerce them by the method of successive raising: raise all the
;; arguments to the type of the highest one in the tower of types.
;; This coercion strategy assumes that only same-type operations are
;; used in the package.
(define (apply-generic op . args)
  (let* ((type-tags (map type-tag args))
	 (proc (get op type-tags)))
    (if proc
	(apply proc (map contents args))
	(if (< 1 (length args))
	    (coerce op args type-tags)
	    (error "No method for this type" (list op (car args)))))))

;; In order for this to work, we need a method to know which types are
;; higher or lower than the others. And this method has to enable us
;; to easily add new levels. To do this, we can use a TOWER-OF-TYPES
;; variable, which represents this hierarchy increasingly.

;; Coerce all the arguments to the highest level one. Then search for
;; the procedure in the dispatch table for that type and apply it if
;; it exists.
(define (coerce op args type-tags)

  ;; Find the highest type of the list.
  (define (find-highest-type)
    (define (iter types current-highest)
      (if (null? types)
	  current-highest
	  (let ((current-type (car types))
		(next-types (cdr types)))
	    (if (or (null? current-highest)
		    (higher-type? current-type current-highest))
		(iter next-types current-type)
		(iter next-types current-highest)))))
    (iter type-tags '()))

  ;; Check whether the first type is the highest (the last found in
  ;; the list).
  (define (higher-type? x y)
    (define (iter lst)
      (let ((type (car lst)))
	(cond ((eq? type x) #f)
	      ((eq? type y) #t)
	      (else (iter (cdr lst))))))
    (iter tower-of-types))

  ;; Raise all the necessary arguments.
  (define (raise-args)
    (define (iter in-args out-args)
      (if (null? in-args)
	  out-args
	  (iter (cdr in-args) (append out-args (raise-arg (car out-args))))))
    (iter args '()))

  (let ((highest-type (find-highest-type)))
    
    ;; Keep raising the argument until the highest type, except those
    ;; who are already that type.
    (define (raise-arg arg)
      (define (iter arg)
	(if (eq? (type-tag arg) highest-type)
	    arg
	    (iter (raise arg))))
      (iter arg))

    ;; Apply the procedure on all the (now raised) arguments.
    (apply (get op (symbol-list (length args) highest-type))
	   (raise-args))))

;; Return a list of length LEN with the same symbol SYM.
(define (symbol-list len sym)
  (if (zero? len)
      '()
      (cons sym (symbol-list (- len 1) sym))))
