;; Remember that the APPLY-GENERIC procedure strips the tags and
;; applies the generic procedures to the contents of the data.
(define (install-raise-procedures)
  (define (raise-integer n)
    (make-rational n 1))
  ;; We assume that the representation of the type REAL is just that number stored.
  (define (raise-rational n)
    (make-real (/ (numer n) (denom n))))
  (define (raise-real n)
    (make-complex-from-real-imag n 0))
  (put 'raise '(integer) raise-integer)
  (put 'raise '(rational) raise-rational)
  (put 'raise '(real) raise-real))
(install-raise-procedures)

;; Generic procedure.
(define (raise n)
  (apply-generic 'raise n))
