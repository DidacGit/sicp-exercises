(define (displayn x)
  (display x)
  (newline))

;; (define (subsets s)
;;   (if (null? s)
;;       '()
;;       (let ((rest (subsets (cdr s))))
;; 	(append rest (map <??> rest)))))

;; (s '(1 2 3)) -> '(() (3) (2) (2 3) (1) (1 3) (1 2) (1 2 3)), therefore:
;; (s '(1 2 3)) ->
;; (ap '(() (3) (2) (2 3)) (map p '(() (3) (2) (2 3)))), therefore:
;; (map p '(() (3) (2) (2 3))) -> '((1) (1 3) (1 2) (1 2 3))
;; The procedure is adding the car to each element.

(define (subsets s)
  (if (null? s)
      '(())
      (let ((rest (subsets (cdr s))))
	(append rest (map (lambda (x) ;; 'x' is each element
			    (cons (car s) x))
			  rest)))))

(define set-a '(1 2 3))
(displayn (subsets set-a)) ;; '(() (3) (2) (2 3) (1) (1 3) (1 2) (1 2 3))
