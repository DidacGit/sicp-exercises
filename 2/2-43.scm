;; Necessary procedures
(define (displayn x)
  (display x)
  (newline))
(define (displaym lst)
  (map (lambda (element) (display element)) lst))
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
	  (accumulate op initial (cdr sequence)))))
(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))
(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))
(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
	    (accumulate-n op init (map cdr seqs)))))
;; E.g.
;; (accumulate-n - 0 '((1 2) (3 4))) -> '((- 1 (- 3 0)) (- 2 (- 4 0))) -> '(-2 -2)
;; return the element of an index of a list
(define (list-ref k lst)
  (define (iter count lst)
    (if (= count k)
	(car lst)
	(iter (+ count 1) (cdr lst))))
  (iter 0 lst))

;; Procedures from the last exercise

;; For us, a board is a list of columns, which are represented by a
;; number, the position of the queen. An empty board is an empty list.

(define (display-board board number-of-rows)
  (let ((number-of-columns (length board)))
    ;; In an incorrect board, there can be more than one queen per row
    (map (lambda (row-number)
	   (map (lambda (row-position)
	   	  (if (= row-number (list-ref row-position board))
	   	      (display 'X)
	   	      (display 'O))
	   	  (display " "))
	   	(enumerate-interval 0 (- number-of-columns 1)))
	   (newline))
	 (enumerate-interval 1 number-of-rows))
    (newline)))
(define (display-boards boards number-of-rows)
  (map (lambda (board) (display-board board number-of-rows)) boards))

(define (queens board-size procedure-type)
  (let ((empty-board '()))
    
    (define (adjoin-position new-column board)
      ;; It doesn't need the 'k'
      (append board (list new-column)))

    (define (safe? board)
      ;; It doesn't need the 'k'
      (let* ((board-length (length board))
	     (last-queen-position (list-ref (- board-length 1) board)))
	(define (iter-board column-number)
	  (if (= column-number (- board-length 1))
	      #t
	      (let ((current-queen-position (list-ref column-number board))
		    (column-difference (- board-length (+ column-number 1))))
		;; Same row, up diagonal and down diagonal
		(if (or (= last-queen-position current-queen-position)
			(= last-queen-position (- current-queen-position column-difference))
			(= last-queen-position (+ current-queen-position column-difference)))
		    #f
		    (iter-board (+ column-number 1))))))
	(iter-board 0)))
    
    (define (queen-cols k)
      (if (= k 0)
	  (list empty-board)
	  (filter
	   (lambda (board) (safe? board))
	   (flatmap
	    (lambda (board)
	      (map (lambda (new-column)
		     (adjoin-position new-column board))
		   (enumerate-interval 1 board-size)))
	    (queen-cols (- k 1))))))

    (define (queen-cols-bad k)
      (if (= k 0)
	  (list empty-board)
	  (filter
	   (lambda (board) (safe? board))
	   (flatmap
	    (lambda (new-column)
	      (map (lambda (board)
		     (adjoin-position new-column board))
		   (queen-cols-bad (- k 1))))
	    (enumerate-interval 1 board-size)))))
    
    (if (string=? procedure-type "good")
	(queen-cols board-size)
	(queen-cols-bad board-size))))

(displayn "Time comparison: ")
(let ((time1 (current-time)))
  (queens 8 "bad") ;; 31 seconds
  (displaym (list "Queens bad: " (- (current-time) time1))) (newline))
(let ((time1 (current-time)))
  (queens 8 "good") ;; 0 seconds
  (displaym (list "Queens good: " (- (current-time) time1) " seconds.")) (newline))

;; We look at the number of times that 'queen-cols' gets called

;; "good":
;; E.g. (queens 3): On k=3, T=1. On k=2, T=1. On k=1, T=1. On k=0, T=1.
;; In total 'queen-cols' is called n+1 times, and for each, 'adjoin-position' is called n times
;; So T = (n+1)n = O(n^2)

;; "bad":
;; E.g. (queens 3): On k=3, 1 evaluation. On k=2, 3 evaluations. On k=1, 9 evaluations. On k=0, 27 evaluations.
;; T(3) = 3^0 + 3^1 + 3^2 + 3^3 = 40
;; So T(n) = E(i: 0->n) n^i = O(n^n)

;; So if T = n^2 and T' = n^n
;; T' = n^(n-2)T
;; Therefore: T'(8) = 8^6T = 262144T
;; Take into account that this algorithmic analysis is not precise
