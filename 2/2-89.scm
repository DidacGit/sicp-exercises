;; Now the term-list is represented as a list of coefficients, where
;; the position is the order (decreasingly). But the representation of
;; the terms (independently of the list) is still the same: '(order
;; coeff).

;; Remember that the term-list is guaranteed (by the other procedures)
;; to be ordered. Therefore, when adjoining a new term, we just have
;; to find out if we have to add any zeros before adding it.
(define (adjoin-term term term-list)
  (define (add-zeros zeros term-list)
    (if (zero? zeros)
	(cons (coeff term) term-list)
	(add-zeros (1- zeros) (cons 0 term-list))))
  (add-zeros (- (order term) (length term-list)) term-list))

;; We also have to change this procedure since it's the only one that
;; retrieves a term from a term list. We have to transform the number
;; into a term.
(define (first-term term-list)
  (make-term (1- (length term-list))
	     (car term-list)))

;; These remain the same:
(define (rest-terms term-list) (cdr term-list))
(define (the-empty-termlist) '())
(define (empty-termlist? term-list) (null? term-list))
(define (make-term order coeff) (list order coeff))
(define (order term) (car term))
(define (coeff term) (cadr term))
