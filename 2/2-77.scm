;; Yes, as Alyssa has said: in the dispatch table, the procedures of
;; the selectors REAL-PART, IMAG-PART, MAGNITUDE and ANGLE are only in
;; the columns of POLAR and RECTANGULAR, they need to be also in the
;; column of COMPLEX.

(define z (make-complex-from-real-imag 3 4))

(magnitude z)
(magnitude '(complex (rectangular (3 . 4))))
(apply-generic 'magnitude '(complex (rectangular (3 . 4))))
(apply (get 'magnitude '(complex)) '((rectangular (3 . 4))))
;; The generic procedure MAGNITUDE ends up returning itself when used
;; on a COMPLEX object, but this time applied to its inner RECTANGULAR
;; object.
(magnitude '(rectangular (3 . 4)))
(apply-generic 'magnitude '(rectangular (3 . 4)))
(apply (get 'magnitude '(rectangular)) '((3 . 4)))
;; Now the non-generic MAGNITUDE procedure is returned, because the
;; generic procedure MAGNITUDE is used on a RECTANGULAR object.
(magnitude '(3 . 4))
5
;; APPLY-GENERIC is called two times.

;; This occurs because in the dispatch table, in the MAGNITUDE row and
;; COMPLEX column there's a generic procedure. This means that the
;; dispatch table will be again accessed until non-generic procedure
;; is reached.
