(define (displayn x)
  (display x)
  (newline))

(define x (list 1 2 3))
(define y (list 4 5 6))

(displayn (append x y)) ;; (1 2 3 4 5 6)
(displayn (cons x y)) ;; ((1 2 3) 4 5 6)
(displayn (list x y)) ;; ((1 2 3) (4 5 6))
