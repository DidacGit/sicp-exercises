(define (cons x y)
  (lambda (m) (m x y)))

(define (car z)
  (z (lambda (p q) p)))

;; This expression
(car (cons x y))
;; Expands to (according to applicative-order evaluation)
(car (lambda (m) (m x y)))
;; Then to
((lambda (m) (m x y))
 (lambda (p q) p))
;; Then to
((lambda (p q) p)
 x y)
;; And finally to
x

;; Answer procedure
(define (cdr z)
  (z (lambda (p q) q)))
;; So this expression
(cdr (cons x y))
;; Expands to
(cdr (lambda (m) (m x y)))
;; Then to
((lambda (m) (m x y))
 (lambda (p q) q))
;; Then to
((lambda (p q) q)
 x y)
;; And finally to
y

