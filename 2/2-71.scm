;; Leaf implementation.
(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x)
  (cadr x))
(define (weight-leaf x)
  (caddr x))

;; Tree implementation.
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

;; Construct an ordered set of leaves out of symbol-frequency pairs.
(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)   ; symbol
                               (cadr pair)) ; frequency
                    (make-leaf-set (cdr pairs))))))

;; Construct a huffman tree out of symbol-frequency pairs.
(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

(define (successive-merge set)
  (if (null? (cdr set))
      (car set)
      (successive-merge (adjoin-set (make-code-tree (car set) (cadr set))
                                    (cddr set)))))


(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol sym tree)
  (define (symbol-is-here? branch)
    (if (memq sym (symbols branch))
        #t #f))
  (define (encode-symbol-iter tree)
    (if (leaf? tree)
        '()
        (let ((left-b (left-branch tree))
              (right-b (right-branch tree)))
          (if (symbol-is-here? left-b)
              (cons 0 (encode-symbol-iter left-b))
              (cons 1 (encode-symbol-iter right-b))))))
  (if (symbol-is-here? tree)
      (encode-symbol-iter tree)
      (error "bad symbol: ENCODE-SYMBOL" sym)))

;; Answer:
(define pairs-a '((A 1) (B 2) (C 4) (D 8) (E 16)))
(define pairs-b '((A 1) (B 2) (C 4) (D 8) (E 16)
                  (F 32) (G 64) (H 128) (I 256) (J 512)))

(define tree-a (generate-huffman-tree pairs-a))
;; (((((leaf A 1)
;;     (leaf B 2)
;;     (A B) 3)
;;    (leaf C 4)
;;    (A B C) 7)
;;   (leaf D 8)
;;   (A B C D) 15)
;;  (leaf E 16)
;;  (A B C D E) 31)

(define tree-b (generate-huffman-tree pairs-b))
;; ((((((((((leaf A 1)
;;          (leaf B 2)
;;          (A B) 3)
;;         (leaf C 4)
;;         (A B C) 7)
;;        (leaf D 8)
;;        (A B C D) 15)
;;       (leaf E 16)
;;       (A B C D E) 31)
;;      (leaf F 32)
;;      (A B C D E F) 63)
;;     (leaf G 64)
;;     (A B C D E F G) 127)
;;    (leaf H 128)
;;    (A B C D E F G H) 255)
;;   (leaf I 256)
;;   (A B C D E F G H I) 511)
;;  (leaf J 512)
;;  (A B C D E F G H I J) 1023)

(encode '(E) tree-a) ; (1)
(encode '(J) tree-b) ; (1)
;; The most frequent symbol always requieres only 1 bit.

(encode '(A) tree-a) ; (0 0 0 0)
(encode '(A) tree-b) ; (0 0 0 0 0 0 0 0 0)
;; For the first tree, the least frequent symbol requires 4 bits and for the
;; second tree it requires 9.
;; The number of bits required for the least frequent symbol is N - 1.
