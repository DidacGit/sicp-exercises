(define (make-interval a b) (cons a b))

(define (upper-bound a)
  (max (car a) (cdr a)))

(define (lower-bound a)
  (min (car a) (cdr a)))

(define (width-interval a) ;; measure of uncertainity
  (/ (- (upper-bound a) (lower-bound a))
     2))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

;; The two extreme cases are:
;; 1) The minuend is at its lower bound and the subtrahend is at its upper (lowest result)
;; 2) The minuend is at its upper bound and the subtrahend is at its lower (highest result)
(define (sub-interval minu subtra)
  (make-interval (- (lower-bound minu) (upper-bound subtra))
		 (- (upper-bound minu) (lower-bound subtra))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval x
		(make-interval (/ 1.0 (upper-bound y))
			       (/ 1.0 (lower-bound y)))))
(define (display-interval i)
  (display "[")
  (display (lower-bound i))
  (display ", ")
  (display (upper-bound i))
  (display "]")
  (newline))

;; The width of the result of addition and substraction, are functions
;; of only the widths of the argument intervals

;; a+b = [La + Lb, Ua + Ub]
;; Wa+b = (Ua+b - La+b) / 2 = (Ua + Ub - La - Lb) / 2

;; Wa = (Ua - La) / 2
;; Wb = (Ub - Lb) / 2
;; Wa + Wb = (Ua - La + Ub - Lb) / 2

;; Therefore:
;; Wa+b = Wa + Wb

;; a-b = [La - Ub, Ua - Lb]
;; Wa-b = (Ua-b - La-b) / 2 = (Ua - Lb - La + Ub) / 2

;; Therefore:
;; Wa-b = Wa+b = Wa + Wb

(define ia (make-interval  8 15))
(define ib (make-interval 2 6))
(define wa (width-interval ia)) 
(define wb (width-interval ib)) 
(define wa+b (width-interval (add-interval ia ib))) 
(define wa-b (width-interval (sub-interval ia ib)))
(define (width-add-interval a b)
  (+ (width-interval a) (width-interval b)))
(define (width-sub-interval a b)
  (width-add-interval a b))

(display wa+b) ;; 11/2
(newline)
(display wa-b) ;; 11/2
(newline)
(display (width-add-interval ia ib)) ;; 11/2
(newline)
(display (width-sub-interval ia ib)) ;; 11/2
(newline)

;; If Wa*b is a function of only Wa and Wb, then if:
;; Wa = Wc and Wb = Wd, Wa*b = Wc*d
(define ic (make-interval 2 9)) ;; For example
(define id (make-interval 4 8)) ;; For example
(display (= (width-interval ia) (width-interval ic))) ;; #t
(newline)
(display (= (width-interval ib) (width-interval id))) ;; #t
(newline)
(display
 (= (width-interval (mul-interval ia ib))
    (width-interval (mul-interval ic id)))) ;; #f
(newline)
;; The same reasoning applies to division
(display
 (= (width-interval (div-interval ia ib))
    (width-interval (div-interval ic id)))) ;; #f
(newline)
