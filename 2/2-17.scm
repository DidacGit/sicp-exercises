(display (cdr (list 3))) ;; ()
;; When a list has only one element, the 'cdr' of it is '()' = 'nil'
(newline)

(define (last-pair l)
  (if (null? (cdr l)) 
      (car l)
      (last-pair (cdr l))))

(display (last-pair (list 23 72 149 34))) ;; 34
(newline)
