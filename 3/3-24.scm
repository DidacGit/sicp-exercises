(define (make-table same-key?)
  (let ((table (list '*table*')))

    (define (assoc key records)
      (cond ((null? records) #f)
	    ((same-key? key (caar records))
	     (car records))
	    (else (assoc key (cdr records)))))

    (define (lookup key)
      (let ((record (assoc key (cdr table))))
	(if record (cdr record) #f)))

    (define (insert! key value)
      (let ((record (assoc key (cdr table))))
	(if record
	    (set-cdr! record value)
	    (set-cdr! table (cons (cons key value)
				  (cdr table))))))

    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
	    ((eq? m 'insert-proc!) insert!)
	    (else (error "Unknown operation: TABLE" m))))

    dispatch))

(define (lookup table key) ((table 'lookup-proc) key))
(define (insert! table key value) ((table 'insert-proc!) key value))

;; Tests:
(define t1 (make-table equal?))
(insert! t1 'a 1)
(insert! t1 'b 2)
(lookup t1 'b) ;; 2
