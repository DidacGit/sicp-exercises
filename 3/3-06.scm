;; Arbitrary value and procedure so the answer can be tested.
(define random-init 5)
(define (rand-update x) (+ x 1))

;; First version.
(define rand
  (let ((x random-init))
    (lambda (sym)
      (cond ((eq? sym 'generate)
	     (set! x (rand-update x))
	     x)
	    ((eq? sym 'reset)
	     (lambda (new-value)
	       (set! x new-value)
	       x))
	    (else (error "Unknown keyword: RAND" sym))))))
;; Second version: cleaner.
(define rand
  (let ((x random-init))
    (define (generate)
      (set! x (rand-update x))
      x)
    (define (reset new-value)
      (set! x new-value)
      x)
    (define (dispatch sym)
      (cond ((eq? sym 'generate)
	     (generate))
	    ((eq? sym 'reset)
	     reset)
	    (else (error "Unknown keyword: RAND" sym))))
    dispatch))

(rand 'generate) ;; 6
(rand 'generate) ;; 7
((rand 'reset) 10) ;; 10
(rand 'generate) ;; 11
