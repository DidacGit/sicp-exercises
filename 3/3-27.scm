(load "table.scm")
(use-modules (my-modules table)
	     (statprof))

(define (memoize f)
  (let ((table (make-table)))
    (lambda (x)
      (let ((previously-computed-result
	     (lookup x table)))
	(or previously-computed-result
	    (let ((result (f x)))
	      (insert! x result table)
	      result))))))

(define (fib n)
  (cond ((= n 0) 0)
	((= n 1) 1)
	(else (+ (fib (- n 1)) (fib (- n 2))))))

(fib 37) ;; Not instant.

;; This procedure is very inefficient due to the fact that most
;; computations are repeated a lot of times (exponentially). For
;; example, the computation of (FIB 3) is repeated for each (FIB N)
;; where N>3. At the same time, the computation of (FIB N) is repeated
;; for each (FIB M) where M>N.

(define worse-memo-fib
  (memoize fib))
(worse-memo-fib 37) ;; Not-instant. 
(worse-memo-fib 37) ;; Instant.
(worse-memo-fib 36) ;; Not-instant

;; This version only stores the result of the explicit call of
;; FIB. That's why in the previous example, even though the
;; computation for 37 is instant, the one for 36 isn't.

(define memo-fib
  (memoize
   (lambda (n)
     (cond ((= n 0) 0)
	   ((= n 1) 1)
	   (else (+ (memo-fib (- n 1))
		    (memo-fib (- n 2))))))))

(memo-fib 120) ;; Instant, even the first time!
;; It computes the result in O(n).

(memo-fib 3)
((memoize (lambda (n) ...)) 3)
((let ((table ...))) 3)
;; The table is created, the result of the computation for 3 will be
;; stored. Also, when the procedure is applied on 3, the same
;; procedure is applied on 2, storing the value in the table. This way
;; not only the result of the explicit call is stored in the table,
;; but also all the recursive calls of it.

;; So if every application result for FIB is stored in the table, all
;; the procedure applications of FIB will be O(n) (since the table
;; lookup procedure is also linear)  instead of O(k^n).
