(load "digital-circuit.scm")
(use-modules (my-modules digital-circuit))

;; We need the agenda object and the value of the delay of the and gate.
(define the-agenda (make-agenda))
(define and-gate-delay 3)

;; Initialize the wires and arrange them in an AND gate.
(define i1 (make-wire))
(define i2 (make-wire))
(define out (make-wire))
(and-gate i1 i2 out)

;; | Wire | Procs      | State |
;; |------+------------+-------|
;; | i1   | and-action |     0 |
;; | i2   | and-action |     0 |
;; | out  |            |     0 |

;; Agenda current time: 0
;; | Segment Time | Segment Queue                   |
;; |--------------+---------------------------------|
;; |            3 | (lambda () (set-signal! out 0)) |
;; |              | (lambda () (set-signal! out 0)) |

;; This expression does nothing because the signal doesn't change.
(set-signal! i1 0)
;; This changes the signal and calls the wire's action procedures. 
(set-signal! i2 1)

;; | Wire | Procs      | State |
;; |------+------------+-------|
;; | i1   | and-action |     0 |
;; | i2   | and-action |     1 |
;; | out  |            |     0 |

;; Agenda current time: 0
;; | Segment Time | Segment Queue                   |
;; |--------------+---------------------------------|
;; |            3 | (lambda () (set-signal! out 0)) |
;; |              | (lambda () (set-signal! out 0)) |
;; |              | (lambda () (set-signal! out 0)) |

;; We start the circuit.
(propagate)

;; The procedures get executed and the agenda time changes but, since
;; no wire's signal changed, no action procedure is executed. The
;; agenda is now empty and it's time is 3.

(set-signal! i1 1)
;; In this moment, both signals are 1, therefore the procedure will
;; set out to 1.
(set-signal! i2 0)

;; Agenda current time: 3
;; | Segment Time | Segment Queue                   |
;; |--------------+---------------------------------|
;; |            6 | (lambda () (set-signal! out 1)) |
;; |              | (lambda () (set-signal! out 0)) |

(propagate)

;; First OUT is set to 1 but then it goes back to 0. The result is the
;; expected one: after 6 seconds, the signal of the output wire
;; remains 0.

;; Now lets see how this would differ if the implementation of the set
;; of procedures of each segment in the agenda would be done with
;; Scheme's lists, which act like stacks, instead of queues.

;; Initialize the wires and arrange them in an AND gate.
(define i1 (make-wire))
(define i2 (make-wire))
(define out (make-wire))
(and-gate i1 i2 out)

;; | Wire | Procs      | State |
;; |------+------------+-------|
;; | i1   | and-action |     0 |
;; | i2   | and-action |     0 |
;; | out  |            |     0 |

;; Agenda current time: 0
;; | Segment Time | Segment Queue                   |
;; |--------------+---------------------------------|
;; |            3 | (lambda () (set-signal! out 0)) |
;; |              | (lambda () (set-signal! out 0)) |

;; This expression does nothing because the signal doesn't change.
(set-signal! i1 0)
;; This changes the signal and calls the wire's action procedures. 
(set-signal! i2 1)

;; | Wire | Procs      | State |
;; |------+------------+-------|
;; | i1   | and-action |     0 |
;; | i2   | and-action |     1 |
;; | out  |            |     0 |

;; Agenda current time: 0
;; | Segment Time | Segment Queue                   |
;; |--------------+---------------------------------|
;; |            3 | (lambda () (set-signal! out 0)) |
;; |              | (lambda () (set-signal! out 0)) |
;; |              | (lambda () (set-signal! out 0)) |

(propagate)

;; The procedures get executed and the agenda time changes but, since
;; no wire's signal changed, no action procedure is executed. The
;; agenda is now empty and it's time is 3.

(set-signal! i1 1)
(set-signal! i2 0)

;; Agenda current time: 3
;; | Segment Time | Segment Queue                   |
;; |--------------+---------------------------------|
;; |            6 | (lambda () (set-signal! out 1)) |
;; |              | (lambda () (set-signal! out 0)) |

(propagate)

;; But now, the last procedure that was added to the agenda's segment
;; is the one that gets executed first (since now the segment's
;; procedures set is behaving like a stack instead of like a queue),
;; therefore the output will be 1, which is incorrect for (AND 0 1).
