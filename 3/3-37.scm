(load "constraint-system.scm")
(use-modules (my-modules constraint-system))

(define (celsius-fahrenheit-converter C)
  (c+ (c* (c/ (cv 9) (cv 5))
	  C)
      (cv 32)))

(define (c+ x y)
  (let ((z (make-connector)))
    (adder x y z)
    z))

;; A subtraction can easily be expressed as a sum.
(define (c- x y)
  (let ((z (make-connector)))
    (adder y z x)
    z))

(define (c* x y)
  (let ((z (make-connector)))
    (multiplier x y z)
    z))

;; A division can easily be expressed as a multiplication.
(define (c/ x y)
  (let ((z (make-connector)))
    (multiplier z y x)
    z))

(define (cv v)
  (let ((x (make-connector)))
    (constant v x)
    x))

(define C (make-connector))
(define F (celsius-fahrenheit-converter C))

(probe "C" C)
(probe "F" F)

(set-value! C 10 'user)

;; Probe: C = 10
;; Probe: F = 50
;; done
