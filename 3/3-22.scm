(load "../utils.scm")
(use-modules (my-modules utils))

;; It's basically the same implementation but not needing the
;; FRONT-PTR and REAR-PTR accessor procedures.
(define (make-queue)
  (let ((front-ptr '())
        (rear-ptr '()))
    (define (set-front-ptr! item)
      (set! front-ptr item))
    (define (set-rear-ptr! item)
      (set! rear-ptr item))
    (define (empty-queue?)
      (null? front-ptr))
    (define (front-queue)
      (if (empty-queue?)
	  (error "FRONT called with an empty queue" dispatch)
	  (car front-ptr)))
    (define (insert-queue! item)
      (let ((new-pair (cons item '())))
	(cond ((empty-queue?)
               (set-front-ptr! new-pair)
               (set-rear-ptr! new-pair)
               dispatch)
              (else
               (set-cdr! rear-ptr new-pair)
               (set-rear-ptr! new-pair)
               dispatch))))
    (define (delete-queue!)
      (cond ((empty-queue?)
             (error "DELETE! called with an empty queue" dispatch))
	    (else (set-front-ptr! (cdr front-ptr))
		  dispatch)))
    (define (print-queue)
      (if (empty-queue?)
	  (display "(emtpy)")
	  (dolist (item front-ptr)
		  (display item)
		  (display " "))))
    ;; We only export the procedures that will be used outside the
    ;; queue-implementing package.
    (define (dispatch m)
      (cond ((eq? m 'empty-queue?) (empty-queue?))
	    ((eq? m 'front-queue) (front-queue))
	    ((eq? m 'insert-queue!) insert-queue!)
	    ((eq? m 'delete-queue!) (delete-queue!))
	    ((eq? m 'print-queue) (print-queue))
	    (else (error "Unknown request: MAKE-QUEUE" m))))
    dispatch))

(define (empty-queue? q)
  (q 'empty-queue))

(define (front-queue q)
  (q 'front-queue))

(define (insert-queue! q item)
  ((q 'insert-queue!) item))

(define (delete-queue! q)
  (q 'delete-queue!))

(define (print-queue q)
  (q 'print-queue))

;; Test:
(define q1 (make-queue))
(insert-queue! q1 'a)
(print-queue q1) ;; a

(insert-queue! q1 'b)
(print-queue q1) ;; a b

(delete-queue! q1)
(print-queue q1) ;; b

(delete-queue! q1)
(print-queue q1) ;; (empty)
