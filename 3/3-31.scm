;; In some circuits it may make no difference, but in this one, due to
;; the inverter, it does. Since all the initial signals are 0 and its
;; 0 input will turn into 1. E.g. a circuit made only from AND and OR
;; gates would make no difference, since all inputs and outputs would
;; be 0.
