;; You can have lists with repeated pairs in them. But what defines a
;; cycle is that the same pair is not encountered more than once in
;; the same branch of the tree.

;; This time, we don't actually need assignment. Since we just have to
;; keep track of state for each branch, independently of the others.

;; The procedure descends the tree until the same pair is found in a
;; branch, then #t is returned. Also, this time, instead of a sum, we
;; use OR.

;; This procedure is actually more complete than the one that the
;; exercise asks for, since it checks for loops not only in the CDRs
;; but also in the CARs.
(define (has-cycle? x)
  ;; Return #t if the pair is in the list, #f otherwise.
  (define (is-member? x pair-list)
    (cond ((null? pair-list) #f)
	  ((eq? (car pair-list) x) #t)
	  (else (is-member? x (cdr pair-list)))))
  (define (iter x pair-list)
    ;; If it isn't a pair, return #f. 
    (cond ((not (pair? x)) #f)
	  ;; If we have already encountered this pair, return #t.
	  ;; This should actually stop the iteration, for it to be
	  ;; fully efficient, but it doesn't.
	  ((is-member? x pair-list) #t)
	  ;; Otherwise add it to the list and descend to the CAR and
	  ;; CDR.
	  (else (let ((new-pair-list (cons x pair-list)))
		  (or (iter (car x) new-pair-list)
		      (iter (cdr x) new-pair-list))))))
  (iter x '()))

(has-cycle? (cons 1 (cons 2 (cons 3 '())))) ;; #f

;; This defines a cycle.
(define a (cons 1 '()))
(define b (cons 2 '()))
(define c (cons 3 a))
(set-cdr! a b)
(set-cdr! b c)
(has-cycle? a) ;; #t
