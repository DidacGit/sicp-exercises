(define (count-pairs x)
  (if (not (pair? x))
      0
      (+ (count-pairs (car x))
	 (count-pairs (cdr x))
	 1)))

;; Follow a similar iteration as Ben's one but keep each pair you
;; visit in an unique list, checking whether it is new (with EQ?) and
;; then adding it to the list if it is. Also don't descend further
;; into the tree.

;; For each pair, before descending into the CDR, the pairs list needs
;; to be updated. This complicates things. The easier way to do this
;; is with assignment.  
(define (count-pairs x)
  (define pair-list '())
  ;; Return #t if the pair is in the list, #f otherwise.
  (define (is-member? x pair-list)
    (cond ((null? pair-list) #f)
	  ((eq? (car pair-list) x) #t)
	  (else (is-member? x (cdr pair-list)))))
  (define (iter x)
    ;; If it isn't a pair, or it is and it is already in the list,
    ;; return 0.
    (if (or (not (pair? x)) (is-member? x pair-list))
	0
	;; If it's a new pair, add it to the list. Then return the sum
	;; of 1 plus the result of descending to the CAR and CDR.
	(begin (set! pair-list (cons x pair-list))
	       (+ 1 (iter (car x)) (iter (cdr x))))))
  (iter x))

(count-pairs (cons 'a (cons 'b (cons 'c '())))) ;; 3.

(define x (cons 'a '()))
(count-pairs (cons 'b (cons x x))) ;; 3 instead of 4.

(define y (cons x x))
(count-pairs (cons y y)) ;; 3 instead of 7.

;; For this last one we have to use assignment.
(define x (cons 'a '()))
(define y (cons 'b x))
(define z (cons 'c y))
(set-cdr! x z)
(count-pairs z)  ;; 3 instead of ERROR.
