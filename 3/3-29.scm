;; We may know that (AND A B) = (NOT (AND (NOT A) (NOT B))).

(define (or-gate a1 a2 output)
  (let ((a1-inv (make-wire))
	(a2-inv (make-wire))
	(inv-and (make-wire))))
  (inverter a1 a1-inv)
  (inverter a2 a2-inv)
  (and-gate a1-inv a2-inv inv-and)
  (inverter inv-and output))

;; The or-gate-delay time will be the (+ (* 2 inverter-delay)
;; and-delay), since the two first inversions are done simultaneously.
