(define (make-account balance password)
  (let ((pass-fails 0))
    (define (withdraw amount)
      (if (>= balance amount)
	  (begin (set! balance (- balance amount))
		 balance)
	  "Insufficient funds"))
    (define (deposit amount)
      (set! balance (+ balance amount))
      balance)
    (define (dispatch in-password m)
      (cond ((> pass-fails 6) (call-the-cops))
	    ((eq? in-password password)
	     ;; If it's correct, reset the counter of fails and return
	     ;; the corresponding procedure.
	     (set! pass-fails 0)
	     (cond ((eq? m 'withdraw) withdraw)
		   ((eq? m 'deposit) deposit)
		   (else (error "Unknown request: MAKE-ACCOUNT" m))))
	    (else
	     ;; If it's incorrect, increase the counter and display it.
	     (set! pass-fails (1+ pass-fails))
	     (lambda (amount)
	       (string-append "Incorrect Password. Number of tries: "
			      (number->string pass-fails))))))
    dispatch))

(define (call-the-cops) (error "COPS CALLED!"))
(define acc (make-account 100 'secret-password))

((acc 'secret-password 'withdraw) 10) ;; 90
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 1"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 2"
((acc 'secret-password 'withdraw) 10) ;; 80
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 1"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 2"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 3"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 4"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 5"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 6"
((acc 'some-other-password 'withdraw) 10) ;; "Incorrect Password. Number of tries: 7"
((acc 'some-other-password 'withdraw) 10) ;; Error: ERROR: In procedure scm-error: COPS CALLED!
