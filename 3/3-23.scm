;; The internal representation this time can't be done as a simple
;; list of cons cells. This is because the operation
;; REAR-DELETE-QUEUE! requires the REAR-PTR to be able to move one
;; position backwards.

;; We therefore need a way to not only move forwards the list but also
;; backwards. This can be done by adding an intermediate cons cell
;; between the elements, whose CAR points backwards and whose CDR
;; points forward. The other cons cells remain the same: the CAR holds
;; the value and the CDR points to the next (intermediary) cell. This
;; way, for an item IT: (CAR (CDR IT)) points to the previous element
;; and (CDR (CDR IT)) points to the next one.

;; Besides this, we will still have the two pointers to the front and
;; rear ends of the list, to access it.

;; Our implementation assumes that if the deque is empty, both
;; pointers will be null. Also, if there's only one element, both
;; pointers will point to it. For non-empty deques, there will always
;; be an intermediary cell at the end of it.

(define (make-deque) (cons '() '()))

(define (front-ptr d) (car d))

(define (rear-ptr d) (cdr d))

(define (set-front-ptr! d item-cell)
  (set-car! d item-cell))

(define (set-rear-ptr! d item-cell)
  (set-cdr! d item-cell))

(define (empty-deque? d)
  (null? (front-ptr d)))

(define (front-deque d)
  (if (empty-deque? d)
      (error "FRONT called with an empty deque" d)
      (car (front-ptr d))))

(define (rear-deque d)
  (if (empty-deque? d)
      (error "REAR called with an empty deque" d)
      (car (rear-ptr d))))

(define (front-insert-deque! d value)
  (let* ((new-intermediary (cons '() '()))
	 (new-front (cons value new-intermediary)))
    (if (empty-deque? d)
	;; Both the FRONT and REAR point to the same cell.
	(set-rear-ptr! d new-front)
	(let ((old-front (front-ptr d)))
	  ;; Point the new intermediary's CDR to the old front.
	  (set-cdr! new-intermediary old-front)
	  ;; Point the old intermediary's CAR to the new front.
	  (set-car! (cdr old-front) new-front)))
    ;; Always update the front pointer.
    (set-front-ptr! d new-front)))

(define (print-deque d)
  (define (iter-deque front-pointer str)
    (let ((new-str (string-append str (symbol->string (car front-pointer)) " ")))
      (if (null? (cddr front-pointer))
	  new-str
	  (iter-deque (cdr (cdr front-pointer)) new-str))))
  (if (empty-deque? d)
      "(empty)"
      (iter-deque (front-ptr d) "")))

;; Tests:
(define d1 (make-deque))
(print-deque d1) "(empty)"
(front-insert-deque! d1 'a)
(front-insert-deque! d1 'b)
(front-insert-deque! d1 'c)
(print-deque d1) "c b a "

(define (rear-insert-deque! d value)
  (if (empty-deque? d)
      (front-insert-deque! d value)
      ;; The new-intermediary's CAR points to the old rear.
      (let* ((old-rear (rear-ptr d))
	     (new-intermediary (cons old-rear '()))
	     (new-rear (cons value new-intermediary)))
	;; Point the old-intermediary's CDR to the new rear.
	(set-cdr! (cdr old-rear) new-rear)
	;; Update to the new rear.
	(set-rear-ptr! d new-rear))))

;; Tests:
(rear-insert-deque! d1 'x)
(rear-insert-deque! d1 'y)
(rear-insert-deque! d1 'z)
(print-deque d1) ;; "c b a x y z "

(define (rear-delete-deque! d)
  (if (empty-deque? d)
      (error "DELETE! called with an empty deque" d)
      (let* ((old-rear (rear-ptr d))
	     ;; The actual last cell of the list.
	     (old-intermediary (cdr old-rear))
	     (new-rear (car old-intermediary))
	     ;; The new last cell of the list.
	     (new-intermediary (cdr new-rear)))
	;; Update the rear pointer of the deque.
	(set-rear-ptr! d new-rear)
	;; Remove the pointer from the new intermediary cell's CDR.
	(set-cdr! new-intermediary '())
	;; These two last expressions may not be necessary. They are
	;; used in case these pointers makes it be kept in memory.
	;; Remove the pointer from the old rear's CDR.
	(set-cdr! old-rear '())
	;; Remove the pointer from the last intermediary cell's CAR to
	;; the previous cell.
	(set-car! old-intermediary '()))))

;; Tests:
(rear-delete-deque! d1)
(rear-delete-deque! d1)
(print-deque d1) ;; "c b a x "

(define (front-delete-deque! d)
  (if (empty-deque? d)
      (error "DELETE! called with an empty deque" d)
      (let* ((old-front (front-ptr d))
	     (old-intermediary (cdr old-front))
	     (new-front (cdr old-intermediary))
	     (new-intermediary (cdr new-front)))
	;; Update the deque's front pointer.
	(set-front-ptr! d new-front)
	;; Remove the pointer from the new-intermediary's CAR.
	(set-car! new-intermediary '())
	;; These two last expressions may not be necessary. They are
	;; used in case these pointers makes it be kept in memory.
	(set-cdr! old-front '())
	(set-cdr! old-intermediary '()))))

;; Tests:
(front-delete-deque! d1)
(front-delete-deque! d1)
(print-deque d1) ;; "a x "
