;; We organize the keys using a binary tree. We make it work for
;; alphabetical keys and numeric ones. The left branches decrease and
;; the right ones increase.

;; This implementation doesn't support nested tables. It also doesn't
;; balance the trees, so it's not completely efficient.

;; Each node of the tree is a list with the key, the value, the left
;; subtree and the right one.

;; We have to pass the comparison procedures, depending if the keys
;; are numbers or letters. 
(define (make-table equal-proc lesser-proc)
  (let ((table (list '*table*)))
    
    (define (get-key node) (car node))
    (define (get-value node) (cadr node))
    (define (left-branch node) (caddr node))
    (define (right-branch node) (cadddr node))

    (define (set-value! node value)
      (set-car! (cdr node) value))
    ;; Set a NEW-NODE at the left branch of NODE.
    (define (set-left-branch! node new-node)
      (set-car! (cddr node) new-node))
    ;; Set a NEW-NODE at the right branch of NODE.
    (define (set-right-branch! node new-node)
      (set-car! (cdddr node) new-node))
    
    (define (make-node key value left right)
      (list key value left right))

    ;; Test if the table has no elements yet.
    (define (empty-table)
      (null? (cdr table)))
    
    ;; Set the table to the given value. Useful for debugging or
    ;; testing purposes.
    (define (set-table! tree)
      (set! table (list '*table* tree)))

    ;; Get the table. Useful for debugging or testing purposes.
    (define (get-table) (cadr table))

    ;; Iterate the tree to find the node corresponding to the KEY. If the
    ;; node was not found, return the last visited node.
    (define (assoc key)
      ;; If the next node exists, recur. If not, return the
      ;; current one.
      (define (recur-or-stop node next-proc)
	(let ((next-node (next-proc node)))
	  (if (null? next-node)
	      node
	      (iter next-node))))
      ;; Iterate the tree.
      (define (iter node)
	(let ((node-key (get-key node)))
	  (cond ((equal-proc key node-key)
		 node)
		((lesser-proc key node-key)
		 (recur-or-stop node left-branch))
		(else
		 (recur-or-stop node right-branch)))))
      (iter (cadr table)))

    ;; If ASSOC returns the exact key, return the node's value. If
    ;; not, return #f.
    (define (lookup key)
      (if (empty-table)
	  #f
	  (let ((node (assoc key)))
	    (if (equal-proc key (get-key node))
		(get-value node)
		#f))))

    ;; INSERT! can also substitute a node, besides inserting new
    ;; ones. If the key of the node returned by ASSOC is the same,
    ;; substitute it, if it's not, add it to the node's left or right
    ;; branch.
    (define (insert! key value)
      (let ((new-node (make-node key value '() '())))
	(if (empty-table)
	    (set! table (list (car table) new-node))
	    (let* ((node (assoc key))
		   (node-key (get-key node)))
	      (if (equal-proc key node-key)
		  (set-value! node value)
		  ;; Create the new node.
		  ((if (lesser-proc key node-key)
		       set-left-branch! ; here
		       set-right-branch!)
		   node new-node))))))

    (define (dispatch m)
      (cond ((eq? m 'set-table!) set-table!)
	    ((eq? m 'get-table) (get-table))
	    ((eq? m 'assoc) assoc)
	    ((eq? m 'lookup) lookup)
	    ((eq? m 'insert!) insert!)
	    (else (error "Unknown operation: TABLE" m))))
    dispatch))

(define (set-table! table tree)
  ((table 'set-table!) tree))
(define (get-table table)
  (table 'get-table))
(define (assoc table key)
  ((table 'assoc) key))

;; Testing of ASSOC.
(define t1 (make-table = <))
(set-table! t1 '(5 a (3 b (1 c () ()) ())
		   (9 d (7 e () ()) (11 f () ()))))
(assoc t1 1) ;; (1 c () ())
(assoc t1 9) ;; (9 d (7 e () ()) (11 f () ()))
(assoc t1 8) ;; (7 e () ())

(define t2 (make-table string=? string<?))
(set-table! t2 '("f" 1 ("d" 2 ("b" 3 () ()) ())
		 ("h" 4 ("g" 5 () ()) ("l" 6 () ()))))
(assoc t2 "b") ;; ("b" 3 () ())
(assoc t2 "h") ;; ("h" 4 ("g" 5 () ()) ("l" 6 () ()))
(assoc t2 "m") ;; ("l" 6 () ())

(define (lookup table key)
  ((table 'lookup) key))

;; Testing of LOOKUP.
(lookup t1 1) ;; c
(lookup t1 8) ;; #f

(define (insert! table key value)
  ((table 'insert!) key value))

;; Testing of INSERT!.
(define t3 (make-table = <))
(insert! t3 5 'g)
(insert! t3 3 'i)
(insert! t3 4 'n)
(insert! t3 8 'o)
(insert! t3 3 'u)
(get-table t3)
;; (5 g (3 u () (4 n () ()))
;;    (8 o () ()))
