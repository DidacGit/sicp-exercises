(define (mystery x)
  (define (loop x y)
    (if (null? x)
	y
	(let ((temp (cdr x)))
	  (set-cdr! x y)
	  (loop temp x))))
  (loop x '()))

(define v '(a b c d))
(define w (mystery v))
(loop v '())
(loop '(b c d) v)
(loop '(c d) '(b c d))
(loop '(d) '(c d))
(loop '() '(d))
'(d)
;; v = '(a)
;; w = '(d c b a)
;; See the image with the same name as this file for the diagram.
;; This procedure simply reverts a list.
