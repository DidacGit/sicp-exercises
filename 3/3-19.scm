;; Our algorithm in 3-18 uses the expansion of the OR expressions to
;; compute the value. It also keeps a list with the unique pairs. This
;; makes its needed space scale, which can be inefficient at large.

;; If we want it to take only a constant amount of space (even if it
;; means sacrificing time), we can only use constants or pointers.

;; Since, according to the exercise 1-18, the loops can only be found
;; in the CDRs, this question is not as complicated as it could
;; be. It's, therefore, about linked lists, not trees.

;; This is actually a common interview question. It is called Floyd's
;; turtle and hare algorithm (or Floyd's cycle-finding
;; algorithm). There are two pointers: the turtle and the hare. The
;; turtle moves one position at a time and the hare moves twice as
;; fast. Along the way, if there's actually a loop, they will get
;; trapped in it. In the loop, given the necessary amount of time,
;; they will eventually encounter.
(define (has-cycle? x)
  (define (iter turtle hare)
    (cond ((eq? turtle hare) #t)
	  ((or (null? (cdr hare)) (null? (cddr hare))) #f)
	  (iter (cdr turtle) (cddr hare))))
  (iter x (cdr x)))

(has-cycle? (cons 1 (cons 2 (cons 3 '())))) ;; #f

;; This defines a cycle.
(define a (cons 1 '()))
(define b (cons 2 '()))
(define c (cons 3 a))
(set-cdr! a b)
(set-cdr! b c)
(has-cycle? a) ;; #t

