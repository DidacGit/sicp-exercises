(load "constraint-system.scm")
(use-modules (my-modules constraint-system))

;; Simple example: 2x = y

;; These simply initialize the connectors: with the "#f" value and
;; informant and no constraints.
(define x (make-connector))
(define y (make-connector))
(define c (make-connector))

(constant 2 c)
;; Add CONSTANT as the informant of C and also add it to its
;; constraint list. Also change its value.

;; | Connector | Value | Informant | Constraints |
;; |-----------+-------+-----------+-------------|
;; | c         | 2     | constant  | constant    |
;; | x         | #f    | #f        | (empty)     |
;; | y         | #f    | #f        | (empty)     |

(probe "x" x)
(probe "y" y)
;; Only add PROBE to their constraints list, not as informants.

;; | Connector | Value | Informant | Constraints |
;; |-----------+-------+-----------+-------------|
;; | c         | 2     | constant  | constant    |
;; | x         | #f    | #f        | probe       |
;; | y         | #f    | #f        | probe       |

(multiplier c x y)
;; Only add MULTIPLIER to their constraints list.

;; The C connector, since it has a value, makes the multiplier run
;; PROCESS-NEW-VALUE, but since its terminals aren't enough to process
;; the operation, nothing happens.

;; | Connector | Value | Informant | Constraints         |
;; |-----------+-------+-----------+---------------------|
;; | c         | 2     | constant  | constant multiplier |
;; | x         | #f    | #f        | probe multiplier    |
;; | y         | #f    | #f        | probe multiplier    |

(set-value! x 4 'user)

;; First, it changes the value of X to 2. It also changes its
;; informant to USER. Then it informs MULTIPLIER and PROBE. PROBE will
;; print the new value in the console.

;; MULTIPLIER, on the other hand, upon being informed, performs the
;; operation and changes the value of Y. It also sets itself as Y's
;; informant. Y's PROBE is also informed about the value change and
;; prints this fact in the console. 

;; | Connector | Value | Informant  | Constraints         |
;; |-----------+-------+------------+---------------------|
;; | c         |     2 | constant   | constant multiplier |
;; | x         |     4 | user       | probe multiplier    |
;; | y         |     8 | multiplier | probe multiplier    |

;; Console print:
;; Probe: y = 8
;; Probe: x = 4
;; done

(set-value! y 10 'user)

;; ERROR: Contradiction (8 10). The connector complains since this
;; wouldn't satisfy the constraint's rule.

(forget-value! x 'user)

;; Since USER was the original informant, X loses its informant and
;; informs MULTIPLIER and PROBE about this.

;; | Connector | Value | Informant  | Constraints         |
;; |-----------+-------+------------+---------------------|
;; | c         |     2 | constant   | constant multiplier |
;; | x         |     4 | #f         | probe multiplier    |
;; | y         |     8 | multiplier | probe multiplier    |

;; Then, recursively, upon being informed, MULTIPLAYER makes its
;; terminals, for whom it's the informant, forget their informant and
;; in the end processes the new value(s).

;; | Connector | Value | Informant | Constraints         |
;; |-----------+-------+-----------+---------------------|
;; | c         |     2 | constant  | constant multiplier |
;; | x         |     4 | #f        | probe multiplier    |
;; | y         |     8 | #f        | probe multiplier    |

;; In the end both probes of X and Y are informed about the value loss
;; and they print "?". They have lost the informant, but their values
;; remain unchanged (in our model, not having an informant de facto
;; means not having a value).

(set-value! y 10 'user)

;; Now that Y "has no value" (i.e. has no informant), its value can be
;; changed. Upon changing its value and informant, the MULTIPLIER (and
;; later PROBE) is informed. Then, X value is computed, set, and PROBE
;; its PROBE constrained informed.

;; | Connector | Value | Informant  | Constraints         |
;; |-----------+-------+------------+---------------------|
;; | c         |     2 | constant   | constant multiplier |
;; | x         |     5 | multiplier | probe multiplier    |
;; | y         |    10 | user       | probe multiplier    |

;; Gets printed:
;; Probe: x = 5
;; Probe: y = 10
;; done
