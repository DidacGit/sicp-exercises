;; By evaluating the recursive version of (factorial 6), six frames
;; and environments are created, all pointing to the global
;; environment. The environments E{1..6} have the bindings n:{6..1}
;; respectively, in their respective frames.

;; The iterative version produces a different environment
;; structure. First, E1 is created and in its frame the binding of n:6
;; is created. Then the environments E{2..8} are created. In E2's
;; frame there are the bindings of [p:1 c:1 m:6] and in E8's frame
;; there are the bindings of [p:720 c:7 m:6]. All of the environments
;; point to the global environment.
