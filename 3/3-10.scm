;; This is the equivalent expression using LAMBDA instead of LET.
(define (make-withdraw initial-amount)
  (lambda (balance)
    (lambda (amount)
      (if (>= balance amount)
	  (begin (set! balance (- balance amount))
		 balance)
	  "Insufficient funds")))
  initial-amount)

(define W1 (make-withdraw 100))
(W1 50)
(define W2 (make-withdraw 100))

;; In the Global Environment's frame we have the three bindings of the
;; three procedures defined there: MAKE-WITHDRAW, W1 and
;; W1. MAKE-WITHDRAW points to the Global Environment, has the
;; parameter of INITIAL-AMOUNT and the body of (lambda (balance
;; ... initial-amount).

;; Evaluation of (define W1 (make-withdraw 100)):

;; The procedure application of MAKE-WITHDRAW creates a new frame for
;; the environment E1, with the binding initial-amount:100. In this
;; environment, the expression (lambda (balance ... 100) is
;; evaluated. This creates a new procedure that points to E1, with the
;; parameter of BALANCE and the body of (lambda (amount
;; ... ). Continuing with the evaluation of the expression, a new
;; environment E2 is created whose frame has the binding of
;; balance:100. Now the last lambda expression is evaluated, creating
;; a procedure that points to E2, has the parameter AMOUNT and the
;; body of (if...).

;; Evaluation of (W1 50):

;; This procedure application creates a new environment E3 whose frame
;; has the binding of amount:50. This frame enclosing environment is
;; that of W1: E2. Here the expression (if ...) is evaluated. The
;; binding for AMOUNT is found in this environment's frame, and the
;; binding for BALANCE is found in E2's frame. SET! changes the
;; binding in E2 to balance:50. 50 is also returned.

;; Evaluation of (define W2 (make-withdraw 100))

;; It is the same process as with the evaluation of the definition of
;; W1. But creating two different environments: E4 and E5. The two
;; procedures have the same lambda expression (parameters and body) as
;; their analogous but they point to E4 and E5 this time.

;; We can see how this version of MAKE-WITHDRAW is redundant. The
;; withdraws will affect E2's BALANCE binding and E1's INITIAL-AMOUNT
;; will remain unchanged, occupying space uselessly.
