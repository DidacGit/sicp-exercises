(define (count-pairs x)
  (if (not (pair? x))
      0
      (+ (count-pairs (car x))
	 (count-pairs (cdr x))
	 1)))
;; This procedure is not correct since the same pair can be in more
;; than one place of the structure.

;; See 3-16.png for the box structures.

(count-pairs (cons 'a (cons 'b (cons 'c '())))) ;; 3

(define x (cons 'a '()))
(count-pairs (cons 'b (cons x x))) ;; 4

(define y (cons x x))
(count-pairs (cons y y)) ;; 7

;; For this last one we have to use assignment.
(define x (cons 'a '()))
(define y (cons 'b x))
(define z (cons 'c y))
(set-cdr! x z)
(count-pairs z)  ;; ERROR
