(define (make-monitored f)
  (let ((counter 0))
    (define (mf sym)
      (cond ((eq? sym 'how-many-calls?)
	     counter)
	    ((eq? sym 'reset-count)
	     (set! counter 0)
	     counter)
	    (else
	     (set! counter (1+ counter))
	     (f sym))))
    mf))

(define s (make-monitored sqrt))
(s 100) ;; 10
(s 81) ;; 9
(s 'how-many-calls?) ;; 2
(s 'reset-count) ;; 0
(s 64) ;; 8
(s 'how-many-calls?) ;; 1
