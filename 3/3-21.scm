(load "queue.scm")
(load "../utils.scm")
(use-modules (my-modules queue)
             (my-modules utils))

;; Simply iterate the queue printing each element.
(define (print-queue q)
  (if (empty-queue? q)
      (display "(emtpy)")
    (dolist (item (front-ptr q))
      (display item)
      (display " "))))

(define q1 (make-queue))
(insert-queue! q1 'a) ;; ((a) a)
(print-queue q1) ;; a

(insert-queue! q1 'b) ;; ((a b) b)
(print-queue q1) ;; a b

(delete-queue! q1) ;; ((b) b)
(print-queue q1) ;; b

(delete-queue! q1) ;; (() b)
(print-queue q1) ;; (emtpy)

;; Ben is breaking the queue abstraction: he's not supposed to access
;; a queue object without using one of the procedures of its API. When
;; "b" is deleted from the queue, the REAR-PTR keeps pointing to
;; it. But this doesn't matter because EMPTY-QUEUE? looks at the
;; FRONT-PTR only: the queue representation is correct. But, of
;; course, the Lisp printer doesn't know about this, that's why we
;; need a queue printing procedure.




  
  
