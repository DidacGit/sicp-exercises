;; The base account. Returns the DISPATCH procedure with the local
;; state BALANCE and the procedures that operate on it.
(define (make-base-account balance)
  (define (withdraw amount)
    (if (>= balance amount)
	(begin (set! balance (- balance amount))
	       balance)
	"Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (define (dispatch m)
    (cond ((eq? m 'withdraw) withdraw)
	  ((eq? m 'deposit) deposit)
	  (else (error "Unknown request: MAKE-BASE-ACCOUNT" m))))
  dispatch)

;; The password account. Returns the DISPATCH procedure with the local
;; state BASE-ACC and PASSWORD. First it checks if the password is
;; correct. Then if the message is 'GET-ACCOUNT, in which case it
;; returns the BASE-ACC, in order to make joint accounts. Otherwise it
;; sends the message to the BASE-ACC DISPATCH procedure. 
(define (make-pass-account base-acc password)
  (define (dispatch in-pass m)
    (cond ((not (eq? in-pass password))
	   (lambda (amount) "Incorrect Password"))
	  ;; For when a joint account is made:   
	  ((eq? m 'get-account) base-acc)
	  (else (base-acc m))))
  dispatch)

;; Make a password account from a newly created base account.
(define (make-account balance password)
  (make-pass-account (make-base-account balance) password))

;; Make a password account from another existing one.
(define (make-joint acc in-pass new-pass)
  (make-pass-account (acc in-pass 'get-account) new-pass))

(define peter-acc (make-account 100 'open-sesame)) ;; #<procedure dispatch (in-pass m)>
(define paul-acc (make-joint peter-acc 'open-sesame 'rosebud)) ;; #<procedure dispatch (in-pass m)>
((peter-acc 'open-sesame 'withdraw) 10) ;; 90
((paul-acc 'rosebud 'withdraw) 10) ;; 80
