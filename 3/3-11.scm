(define (make-account balance)
  (define (withdraw amount)
    (if (>= balance amount)
	(begin (set! balance (- balance amount))
	       balance)
	"Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (define (dispatch m)
    (cond ((eq? m 'withdraw) withdraw)
	  ((eq? m 'deposit) deposit)
	  (else
	   (error "Unknown request: MAKE-ACCOUNT"
		  m))))
  dispatch)

(define acc (make-account 50)) ;; #<procedure dispatch (m)>
((acc 'deposit) 40) ;; 90
((acc 'withdraw) 60) ;; 30

;; First, evaluating the definitions of MAKE-ACCOUNT and ACC create
;; those bindings in the global environment's frame.

;; The procedure application of (MAKE-ACCOUNT 50) creates a new frame
;; whose environment is E1. The bindings here are of BALANCE:50 and
;; the three procedures, that point to E1. Both ACC (in the Global
;; Environment) and DISPATCH in E1 are bound to the same procedure
;; object.

;; The evaluation of the expression ((ACC 'DEPOSIT) 40) starts with
;; the procedure application of (ACC 'DEPOSIT). This creates E2 whose
;; frame has the binding of M:'DEPOSIT and enclosing environment is
;; E1. This returns the DEPOSIT environment, which is then applied to
;; 40. A new environment E3 is created. It's enclosing environment is
;; also E1 and its frame contains the binding of AMOUNT:40. The
;; evaluation of the SET! expression in the body of DEPOSIT changes
;; the value of the BALANCE variable bound in E1's frame.

;; The evaluation of the ((ACC 'WITHDRAW) 60) expression is analogous
;; to the previous one. This time creating the environments E4 and E5
;; whose frames contain the bindings of, respectively, M:'WITHDRAW and
;; AMOUNT:60. Those two environments have E1 as their enclosing
;; environments.

;; As we can see, the local state of ACC (the BALANCE variable and the
;; three procedures) is kept in E1 first frame.

;; Defining a new account (DEFINE ACC2 (MAKE-ACCOUNT 100)) would
;; create a new environment E6 whose frame would have its own
;; bindings. Therefore the local states of the two accounts are kept
;; distinct. The only part of the environment structure that's shared
;; is the global environment, which is the enclosing environment of E1
;; and E6.
