;; Insert or substitute a record of a table.
(define (insert! table keys value)
  ;; Iterate the records of a table and possibly the ones of its
  ;; nested tables.
  (define (iter table keys records)
    (if (null? records)
	;; If we finished the record list at this level without
	;; matching, create the necessary record or nested-table(s)
	;; and add them to the table.
	(add-to-table! table (create-record-or-tables keys value))
	(let* ((record (car records))
	       (record-key (car record))
	       (key (car keys))
	       (rest-of-keys (cdr keys)))
	  ;; If the keys match.
	  (if (equal? key record-key)
	      ;; If there aren't any more keys left,
	      (if (null? rest-of-keys)
		  ;; Replace the record's value with the new one.
		  (set-cdr! record value)
		  ;; If there are more keys left.
		  ;; If the record is a nested table.
		  (if (list? record)
		      ;; Iterate it.
		      (iter record rest-of-keys (cdr record))
		      ;; If the record isn't a nested table, replace
		      ;; it with the new record or nested table(s).
		      (set-car! records (create-record-or-tables keys
								 value))))
	      ;; If the keys don't match, continue iterating the records.
	      (iter table keys (cdr records))))))
  (iter table keys (cdr table)))

;; Add to a table a simple record, if the list has only one key, or
;; multiple nested tables, if the list has more elements.
(define (add-to-table! table record)
  (set-cdr! table (cons record
			(cdr table))))

;; Create a simple record or multiple nested tables from a list of
;; keys and a value.
(define (create-record-or-tables keys value)
  (let ((key (car keys))
	(rest-of-keys (cdr keys)))
    (if (null? rest-of-keys)
	(cons key value)
	(list key (create-record-or-tables rest-of-keys value)))))

;; Return the value corresponding to a key or a list of keys (nested
;; tables). The value can be a primitive or it can also be a list of
;; records, if the found record was a nested table.
(define (lookup table keys)
  (define (iter records keys)
    ;; If the key was not found, return #f.
    (if (null? records)
	#f
	;; If there are still records.
	(let* ((record (car records))
	       (record-key (car record))
	       (key (car keys))
	       (rest-of-keys (cdr keys)))
	  ;; If it's the matching record.
	  (if (equal? key record-key)
	      ;; If there aren't any keys left.
	      (if (null? rest-of-keys)
		  ;; Return the record's value
		  (cdr record)
		  ;; If there are more keys left.
		  ;; If the record is a nested table.
		  (if (list? record)
		      ;; Iterate it.
		      (iter (cdr record) rest-of-keys)
		      ;; If the record is not a nested table, return
		      ;; #f.
		      #f))
	      ;; If it's not the matching record, continue iterating.
	      (iter (cdr records) keys)))))
  (iter (cdr table) keys))

;; Tests:
(define t1 (list '*table*))
(begin
  (insert! t1 '(a1) 1)
  (insert! t1 '(a2) 2)
  (insert! t1 '(a3) 3))
t1 ;; (*table* (a3 . 3) (a2 . 2) (a1 . 1))
(insert! t1 '(a3 b1) 4)
t1 ;; (*table* (a3 (b1 . 4)) (a2 . 2) (a1 . 1))
(insert! t1 '(a3 b2) 5)
t1 ;; (*table* (a3 (b2 . 5) (b1 . 4)) (a2 . 2) (a1 . 1))
(insert! t1 '(a3 b1) 6)
t1 ;; (*table* (a3 (b2 . 5) (b1 . 6)) (a2 . 2) (a1 . 1))
(lookup t1 '(a1)) ;; 1
(lookup t1 '(a3)) ;; ((b2 . 5) (b1 . 6))
(lookup t1 '(a3 b1)) ;; 6
(lookup t1 '(a3 b3)) ;; #f
