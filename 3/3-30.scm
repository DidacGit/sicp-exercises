;; This procedure assumes that A, B and S are lists containing the
;; same number of N wires.
(define (ripple-carry-adder a b s c)
  (define (iterate-wires rest-a rest-b rest-s c-in)
    (let ((current-a (car rest-a))
	  (current-b (car rest-b))
	  (current-s (car rest-s))
	  (c-out (make-wire)))
      (full-adder current-a current-b c-in current-s c-out)
      (if (null? (cdr rest-a))
	  'ok
	  ;; Continue iterating the wire lists and now the C-OUT will be
	  ;; the C-IN.
	  (iterate-wires (cdr rest-a)
			 (cdr rest-b)
			 (cdr rest-s)
			 c-out))))
  (iterate-wires a b s c))

;; An N-bit ripple-carry-adder-delay:
r = N * f

;; Full-adder-delay:
f = o + 2h

;; Half-adder-delay:
h = a + max(i + a, o)

;; We can further simply this because according to 3-29 we know that
;; or-gate-delay is:
o = 2i + a > i + a

;; Therefore:
h = a + 2i + a = 2i + 2a

;; Therefore, the N-bit ripple carry-adder-delay equals:
r = N(o 2h) = N(i + a + 4i + 4a) = 5N * (i + a)

