(define (make-accumulator value)
  (lambda (num)
    (set! value (+ value num))
    value))

(define A (make-accumulator 5))
(A 10) ;; 15
(A 10) ;; 25
