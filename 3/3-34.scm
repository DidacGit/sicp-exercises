(load "constraint-system.scm")
(use-modules (my-modules constraint-system))

;; Flawed SQUARER constraint:
(define (squarer a b)
  (multiplier a a b))

;; For example:
(define a (make-connector))
(define b (make-connector))

(set-value! a 4 'user)
(probe "b" b)
(probe "a" a)
;; Probe: a = 4

;; | Connector | Value | Informant | Constraints |
;; |-----------+-------+-----------+-------------|
;; | a         | 4     | user      |             |
;; | b         | #f    |           | probe       |

(squarer a b)

;; 1. (connect a multiplier) -> sets new constraint

;; (inform-about-value multiplier)
;; (set-value! b 16 multiplier) -> sets informant, value and calls PROBE

;; 2. (connect a multiplier)

;; (inform-about-value multiplier)
;; (set-value! b 16 multiplier) -> ignored   

;; 3. (connect b multiplier) -> sets new constraint

;; (inform-about-value multiplayer)
;; (set-value! b 16 multiplier) -> ignored

;; | Connector | Value | Informant  | Constraints      |
;; |-----------+-------+------------+------------------|
;; | a         |     4 | user       | probe multiplier |
;; | b         |    16 | multiplier | probe multiplier |

;; Prints:
;; Probe: b = 16

;; In this case, it works correctly.

(forget-value! a 'user)

;; Since it's the same informant, it unsets it. Also will call PROBE.

;; (inform-about-no-value multiplier):

;; (forget-value! b multiplier) -> Unsets informant, calls PROBE.

;; (forget-value! a multiplier) -> ignored.

;; (forget-value! a multiplier) -> ignored

;; (process-new-value) -> nothing


;; | Connector | Value | Informant | Constraints      |
;; |-----------+-------+-----------+------------------|
;; | a         |     4 | #f        | probe multiplier |
;; | b         |    16 | #f        | probe multiplier |

;; Probe: b = ?
;; Probe: a = ?
;; done

(set-value! b 5 'user)
;; Sets value, informant, and informs MULTIPLIER (and later, PROBE).

;; | Connector | Value | Informant | Constraints      |
;; |-----------+-------+-----------+------------------|
;; | a         |     4 | #f        | probe multiplier |
;; | b         |    25 | user      | probe multiplier |

;; Probe: b = 5
;; done

;; (inform-about-value multiplier)

;; The problem is here. The computation is not performed because A has
;; no value. The MULTIPLIER constraint is thought-out to be used when
;; the two factors are different, therefore, if the product has a
;; value, at least one from them also has to have one to perform the
;; operation. I.e. the multiplier is a 3-variable equation and the
;; squarer is a 2-variable equation.
