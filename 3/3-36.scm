(define a (make-connector))
(define b (make-connector))
(set-value! a 10 'user)

(for-each-except
 setter inform-about-value constraints)

;; Evaluating the A definition creates an environment whose enclosing
;; environment is the Global Environment: E1. This environment
;; contains the binding for VALUE=#f, INFORMANT=#f, CONSTRAINTS='()
;; and the 4 nested procedures. The ME symbol in this environment
;; points at the same object as the A symbol in the Global
;; Environment.

;; The same happens with the B definition, but with E2.

;; With the evaluation of (set-value! a 10 'user), a new environment
;; E3 is created, with the bindings of CONNECTOR=a, NEW-VALUE=10 and
;; INFORMANT='user. It's enclosing environment is the GE.

;; Then, (a 'set-value) is evaluated, which creates the E4
;; environment, for which the enclosing environment is E1, because it
;; is the environment part of the A/ME procedure object. Its only
;; binding is REQUEST:'SET-VALUE.

;; After this, (set-my-value 10 'user) is evaluated, which creates the
;; E5 environment. Its bindings are NEW-VAL=10 and SETTER:'user. Its
;; enclosing environment is also E1, since it is the environment part
;; of the SET-MY-VALUE procedure object.

;; Now, the stack of environments is E5, E1 and GE, with bindings:

;; | Env | Bindings                |
;; |-----+-------------------------|
;; | GE  | a: lambda               |
;; |     | b: lambda               |
;; |     | (...)                   |
;; |-----+-------------------------|
;; | E1  | value: #f               |
;; |     | informant: #f           |
;; |     | constraints: '()        |
;; |     | set-my-value: lambda    |
;; |     | forget-my-value: lambda |
;; |     | connect: lambda         |
;; |     | me: lambda (same as a)  |
;; |-----+-------------------------|
;; | E5  | newval: 10              |
;; |     | setter: 'user           |

;; The evaluation of (set! value 10) and (set! informant 'user)
;; changes those bindings, in E1. Then the evaluation of the
;; FOR-EACH-EXCEPT expression is done in this same environment: E5.
