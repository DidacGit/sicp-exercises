(load "constraint-system.scm")
(use-modules (my-modules constraint-system))

;; Correct SQUARER procedure:
(define (squarer a b)

  ;; There are only two possibilities now, for a possible computation:
  ;; either B has a value and A hasn't one, or vice-versa.
  (define (process-new-value)
    (if (has-value? b)
	(if (< (get-value b) 0)
	    (error "square less than 0: SQUARER" (get-value b))
	    (set-value! a (sqrt (get-value b)) me))
	(if (has-value? a)
	    (set-value! b (* (get-value a) (get-value a)) me))))

  (define (process-forget-value)
    (forget-value! a me)
    (forget-value! b me)
    (process-new-value))

  ;; This works the same way as with MULTIPLIER.
  (define (me request)
    (cond ((eq? request 'I-have-a-value)
	   (process-new-value))
	  ((eq? request 'I-lost-my-value) (process-forget-value))
	  (else (error "Unknown request: SQUARER"
		       request))))

  (connect a me)
  (connect b me)
  me)

;; Test:

(define a (make-connector))
(define b (make-connector))

(squarer a b)
(probe "b" b)
(probe "a" a)

(set-value! a 4 'user)

;; Probe: a = 4
;; Probe: b = 16
;; done

(forget-value! a 'user)

;; Probe: a = ?
;; Probe: b = ?
;; done

(set-value! b 25 'user)

;; Probe: b = 25
;; Probe: a = 5
;; done
