(define (monte-carlo trials experiment)
  (define (iter trials-remaining trials-passed)
    (cond ((= trials-remaining 0)
	   (/ trials-passed trials))
	  ((experiment)
	   (iter (- trials-remaining 1)
		 (+ trials-passed 1)))
	  (else
	   (iter (- trials-remaining 1)
		 trials-passed))))
  (iter trials 0))

;; Return a random number from a range. LOW included and HIGH not
;; included.
(define (random-in-range low high)
  (let ((range (- high low)))
    (+ low (random range))))

;; Answer:

;; Estimate the area of a circle by choosing a rectangle that contains
;; it. Then picking random points within it. The fraction of those
;; points that pass the circle's predicate is multiplied to the area
;; of the rectangle, giving an estimation of the circle's area.

;; We can use the MONTE-CARLO procedure since our EXPERIMENT procedure
;; captures the ESTIMATE-INTEGRAL parameters as local
;; state. Simplifying things.
(define (estimate-integral p x1 x2 y1 y2 trials)
  (define (experiment)
    ;; We assume that P's arguments are the X and the Y of the point.
    (p (random-in-range x1 x2) (random-in-range y1 y2)))
  ;; The fraction multiplied by the area of the rectangle.
  (* (monte-carlo trials experiment)
     (* (- x2 x1) (- y2 y1))))

;; We can estimate π by calculating the area of the unit circle (that
;; of radius one), since: π = A/r² = A
(define (estimate-pi trials)
  ;; We can center the circle wherever we want. For example, in (1,1).
  (define (unit-circle x y)
    (<= (+ (expt (- x 1) 2) (expt (- y 1) 2)) 1))
  ;; Example of rectangle: the square centered in the (1,1), with a
  ;; side of two. The sum of 0.0 is so the interpreter converts the
  ;; fraction into a decimal.
  (estimate-integral unit-circle 0.0 2.0 0.0 2.0 trials))

(estimate-pi 100000) ;; 3.14252
