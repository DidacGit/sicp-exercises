;; Returns a procedure with local state VAR, initialized as 0. The
;; procedure returns VAR if the argument is 0 and if it is 1, it
;; increments VAR and returns 0.
(define f (let ((var 0))
	    (lambda (n)
	      (if (= n 0)
		  var
		  (and (set! var (1+ var))
		       0)))))

;; Taking into account that my Scheme interpreter evaluates the
;; arguments from left to right, it is the same as:
(+ (f 0) (f 1)) ;; (+ 0 0) -> 0
(+ (f 1) (f 0))	;; (+ 0 1) -> 1
