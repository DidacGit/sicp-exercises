;; The diagrams become really convoluted fast. The important thing to
;; note is that the evaluation of the definitions of X and Z create
;; two new environments (E1 and E2) whose first frame contain the
;; bindings to witch the mutations will be performed.
