(load "constraint-system.scm")
(use-modules (my-modules constraint-system))

;; Constraint that averages two values.
(define (averager a b c)
  (let ((d (make-connector))
	(e (make-connector)))
    (adder a b d)
    (constant 2 e)
    (multiplier e c d)))
