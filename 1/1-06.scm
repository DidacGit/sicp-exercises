;; First note that Scheme's evaluation is aplicative order, therefore it
;; first evaluates the operator and its operands and then applies the
;; procedure. Here is the difference in the two methods:

;; With 'if':

(if (good-enough? 1.0 9)
    1.0
    (sqrt-iter (improve 10 9) 9))

;; When using the 'if' special form, the interpreter first evaluates the
;; predicate and then evaluates the 'then-part' OR the 'else-part'. The
;; iterations will evaluate the 'else-part' (only) until it is
;; 'good-enough' and then it will evaluate the 'then-part'.

;; With the 'new-if':

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
        (else else-clause)))
        
(new-if (good-enough? 1.0 9)
        1.0
        (sqrt-iter (improve 1.0 9) 9))

;; In this case, the special form 'cond' is wrapped as a function. When
;; this function is called (as for every one), the operator and all the
;; operands are evaluated, including the 'else-clause' operand, which
;; will call the same function, evaluating again the 'else-clause',
;; ... leading to an infinite recursion.
