;; - Drawing of the tree done by hand
;; 
;; (helpful resource: http://www.billthelizard.com/2009/12/sicp-exercise-114-counting-change.html)
;; - Orders of Growth:
;;   - Space: at any point in the computation we only need to keep track
;;     of the nodes above the current node. Therefore the space required
;;     is proportional to the maximum depth of the tree.
;;     
;;     Then, we can say that the space required by the procedure grows
;;     linearly with the input. The order of growth of the space required
;;     is *O(n)*.
;;    
;;   - Number of Steps:
;;     - Read and understand the resource. *O(n^5)*.

