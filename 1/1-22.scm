;; This exercise needs to be done by writing a guile script in linux
;; (.scm) and executing it with the command "guile". We are going to need
;; the runtime of our computer.
  
;; necessary functions
(define (runtime)
  (define now (gettimeofday))
  (+ (car now) (/ (cdr now) 1000000.)))
(define (square x) (* x x))  

;; prime functions
(define (smallest-divisor n)
  (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b)
  (= (remainder b a) 0))
(define (prime? n)
  (= n (smallest-divisor n)))
  
;; exercise functions
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))
(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))
(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

;; answer functions
(define (search-3-primes n)
  (define (search-primes n count)
    (cond ((= count 3) (newline))
          ((prime? n) (timed-prime-test n)
                      (search-primes (+ n 1) (+ count 1)))
          (else (search-primes (+ n 1) count))))
  (newline)
  (display "The 3 primes greater than ")
  (display n)
  (display " are:")
  (newline)
  (search-primes (+ n 1) 0))

(search-3-primes 1000) ; 3e-6
(search-3-primes 10000) ; 8e-6
(search-3-primes 100000) ; 2.5e-5
(search-3-primes 1000000) ; 8e-5

3e-6 * sqrt(10) = 9.5e-6
8e-6 * sqrt(10) = 2.5e-5
2.5e-5 * sqrt(10) = 8e-5

;; The results are the expected. The order of growth is O(sqrt(n)).

