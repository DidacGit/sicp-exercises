  (define (inc x) (+ x 1))
  (define (square x) (* x x))
  (define (compose f g)
    (lambda (x)
      (f (g x))))

  ;; (square (inc 6))
  (display ((compose square inc) 6)) ; 49

