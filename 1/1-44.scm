;; - First part:
  (define (square x) (* x x))
  (define dx 0.00001)
  (define (smooth f)
    (lambda (x)
      (/ (+ (f (- x dx))
            (f x)
            (f (+ x dx)))
         3)))
  (display (square 5)) ; 25
  (newline)
  (display ((smooth square) 5)) ; 25.0...6

;; - Second part:
  (define (square x) (* x x))
  (define dx 0.00001)
  (define (compose f g)
    (lambda (x)
      (f (g x))))
  (define (repeated f n)
    (if (= n 1)
        f
        (compose f (repeated f (- n 1)))))
  (define (smooth f)
    (lambda (x)
      (/ (+ (f (- x dx))
            (f x)
            (f (+ x dx)))
         3)))
  (define (n-smooth f n)
    ((repeated smooth n) f))

  (display (square 5)) ; 25
  (newline)
  (display (((repeated smooth 10) square) 5)) ; 25.0...6
  (newline)
  (display ((n-smooth square 10) 5)) ; 25.0...6

