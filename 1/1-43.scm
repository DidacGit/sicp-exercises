  (define (inc x) (+ x 1))
  (define (square x) (* x x))
  (define (compose f g)
    (lambda (x)
      (f (g x))))
  ;; recursive version
  (define (repeated f n)
    (if (= n 1)
        f
        (compose f (repeated f (- n 1)))))
  (display ((repeated square 2) 5)) ;; 625
  ;; iterative version
  (define (repeated f n)
    (define (iter result k)
      (if (= k 1)
          result
          (iter (compose f result) (- k 1))))
    (iter f n))
  (display ((repeated square 2) 5)) ;; 625

  ;; Process:
  ;; A) (repeated square 1) evaluates to
  (square)
  ;; B) (repeated square 2) evaluates to
  (compose square (repeated square 1))
  ;; and then
  (compose square (square))
  ;; and then
  (lambda (x)
    (square (square x)))

