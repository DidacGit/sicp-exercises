;; necessary procedures
(define (average x y)
  (/ (+ x y) 2))
(define (square x) (* x x))

;; Answer procedures
(define (iterative-improve good-enough? improve) 
  ;; returns a procedure that takes a guess as an argument, and keeps
  ;; improving it until it's good enough 
  (lambda (first_guess) ; '1.0'
    (lambda (num) ; '2' and '0'
      (define (iter guess)
	(if (good-enough? guess num)
	    guess
	    (iter (improve guess num))))
      (iter first_guess)))) 

(define (sqrt x)
  (define (improve guess x)
    (average guess (/ x guess)))
  (define (good-enough? guess x)
    (< (abs (- (square guess) x)) 0.001))
  (display (((iterative-improve good-enough? improve) 1.0) x))
  (newline))

(define (fixed-point f)
  ;; in this case we don't need the last argument: 'num', '0'
  (define tolerance 0.00001)
  (define (improve guess num)
    (f guess))
  (define (good-enough? guess num)
    (< (abs (- guess (f guess)))
       tolerance))
  (display (((iterative-improve good-enough? improve) 1.0) 0))
  (newline))

(newline)
;; the call, example: (sqrt 2)
(sqrt 2) ; 1.41
;; the call, for example: cos(0.74)~0.74
(fixed-point cos) ; 0.74

