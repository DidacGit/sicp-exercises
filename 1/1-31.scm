;; - Recursive process:
(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))
         
(define (inc x) (+ x 1))
(define (unit x) x)
(define (factorial x)
  (product unit 1 inc 5))
  
(factorial 5) ; 120

(define (pi-aprox n)
  (define (term a)
    (if (even? a)
        (/ (+ a 2) (+ a 1))
        (/ (+ a 1) (+ a 2))))
  (* 4 (product term 1 inc n)))
  
(pi-aprox 20) ; 3.21
(pi-aprox 100) ; 3.16

;; - Iterative process:
(define (product term a next b)
  (define (it a result)
    (if (> a b)
        result
        (it (next a) (* result
                        (term a)))))
  (it a 1))
(define (inc x) (+ x 1))
(define (unit x) x)
(define (factorial x)
  (product unit 1 inc 5))
  
(factorial 5) ; 120
