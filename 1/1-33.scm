;; Necessary functions
(define (square x) (* x x))  
(define (inc n) (+ n 1))
(define (unit x) x)
(define (smallest-divisor n)
  (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b)
  (= (remainder b a) 0))
(define (prime? n)
  (= n (smallest-divisor n)))

;; Exercise functions
(define (filtered-accumulate filter combiner null-value term a next b)
  (cond ((> a b) null-value)
        ((filter a) (combiner
                      (term a)
                      (filtered-accumulate filter combiner null-value term (next a) next b)))
        (else (filtered-accumulate filter combiner null-value term (next a) next b))))
(define (sum-sq-primes a b)
  (filtered-accumulate prime? + 0 square a inc b))
(define (prod-relat-primes n)
  (define (filter x)
    (= (gcd x n) 1))
  (filtered-accumulate filter * 1 unit 0 inc (- n 1)))
  
(sum-sq-primes 0 10) ; 88 = sq1+sq2+sq3+sq5+sq7
(prod-relat-primes 10) ; 189 = 1*3*7*9

