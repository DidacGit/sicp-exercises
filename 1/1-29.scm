;; Necessary functions
(define (cube x) (* x x x))

;; Excersise functions
(define (s f a b n)
  (define h (/ (- b a) n))
  (define (yk k)
    (f (+ a (* k h))))
  (define (s-it k)
    (cond ((> k n) 0)
          ((or (= k 0) (= k n))
               (+ (yk k)
                  (s-it (+ k 1))))
          ((even? k)
            (+ (* 2 (yk k))
               (s-it (+ k 1))))
          (else
            (+ (* 4 (yk k))
            (s-it (+ k 1))))))
  (* (/ h 3)
     (s-it 0)))
     
(s cube 0 1 100) ; 0.25

