;; Note that "p" is recursively defined by itself.
(if (= 0 0)
    0
   p)
;; Aplicative Order Evaluation: first the operator gets evaluated, then
;; the operands then the procedure is applied.
(test 0 (p))
;; when evaluating (p), the interpreter starts an infinite recursion
;; Normal Order Evaluation: nothing gets evaluated until their values
;; are needed.
;; sequence of expansions 
(test 0 (p))
(if (= 0 0)
  0
  p)
;; the predicate gets evaluated:
(= 0 0)
;; returns TRUE, then the if-part gets evaluated and returned
0
