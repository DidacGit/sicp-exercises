;; - Normal-order: The if tests test the parameter "b" each time. The "r"
;;   procedure is performed 14 times by the if tests and 4 for the main
;;   expression that yields the result.
  
;; Normal-order
(gcd 206 40)
40 ; the if test
(gcd 40 (r 206 40))
(r 206 40) ; 1
6
(gcd (r 206 40) (r 40 (r 206 40)))
(r 40 (r 206 40)) ; 1 + 2 = 3
4
(gcd (r 40 (r 206 40)) (r (r 206 40) (r 40 (r 206 40))))
(r (r 206 40) (r 40 (r 206 40))) ; 3 + 4 = 7
2
(gcd (r (r 206 40) (r 40 (r 206 40))) (r (r 40 (r 206 40)) (r (r 206 40) (r 40 (r 206 40)))))
(r (r 40 (r 206 40)) (r (r 206 40) (r 40 (r 206 40)))) ; 7 + 7 = 14
0
(r (r 206 40) (r 40 (r 206 40))) ; 14 + 4 = 18
2 ; result

;; - Aplicative-order: the "r" procedure is applied 4 times.
  
;; Applicative-order
(gcd 206 40)
40 ; the if test
(gcd 40 (r 206 40)) ; 1
(gcd 40 6)
(gcd 6 (r 40 6)) ; 2
(gcd 6 4)
(gcd 4 (r 6 4)) ; 3
(gcd 4 2)
(gcd 2 (r 4 2)) ; 4
(gcd 2 0)
2 ; result

