(define (d x)
  (* x 2))
(define (h x)
  (/ x 2))
(define (e? x)
  (= (remainder x 2) 0))
(define (f a b)
  (define (f-it a b c)
    (cond ((= b 0) c)
          ((e? b) (f-it (d a) (h b) c))
          (else (f-it a (- b 1) (+ c a)))))
  (f-it a b 0))
(f 3 7) ; 21

