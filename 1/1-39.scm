  (define pi 3.14159265359)
  (define (cont-frac n d x k)
    (define (iter count result)
      (define (compute-result)
        (/ (n x count)
           (- (d count)
              result)))
      (cond ((= count 1) (compute-result))
            (else (iter (- count 1) (compute-result)))))
    (iter (- k 1) (/ (n x k)
                     (d k))))
  (define (n x count)
    (if (= 1 count)
        x
        (* x x)))
  (define (d x)
    (- (* 2 x)
       1))
  (define (lam x)
    (display (cont-frac n d x 1000))
    (newline))

  (lam pi) ;; 2e-13 ~ 0
  (lam (* pi 0.25)) ;; 1

