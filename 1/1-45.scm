;; - First Part
  ;; Fixed-point functions
  (define (average x y)
    (/ (+ x y) 2))
  (define tolerance 0.00001)
  (define (fixed-point f first-guess)
    (define (close-enough? v1 v2)
      (< (abs (- v1 v2)) tolerance))
    (define (try guess)
      (let ((next (f guess)))
        (if (close-enough? guess next)
            next
          (try next))))
    (try first-guess))
  (define (average-damp f)
    (lambda (x) (average x (f x))))
  ;; Repeat functions
  (define (compose f g)
    (lambda (x)
      (f (g x))))
  (define (repeated f n)
    (if (= n 1)
        f
      (compose f (repeated f (- n 1)))))
  ;; New functions
  ;; it calculates the nth-root of 'x', average-damping as many times as specified
  (define (nth-root nth avdamp-num x)
    (define (root-function y)
      (/ x
         (expt y
               (- nth 1))))
    (fixed-point ((repeated average-damp avdamp-num) root-function)
                 1.0))
  ;; the number '20' chosen as example
  (define (display-nth-root-of-20 nth avdamp-num) 
    (display (nth-root nth avdamp-num 20))
    (newline))

  (display-nth-root-of-20 2 1) ; 4.47
  (display-nth-root-of-20 3 1) ; 2.71
  (display-nth-root-of-20 4 2) ; 2.11
  (display-nth-root-of-20 5 2) ; 1.82
  (display-nth-root-of-20 6 2) ; 1.65
  (display-nth-root-of-20 7 2) ; 1.53
  (display-nth-root-of-20 8 3) ; 1.45
  (display-nth-root-of-20 15 3) ; 1.22
  (display-nth-root-of-20 16 4) ; 1.21
  (newline)

;; - Second part:
;;   As we can see, we have to increment the number of times that the
;;   'average-dump' function is applied when calculating the roots: 4
;;   (2^2), 8 (2^3), 16 (2^4), etc.

  ;; Fixed-point functions
  (define (average x y)
    (/ (+ x y) 2))
  (define tolerance 0.00001)
  (define (fixed-point f first-guess)
    (define (close-enough? v1 v2)
      (< (abs (- v1 v2)) tolerance))
    (define (try guess)
      (let ((next (f guess)))
        (if (close-enough? guess next)
            next
            (try next))))
    (try first-guess))
  (define (average-damp f)
    (lambda (x) (average x (f x))))
  ;; Repeat functions
  (define (compose f g)
    (lambda (x)
      (f (g x))))
  (define (repeated f n)
    (if (= n 1)
        f
        (compose f (repeated f (- n 1)))))
  ;; New functions
  (define (logb base x)
    ;; scheme doesn't have logb(x) function?
    (/ (log x) (log base)))
  (define (nth-root nth x)
    ;; it calculates the nth-root of 'x', average-damping as many times as needed
    (define (avdamp-num)
      ;; it calculates the number of times it must be av-damped
      (floor (logb 2 nth))) ; rounded to the floor
    (define (root-function y)
      (/ x
         (expt y
               (- nth 1))))
    (fixed-point ((repeated average-damp (avdamp-num)) root-function)
                 1.0))
  ;; the number '20' chosen as example
  (define (display-nth-root-of-20 nth) 
    (display (nth-root nth 20))
    (newline))

  (display-nth-root-of-20 2) ; 4.47
  (display-nth-root-of-20 3) ; 2.71
  (display-nth-root-of-20 4) ; 2.11
  (display-nth-root-of-20 5) ; 1.82
  (display-nth-root-of-20 6) ; 1.65
  (display-nth-root-of-20 7) ; 1.53
  (display-nth-root-of-20 8) ; 1.45
  (display-nth-root-of-20 15) ; 1.22
  (display-nth-root-of-20 16) ; 1.21
  (newline)

