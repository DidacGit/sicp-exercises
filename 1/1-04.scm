  (define (a-plus-abs-b a b)
    ((if (> b 0)
         +
       -)
     a b))
;; This procedure returns the sum of a plus the absolute value of b. The
;; 'if' expression returns an operator instead of a value.
