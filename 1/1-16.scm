;; More info on the "1_16" image.

;; necessary functions
(define (square n)
  (* n n))

;; answer
(define (fast-exp b n)
  (define (iter b n a)
    (cond ((= n 0) a)
          ((even? n) (iter (square b) (/ n 2) a))
          (else (iter b (- n 1) (* a b)))))
  (iter b n 1))

