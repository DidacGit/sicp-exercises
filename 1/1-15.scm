;; a)
(sine 12.15), (p (sine 4.16)), (p (sine 1.38)), (p (sine 0.46)), (p (sine 0.15)), (p (sine 0.051)). 5 Times.

;; b)
;; The order of growth of the space required and number of steps is the
;; same, since this function doesn't form a tree but a single succession
;; of nodes (number of iterations = number of nodes).   
;;    
;; - Number of necessary iterations:
;;   
;; | angle | iterations | log3(angle) |
;; |-------+------------+-------------|
;; |     3 |          4 |           1 |
;; |     9 |          5 |           2 |
;; |    27 |          6 |           3 |
;; 
;; The order of growth is O(log3(a)) and since
;; log3(x)=log(x)/log(3)=k*log(x), it equals to *O(log(a))*.

