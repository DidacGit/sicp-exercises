;;  Think it like it's a bidimensional Fibonacci. Instead of f(x) we
;;  have f(x,y).
  
;      1
;     1 1
;    1 2 1
;   1 3 3 1
;  1 4 6 4 1
;
;            / 1                              , c = 1 or r = c
; f(r, c) = <  
;            \ f(r - 1, c - 1) + f(r - 1, c)  , otherwise

(define (f r c)
  (cond ((= c 1) 1)
        ((= c r) 1)
        (else (+ (f (- r 1) c) (f (- r 1) (- c 1))))))
(f 5 3) ; 6

