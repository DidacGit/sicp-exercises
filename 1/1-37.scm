;; - The result of 1/Φ is 0.6180339887.
;; - a)
(define (cont-frac n d k)
  (define (iter count)
    (cond ((= count k) (/ (n k) (d k)))
          (else (/ (n count)
                   (+ (d count)
                      (iter (+ count 1)))))))
  (iter 1))
(define (cont-frac-gold k)
  (display (cont-frac (lambda (i) 1.0)
                      (lambda (i) 1.0)
                      k))
  (newline))

(cont-frac-gold 10)  ;; 0.6179775280898876
(cont-frac-gold 100)  ;; 0.6180339887498948
(cont-frac-gold 1000)  ;; 0.6180339887498948

;; - a.2) Finding the exact k 
(define (cont-frac n d k)
  (define (iter count)
    (cond ((= count k) (/ (n k) (d k)))
          (else (/ (n count)
                   (+ (d count)
                      (iter (+ count 1)))))))
  (iter 1))
(define (cont-frac-gold k)
  (cont-frac (lambda (i) 1.0)
             (lambda (i) 1.0)
             k))
(define (find-k)
  (define (is-it? x)
    (if (= (floor (* x 10000)) 6180)
      #t
      #f))
  (define (iter count)
    (cond ((is-it? (cont-frac-gold count))
             (display count)
             (display ", ")
             (display (cont-frac-gold count)))
           (else (iter (+ count 1)))))
  (iter 1))

(find-k)  ;; 11, 0.6180555555555556

;; - b) Iterative process (recursive procedure)
;; Now the iteration has to begin from the end, so the result can be kept on one variable
(define (cont-frac n d k)
  (define (iter count result)
    (define (compute-result)
      (/ (n count)
         (+ (d count)
            result)))
    (cond ((= count 1) (compute-result))
          (else (iter (- count 1) (compute-result)))))
  (iter (- k 1) (/ (n k)
                   (d k))))

(define (cont-frac-gold k)
  (display (cont-frac (lambda (i) 1.0)
                      (lambda (i) 1.0)
                      k))
  (newline))

(cont-frac-gold 10)  ;; 0.6179775280898876
(cont-frac-gold 100)  ;; 0.6180339887498948
(cont-frac-gold 1000)  ;; 0.6180339887498948

