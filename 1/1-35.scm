;; - The transformation yields the golden ratio:
;;   x = 1 + 1/x; φ^2 = φ + 1 (the golden ratio)
 
;; - Solving the quadratic formula we can find that the (positive) result
;;   is: φ = 1.62

;; - The function f(x) whose fixed point is φ has to be: f(x) = x;
;;   f(x) = 1 + 1/x.

;; Excersise functions
(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

;; Answer functions
(fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0) ; we use a float so it returns a decimal and not a fraction
;; 1.618...
