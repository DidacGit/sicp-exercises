(define (d x)
  (* x 2))
(define (h x)
  (/ x 2))
(define (e? x)
  (= (remainder x 2) 0))
(define (f a b)
  (cond ((= b 0) 0)
        ((e? b) (f (d a) (h b)))
        (else (+ a (f a (- b 1))))))
(f 3 7) ; 21

