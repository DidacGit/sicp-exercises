(define (A x y)
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (else (A (- x 1)
                 (A x (- y 1))))))
;; first expression
(A 1 10)
(A 0 (A 1 9))
(A 0 (A 0 (A 1 8)))
(A 0 (A 0 (A 0 (A 1 7))))
(A 0 (A 0 (A 0 (A 0 (A 1 6)))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 1 5))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 4)))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 3))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 2)))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 1))))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 2))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 4))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 8))))
.... ;; 7 reductions later
1024
;; (A 1 10) = 2^10
;; then: (A 1 n) = 2^n

;; second expression
(A 2 4)
(A 1 (A 2 3))
(A 1 (A 1 (A 2 2)))
(A 1 (A 1 (A 1 (A 2 1))))
(A 1 (A 1 (A 1 2)))
(A 1 (A 1 (A 0 (A 1 1))))
(A 1 (A 1 (A 0 2)))
(A 1 (A 1 4))
;; 3 expansions later
(A 1 (A 0 (A 0 (A 0 (A 1 1)))))
;; 4 reductions later
(A 1 16)
;; 15 expansions later
(A 0...(A 1 1)...)
;; 16 reductions later
65536
;; (A 2 4) = (A 1 16) = 2^16 = 2^2^2^2
;; then: (A 2 n) = 2^2^2...(n times)

;; third expression
(A 3 3)
(A 2 (A 3 2))
(A 2 (A 2 (A 3 1)))
(A 2 (A 2 2))
(A 2 (A 1 (A 2 1)))
(A 2 (A 1 2))
(A 2 (A 0 (A 1 1)))
(A 2 (A 0 2))
(A 2 4)
;; we know from the last expression that:
(A 1 16)
65536
;; (A 3 3) = (A 2 4) = (A 1 16) = 2^16

;; Mathematical definitions:

(define (f n) (A 0 n))
;; (f n) computes 2n
(define (g n) (A 1 n))
;; (g n) computes 2^n
(define (h n) (A 2 n))
;; (h n) computes 2^2^2^2... (the number of 2s is n)

