;; - Recursive process:
  
;; Necessary functions
(define (cube x) (* x x x))
(define (inc n) (+ n 1))
(define (unit x) x)
(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))
(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))
;; Exercise functions
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
                (accumulate combiner null-value term (next a) next b))))
(define (sum2 term a next b)
  (accumulate + 0 term a next b))
(define (prod2 term a next b)
  (accumulate * 1 term a next b))

(sum cube 1 inc 10) ; 3025
(sum2 cube 1 inc 10) ; 3025
(product unit 1 inc 5) ; 120
(prod2 unit 1 inc 5) ; 120

;; - Iterative process:

;; Necessary functions
(define (cube x) (* x x x))
(define (inc n) (+ n 1))
(define (unit x) x)
;; Exercise functions
(define (accumulate combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner result (term a)))))
  (iter a null-value))
(define (sum term a next b)
  (accumulate + 0 term a next b))
(define (prod term a next b)
  (accumulate * 1 term a next b))
  
(sum cube 1 inc 10) ; 3025
(prod unit 1 inc 5) ; 120

