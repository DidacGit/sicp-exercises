;; More info on the "1_11" image, about the exercise and about the
;; iterative process of Fibonacci.

;; Recursive Procedure, Recursive Process
(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1))
         (* 2 (f (- n 2)))
         (* 3 (f (- n 3))))))
(f 5)
;; Recursive Procedure, Iterative Process
(define (g n)
  (define (g-it a b c n)
    (if (= n 0)
        a
        (g-it b
              c
              (+ c (* 2 b) (* 3 a))
              (- n 1))))
  (g-it 0 1 2 n))
(g 5)

