 (define (square m)  
   (display "square ")(display m)(newline) 
   (* m m)) 
  
;;  => (expmod 5 101 101) 
;;  square 5 
;;  square 24 
;;  square 71 
;;  square 92 
;;  square 1 
;;  square 1 
;;  5 
;;  => (remainder (fast-expt 5 101) 101) 
;;  square 5 
;;  square 25 
;;  square 625 
;;  square 390625 
;;  square 152587890625 
;;  square 23283064365386962890625 
;;  5 

