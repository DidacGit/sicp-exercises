(define tolerance 0.00001)
(define count -1)
(define (average x y) (/ (+ x y) 2))
;; Without damping
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (set! count (+ count 1))
      (newline)
      (display guess) (display " , ") (display count)
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))
(fixed-point (lambda (x) (/ (log 1000) (log x))) 2) ;; 4.55, 33 times

(set! count -1)
;; With damping
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (set! count (+ count 1))
      (newline)
      (display guess) (display " , ") (display count)
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))
(fixed-point (lambda (x) (average x (/ (log 1000) (log x)))) 2) ;; 4.55, 8 times

