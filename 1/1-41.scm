;; - First Part
  (define (inc x) (+ x 1))
  (define (double f) 
    (lambda (x)
      (f (f x)))) 
  (display (inc 3)) ; 4
  (newline)
  (display ((double inc) 3)) ; 5
  (newline)
  (display (((double double) inc) 5)) ; 9
  (newline)
  (display (((double (double double)) inc) 5)) ; not 13, 21

;; - Second Part:
  ;; A)
  ;; (double inc), evaluates to
  (lambda (x)
    (inc (inc x)))

  ;; B)
  ;; (double double), evaluates to
  (lambda (x)
    (double (double x)))

  ;; C)
  ;; (double (double double)), evaluates to
  (lambda (x)
    ((double double) ((double double) x)))
  ;; and then to
  ;; the (y) parameter refers to the 'x'
  (lambda (x)
    ((double double) ((lambda (y)
                        (double (double y))) x)))
  ;; and then to
  (lambda (x)
    ((double double) (double (double x))))
  ;; and then to
  ;; the (y) parameter refers to the '(double (double x))'
  (lambda (x)
    ((lambda (y)
       (double (double y))) (double (double x))))
  ;; and then to
  (lambda (x)
    (double (double (double (double x)))))

  ;; D)
  ;; ((double (double double)) inc), evaluates to
  ((lambda (x)
     (double (double (double (double x))))) inc)
  ;; and then to
  (double (double (double (double inc))))
  ;; and then to
  (double (double (double (lambda (x)
                            (inc (inc x))))))
  ;; and then to
  (double (double (lambda (x)
                    ((lambda (z)
                       (inc (inc z))) ((lambda (y)
                                         (inc (inc y))) x)))))
  ;; and then to
  (double (double (lambda (x)
                    ((lambda (z)
                       (inc (inc z))) (inc (inc x))))))
  ;; and then to
  (double (double (lambda (x)
                    (inc (inc (inc (inc x)))))))
  ;; and then to
  (double (lambda (x)
            ((lambda (z)
               (inc (inc (inc (inc z))))) ((lambda (y)
                                             (inc (inc (inc (inc y))))) x))))
  ;; and then to
  (double (lambda (x)
            ((lambda (y)
               (inc (inc (inc (inc y))))) (inc (inc (inc (inc x)))))))
  ;; and then to
  (double (lambda (x)
            (inc (inc (inc (inc (inc (inc (inc (inc x))))))))))
  ;; and then to
  (lambda (x)
    ((lambda (z)
       (inc (inc (inc (inc (inc (inc (inc (inc x))))))))) ((lambda (y)
                                                             (inc (inc (inc (inc (inc (inc (inc (inc x))))))))) x)))
  ;; and then to
  (lambda (x)
    ((lambda (y)
       (inc (inc (inc (inc (inc (inc (inc (inc x))))))))) (inc (inc (inc (inc (inc (inc (inc (inc x))))))))))
  ;; and then to
  (lambda (x)
    (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc x)))))))))))))))))
  ;; The number of 'inc' procedures has grown like this: 1, 2, 4, 8, 16

  ;; E)
  ;; (((double (double double)) inc) 5), evaluates to
  ((lambda (x)
     (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc x))))))))))))))))) 5)
  ;; and then to
  (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc (inc 5))))))))))))))))
  ;; and finally, to
  21

